/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#include <iostream>
#include "../TrackerReadout/TRB_ConfigRegisters.h"


using namespace FASER;

/** *******************************************************
 \brief Default configuration set in the constructor
 ******************************************************** */
ConfigReg::ConfigReg() : m_FPGA_version(0x1E)
{
  Global        = 0x000000000FF800FF;
  GlobalLoop    = 0x3E800000;
  Module_L1En   = 0x0;
  Module_BCREn  = 0x0;
  Module_ClkCmdSelect = 0x0;
  Led0RXEn      = 0x0;
  Led1RXEn      = 0x0;
}

/** *******************************************************
\brief Default configuration set in the constructor
******************************************************** */
void ConfigReg::Print()
{
  std::cout << "m_FPGA_version = 0x"<< std::hex<<m_FPGA_version<<std::endl;
  std::cout <<std::endl << "Content of TRB configuration register object: "<<std::endl;
  std::cout << "Module L1AEn          = 0x" <<std::hex <<(int)Module_L1En<<std::endl;
  std::cout << "Module BCREn          = 0x" <<std::hex <<(int)Module_BCREn<<std::endl;
  std::cout << "Module ClkCmdSelect   = 0x" <<std::hex <<(int)Module_ClkCmdSelect<<std::endl;
  std::cout << "Module Led En         = 0x" <<std::hex <<(int)Led0RXEn<<std::endl;
  std::cout << "Module LedxEn         = 0x" <<std::hex <<(int)Led1RXEn<<std::endl;
  std::cout << "Global config reg     = 0x" <<std::hex <<Global<<std::endl;
  std::cout << "      RXTimeout       = "<<std::dec << (Global & 0xff) <<std::endl;
  std::cout << "      Overflow        = "<<std::dec << ((Global >> 8) & 0xfff) <<std::endl;
  std::cout << "      L1Timeout       = "<<std::dec << ((Global >> (8+12)) & 0xff) <<std::endl;
  std::cout << "    L1TimeoutDisable  = "<<std::dec << ((Global >> (8+12+8)) & 0x1) <<std::endl;
  std::cout << "      L2SoftL1AEn     = "<<std::dec << ((Global >> (8+12+8+1)) & 0x1) <<std::endl;
  std::cout << "      HardwareDelay0  = "<<std::dec << ((Global >> (8+12+8+1+1)) & 0x7) <<std::endl;
  std::cout << "      HardwareDelay1  = "<<std::dec << ((Global >> (8+12+8+1+1+3)) & 0x7) <<std::endl;
  std::cout << "      RxTimeoutDisable= "<<std::dec << ((Global >> (8+12+8+1+1+3+3)) & 0x1) <<std::endl;
  if ((m_FPGA_version >= 0x1C && m_FPGA_version < 0x110) || m_FPGA_version >= 0x11C){
     std::cout << "      CalLoopNb       = "<<std::dec << ((GlobalLoop ) & 0xFFFFF) <<std::endl;
     std::cout << "      CalLoopDelay    = "<<std::dec << ((GlobalLoop >> (20)) & 0x7FF) <<std::endl;
     std::cout << "      DynamicDelay    = "<<std::dec << ((GlobalLoop >> (20+11)) & 0x1) <<std::endl;
  }
  std::cout << "-----------------------------------------"<<std::endl<<std::endl;
}

/** *******************************************************
 \brief Decodes information in payload vector (as read back from hardware) and fills internal variables.
 ******************************************************** */
void ConfigReg::FillFromPayload(std::vector<uint16_t> payload)
{
  //if (payload.size() != 6){
  //  std::cout << "WARNING: expected 6 payload word but received "<<payload.size()<<" decoded information may be corrupted"<<std::endl;
  //}
  std::cout << "FillFromPayload: using FW version 0x"<<std::hex<< m_FPGA_version<<", decoding "<<std::dec<<payload.size()<<" config words"<<std::endl;
  for (auto word : payload){ std::cout << "0x"<<std::hex<<word<<std::endl;} 
  
  Module_L1En   = 0;
  Module_BCREn  = 0;
  Module_ClkCmdSelect = 0;
  Led0RXEn      = 0;
  Led1RXEn      = 0;
  Global        = 0;
  GlobalLoop    = 0;

  if (m_FPGA_version < 0x1C || (m_FPGA_version > 0x110 && m_FPGA_version < 0x11C)){
  for (auto word : payload){
    switch (word & 0xE000){
      case 0x0:
        Module_L1En   |= (word & 0xFF); // 8 bits
        Module_BCREn  |= ((word >> 8) & 0x1F); // 5 bits
        break;
      case 0x2000:
        Module_BCREn  |= ((word << 5) & 0xE0); // 3 bits
        Module_ClkCmdSelect |= ((word >> 3) & 0xFF); // 8 bits
        Led0RXEn |= ((word >> 11) & 0x3); // 2 bits
        break;
      case 0x4000:
        Led0RXEn |= ((word << 2) & 0x3F); // 6 bits
        Led1RXEn |= ((word >> 6) & 0x7F); // 7 bits
        break;
      case 0x6000:
        Led1RXEn |= ((word << 7) & 0x1); // 1 bits
        Global   |= ((word >> 1) & 0xFFF); // 12 bits
        break;
      case 0x8000:
        Global   |= ((((uint64_t)word) << 12) & 0x1FFF000); // 13 bits
        break;
      case 0xA000:
        Global   |= ((((uint64_t)word) << (12+13)) & 0x3FFE000000); // 13 bits
        break;
      default:
        std::cout << "ERROR: unknown address found 0x"<<std::hex<< (word & 0xE000) <<std::endl;
    }
  }
  } else {
     // 10 words, 12 bits payload each
     for (auto word : payload){
        switch (word & 0xF000){
           case 0x0:
              Module_L1En   |= (word & 0xFF); // 8 bits
              Module_BCREn  |= ((word >> 8) & 0xF); // 4 bits
              break;
           case 0x1000:
              Module_BCREn  |= ((word << 4) & 0xF0); // 4 bits
              Module_ClkCmdSelect |= ((word >> 4) & 0xFF); // 8 bits
              break;
           case 0x2000:
              std::cout << "Word = "<<std::hex<<word<<std::endl;
              Led0RXEn |= ((word ) & 0xFF); // 8bits
              Led1RXEn |= ((word >> 8) & 0xF); // 4 bits
              break;
           case 0x3000:
              Led1RXEn |= ((word << 4) & 0xF0); // 4 bits
              Global   |= ((word >> 4) & 0xFF); // 8 bits
              break;
           case 0x4000:
              Global   |= ((((uint64_t)word) << 8) & 0xFFF00); // 12 bits
              break;
           case 0x5000:
              Global   |= ((((uint64_t)word) << 8+12) & 0xFFF00000); // 12 bits
              break;
           case 0x6000:
              Global   |= ((((uint64_t)word) << 8+12+12) & 0x1F00000000); // 5 bits
              GlobalLoop   |= ((((uint32_t)word) >> 5) & 0xEF); // 7 bits
              break;
           case 0x7000:
              GlobalLoop   |= ((((uint32_t)word) << 7) & 0x7FF80); // 12 bits
              break;
           case 0x8000:
              GlobalLoop   |= ((((uint32_t)word) << 7+12) & 0x7FF80000); // 12 bits
              break;
           case 0x9000:
              GlobalLoop   |= ((((uint32_t)word) << 7+12+12) & 0x80000000); // 1 bits
              break;
        }
     }
  }
}
     /** *******************************************************
       \brief Returns a vector of 8 16-bit words to be used as payload when writing config to the TRB.
       3 MSB are the frame number, remaining 13 LSB contain the config bits.
       Startign from version 1.C-638 this regidterd contains 10 words of 12 bit paylaod and 4 MSB address.
      ******************************************************** */
std::vector<uint16_t> ConfigReg::GetPayloadVector()
{
  std::vector<uint16_t> output;
  uint16_t word = 0, tmp = 0;
  
  if (m_FPGA_version < 0x1C || (m_FPGA_version > 0x110 && m_FPGA_version < 0x11C)){ // TODO: check if this is correct!!}
  word = 0; // wordID 0
  word = Module_L1En; // 8-bits
  tmp = Module_BCREn;
  tmp <<= 8;
  word |= (tmp & 0x1F00); // use only 5-bits
    
  output.push_back(word);
  
  word = 0x2000; //  wordID 1
  tmp  = Module_BCREn;
  tmp >>= 5; // remaining 3 bits out of 8
  word |= tmp;
  tmp = Module_ClkCmdSelect; // 8 bits
  tmp <<=3;
  word |= tmp;
  tmp = Led0RXEn;
  tmp <<= 3+8;
  word |= (tmp & 0x1800); // only use bits 11 & 12
  
  output.push_back(word);
  
  word = 0x4000; //  wordID 2
  tmp  = Led0RXEn;
  tmp >>= 2; // remaining 6 bits out of 8
  word |= tmp;
  tmp = Led1RXEn; // 8 bits
  tmp <<=6;
  word |= (tmp & 0x1FC0); // only use 7 bits
 
  output.push_back(word);
  
  word = 0x6000; //  wordID 3
  tmp  = Led1RXEn;
  tmp >>= 7; // remaining 1 bits out of 8
  word |= tmp;
  tmp = Global; // 64 bits - 36 used
  tmp <<=1;
  word |= (tmp & 0x1FFE); // only 12 bits
  
  output.push_back(word);
  
  word = 0x8000; //  wordID 4
  tmp = Global >> 12; // 12 bits already done, Global is 64bit long
  word |= (tmp & 0x1FFF); // only 13 bits
  
  output.push_back(word);
  
  word = 0xA000; //  wordID 5
  tmp = Global >> (12+13); // 12+13 bits already done, Global is 64bit long
  word |= (tmp & 0x1FFF); // only 13 bits
  output.push_back(word);
  
  if (m_FPGA_version >= 0x15){
    word = 0xC000; //  wordID 6
    tmp = Global >> (12+13+13); // 12+13+13 bits already done, Global is 64bit long
    word |= (tmp & 0x1FFF); // only 13 bits
    output.push_back(word);
    
    word = 0xE000; //  wordID 7
    tmp = Global >> (12+13+13+13); // 12+13+13+13 bits already done, Global is 64bit long
    word |= (tmp & 0x1FFF); // only 13 bits
    output.push_back(word);
  }
  } else {
    // 10 words, 12 bits payload each
    
    word = 0; // wordID 0
    word = Module_L1En; // 8-bits
    tmp = Module_BCREn;
    tmp <<= 8;
    word |= (tmp & 0xF00); // use only 4-bits
    
    output.push_back(word);
    
    word = 0x1000; //  wordID 1
    tmp  = Module_BCREn;
    tmp >>= 4; // remaining 4 bits out of 8
    word |= tmp;
    tmp = Module_ClkCmdSelect; // 8 bits
    tmp <<=4;
    word |= tmp;
    
    output.push_back(word);
    
    word = 0x2000; //  wordID 2
    tmp = Led0RXEn;
    word |= (tmp & 0xff); // use 8 bits
    tmp = Led1RXEn; // 8 bits
    tmp <<=8;
    word |= (tmp & 0xF00); // only use 4 bits
    output.push_back(word);
    
    word = 0x3000; //  wordID 3
    tmp  = Led1RXEn;
    tmp >>= 4; // remaining 4 bits out of 8
    word |= tmp;
    tmp = Global; // 64 bits - 37 used
    tmp <<=4;
    word |= (tmp & 0xFF0); // only 8 bits
    
    output.push_back(word);
    
    word = 0x4000; //  wordID 4
    tmp = Global >> 8; // 8 bits already done, Global is 64bit long
    word |= (tmp & 0xFFF); // only 12 bits
    
    output.push_back(word);
    
    word = 0x5000; //  wordID 5
    tmp = Global >> (8+12); // 8+12=24 bits already done, Global is 64bit long
    word |= (tmp & 0xFFF); // only 12 bits
    
    output.push_back(word);
    
    word = 0x6000; //  wordID 6
    tmp = Global >> (8+12+12); // 8+12+12=32 bits already done, Global is 64bit long
    word |= (tmp & 0x1F); // only 5 bits
    tmp = GlobalLoop; // 32 bits in use
    tmp <<= 5;
    word |= (tmp & 0xFE0); // only 7 bits
    
    output.push_back(word);
    
    word = 0x7000; //  wordID 8
    tmp = GlobalLoop >> (7); // 7 bits already done
    word |= (tmp & 0xFFF); // only 12 bits
   
    output.push_back(word);
       
    word = 0x8000; //  wordID 9
    tmp = GlobalLoop >> (7+12); // 7+12 = 19 bits already done
    word |= (tmp & 0xFFF); // only 12 bits
    
    output.push_back(word);
         
    word = 0x9000; //  wordID 10
    tmp = GlobalLoop >> (7+12+12); // 7+12+12 = 31 bits already done
    word |= (tmp & 0x1); // only 1 bits
    
    output.push_back(word);
    
  }
  return output;
}


/** *******************************************************
 \brief Set bit <module> of 8-bit word <config> to <value>.
 if <module> > 7 all bits are set.
 ******************************************************** */
void ConfigReg::Set_Module_ConfigBit(uint8_t &config, short module, bool value){
  if (module < 8){
    if (value) {
      config |= (0x1 << module);
    } else {
      config &= ~(0x1 << module);
    }
  } else { // simply set for all module
    if (value) {
      config = 0xFF;
    } else {
      config = 0x00;
    }
  }
}

/** *******************************************************
 \brief DEPRECATED: Set Enable L1A bit for the module 0-7. Set for all modules if <module> > 7
 ******************************************************** */
void ConfigReg::Set_Module_L1En(short module, bool value)
{
  std::cerr << "DEPRECATED: Please use ConfigReg::Set_Module_L1En(moduleMask) instead"<<std::endl;
  Set_Module_ConfigBit(Module_L1En, module, value);
}

/** *******************************************************
 \brief Set Enable L1A bit for the module 0-7.
 \param moduleMask bit mask of enabled modules
 ******************************************************** */
void ConfigReg::Set_Module_L1En(short moduleMask)
{
  Module_L1En = moduleMask;
}
  
/** *******************************************************
 \brief DEPRECATED: Set BCREn bit
 ******************************************************** */
void ConfigReg::Set_Module_BCREn(short module, bool value){
  std::cerr << "DEPRECATED: Please use ConfigReg::Set_Module_BCREn(moduleMask) instead"<<std::endl;
  Set_Module_ConfigBit(Module_BCREn, module, value);
}

/** *******************************************************
 \brief Set BCREn bit for specified modules
 \param moduleMask bit mask of enabled modules
 ******************************************************** */
void ConfigReg::Set_Module_BCREn(short moduleMask){
  Module_BCREn = moduleMask;
}

/** *******************************************************
 \brief DEPRECATED: Set ClkCmdSelect bit
 ******************************************************** */
void ConfigReg::Set_Module_ClkCmdSelect(short module, bool value){
  std::cerr << "DEPRECATED: Please use ConfigReg::Set_Module_ClkCmdSelect(moduleMask) instead"<<std::endl;
  Set_Module_ConfigBit(Module_ClkCmdSelect, module, value);
}

/** *******************************************************
 \brief Set ClkCmdSelect bit
 \param moduleMask bit mask of modules using CMD/CLK 1
 ******************************************************** */
void ConfigReg::Set_Module_ClkCmdSelect(short moduleMask){
  Module_ClkCmdSelect = moduleMask;
}

/** *******************************************************
 \brief DEPRECATED: Enable Data lines LED and LEDx as requested
 \param module: trab channel 0-8
 ******************************************************** */
void ConfigReg::Set_Module_LedxRXEn(short module, bool enableLed, bool enableLedx){
  std::cerr << "DEPRECATED: Please use ConfigReg::Set_Module_LedRXEn(moduleMask) and Set_Module_LedxRXEn(short moduleMask) instead"<<std::endl;
  if (enableLed){
    Set_Module_ConfigBit(Led0RXEn, module, true);
  } else {
    Set_Module_ConfigBit(Led0RXEn, module, false);
  }
  if (enableLedx){
    Set_Module_ConfigBit(Led1RXEn, module, true);
  } else {
    Set_Module_ConfigBit(Led1RXEn, module, false);
  }
}

/** *******************************************************
 \brief Enable Data lines LED for specified modules
 \param moduleMask bit mask of modules
 ******************************************************** */
void ConfigReg::Set_Module_LedRXEn(short moduleMask){
  Led0RXEn = moduleMask;
}
/** *******************************************************
 \brief Enable Data lines LEDx for specified modules
 \param moduleMask bit mask of modules
 ******************************************************** */
void ConfigReg::Set_Module_LedxRXEn(short moduleMask){
  Led1RXEn = moduleMask;
}

/** *******************************************************
 \brief Set global config -> Rx Timeout (8 bits)
 ******************************************************** */
void ConfigReg::Set_Global_RxTimeout(uint8_t value) // 6 bits
{
  uint64_t tmp = ~(MASK_Global_RxTimeout);
  Global &= tmp; // make all bits of interest 0
  tmp = value;
  Global |= (tmp << SHIFT_Global_RxTimeout)&MASK_Global_RxTimeout;
}


/** *******************************************************
 \brief Set global config ->Overflow (12 bits)
 ******************************************************** */
void ConfigReg::Set_Global_Overflow(uint16_t value) // 12 bits
{
  uint64_t tmp = ~(MASK_Global_Overflow);
  Global &= tmp; // make all bits of interest 0
  tmp = value; 
  Global |= (tmp << SHIFT_Global_Overflow)&MASK_Global_Overflow;
}

/** *******************************************************
 \brief Set global config ->HardwareDelay0 (3 bits)
 ******************************************************** */
void ConfigReg::Set_Global_HardwareDelay0(uint8_t value) // 3 bits
{
  uint64_t tmp = ~(MASK_Global_HardwareDelay0);
  Global &= tmp; // make all bits of interest 0
  tmp = (value); // only consider 12 bits
  Global |= (tmp << SHIFT_Global_HardwareDelay0)&MASK_Global_HardwareDelay0;
}

/** *******************************************************
 \brief Set global config ->HardwareDelay1 (3 bits)
 ******************************************************** */
void ConfigReg::Set_Global_HardwareDelay1(uint8_t value) // 3 bits
{
  uint64_t tmp = ~(MASK_Global_HardwareDelay1);
  Global &= tmp; // make all bits of interest 0
  tmp = (value); // only consider 12 bits
  Global |= (tmp << SHIFT_Global_HardwareDelay1)&MASK_Global_HardwareDelay1;
}

/** *******************************************************
 \brief Set global config -> RxTimeoutDisable (1 bit)
 ******************************************************** */
void ConfigReg::Set_Global_RxTimeoutDisable(bool value) // 1 bits
{
  if (value){
    Global |= MASK_Global_RxTimeoutDisable;
  } else {
    Global &= ~MASK_Global_RxTimeoutDisable;
  }
}

/** *******************************************************
 \brief Set global config -> L1Timeout (8 bits)
 ******************************************************** */
void ConfigReg::Set_Global_L1Timeout(uint8_t value) // 8 bits
{
  uint64_t tmp = ~(MASK_Global_L1Timeout);
  Global &= tmp; // make all bits of interest 0
  tmp = value;
  Global |= (tmp << SHIFT_Global_L1Timeout)&MASK_Global_L1Timeout;
}


/** *******************************************************
 \brief Set global config -> TimeoutDisable (1 bit)
 ******************************************************** */
void ConfigReg::Set_Global_L1TimeoutDisable(bool value)
{
  if (value){
    Global |= MASK_Global_TimeoutDisable;
  } else {
    Global &= ~MASK_Global_TimeoutDisable;
  }
}

/** *******************************************************
 \brief Set global config -> L2SoftL1AEn (1 bit)
 ******************************************************** */
void ConfigReg::Set_Global_L2SoftL1AEn(bool value)
{
  if (value){
    Global |= MASK_Global_L2SoftL1AEn;
  } else {
    Global &= ~MASK_Global_L2SoftL1AEn;
  }
}

/** *******************************************************
 \brief Set global config -> CalLoopNb, 16 bits
 This is the number of Strobe + L1A sequences to be sent
 \param value: 16 bits, FW > 1.C: 20 bits
 ******************************************************** */
void ConfigReg::Set_Global_CalLoopNb(uint32_t value) //
{
  if (m_FPGA_version < 0x15) {
    std::cout << "ERROR: Set_Global_CalLoopNb() not supported in firmware versions < 0x15. This is version 0x"<<std::hex<<m_FPGA_version<<std::endl;
    return;
  }
  if (m_FPGA_version < 0x1C || (m_FPGA_version > 0x110 && m_FPGA_version < 0x11C)){
    uint64_t tmp = ~(MASK_Global_CalLoopNb);
    Global &= tmp; // make all bits of interest 0
    tmp = value;
    Global |= (tmp << SHIFT_Global_CalLoopNb)&MASK_Global_CalLoopNb;
  } else {
    // bits 0 .. 19
    GlobalLoop &= ~(0xFFFFF);
    GlobalLoop |= (value & 0xFFFFF);
  }
}

/** *******************************************************
 \brief Set global config -> CalLoopDelay, 11 bits
This is the delay bewteen consecutive Strobe + L1A sequences in 100ns steps
 ******************************************************** */
void ConfigReg::Set_Global_CalLoopDelay(uint16_t value) // 11 bits
{
  if (m_FPGA_version < 0x15) {
    std::cout << "ERROR: et_Global_CalLoopDelay() not supported in firmware versions < 0x15. This is version 0x"<<std::hex<<m_FPGA_version<<std::endl;
    return;
  }
  if (m_FPGA_version < 0x1C || (m_FPGA_version > 0x110 && m_FPGA_version < 0x11C)){
    uint64_t tmp = ~(MASK_Global_CalLoopDelay);
    Global &= tmp; // make all bits of interest 0
    tmp = value;
    Global |= (tmp << SHIFT_Global_CalLoopDelay)&MASK_Global_CalLoopDelay;
  } else {
    uint32_t tmp = value;
    GlobalLoop &= ~(0x7FF00000);
    GlobalLoop |= ( (tmp << 20) & 0x7FF00000);
  }
}

/** *******************************************************
 \brief Set global loop config -> LoopDynamicDelayEn, 1 bits
Activate dynamic adjustemnt of CalLoopDelay depending on FIFO status.
 ******************************************************** */
void ConfigReg::Set_Global_DynamicCalLoopDelay(bool value) // 1 bit
{
  if (m_FPGA_version < 0x1C || (m_FPGA_version > 0x110 && m_FPGA_version < 0x11C)) {
    std::cout << "ERROR: et_Global_DynamicCalLoopDelay() not supported in firmware versions < 0x1C. This is version 0x"<<std::hex<<m_FPGA_version<<std::endl;
    return;
  }
  if (value) GlobalLoop |= 0x80000000;
  else GlobalLoop &= ~(0x80000000);
}

/** *******************************************************
 \brief Set TRB BCREn config direct param to given value
 ******************************************************** */
void ConfigReg::Set_Global_BCREn(bool value)
{
  if (value) {m_GlobalDirectParam |= TRBDirectParameter::BCREn;} //setting bit to 1, rest is unchanged
  else {m_GlobalDirectParam &= ~(TRBDirectParameter::BCREn);} //clearing bit to 0, rest is unchanged
}

/** *******************************************************
 \brief Set TRB TLBClockSel config direct param to given value
 ******************************************************** */
void ConfigReg::Set_Global_TLBClockSel(bool value)
{
  if (value) {m_GlobalDirectParam |= TRBDirectParameter::TLBClockSelect;} //setting bit to 1, rest is unchanged
  else {m_GlobalDirectParam &= ~(TRBDirectParameter::TLBClockSelect);} //clearing bit to 0, rest is unchanged
}

/** *******************************************************
 \brief Set TRB BCREn config direct param to given value
 ******************************************************** */
void ConfigReg::Set_Global_L1AEn(bool value)
{
  if (value) {m_GlobalDirectParam |= TRBDirectParameter::L1AEn;} //setting bit to 1, rest is unchanged
  else {m_GlobalDirectParam &= ~(TRBDirectParameter::L1AEn);} //clearing bit to 0, rest is unchanged
}

/** *******************************************************
 \brief Set TRB BCREn config direct param to given value
 ******************************************************** */
void ConfigReg::Set_Global_SoftCounterMuxEn(bool value)
{
  if (value) {m_GlobalDirectParam |= TRBDirectParameter::SoftCounterMuxEn;} //setting bit to 1, rest is unchanged
  else {m_GlobalDirectParam &= ~(TRBDirectParameter::SoftCounterMuxEn);} //clearing bit to 0, rest is unchanged
}

/** *******************************************************
 \brief Default configuration set in the constructor
 ******************************************************** */
PhaseReg::PhaseReg()
{
  FinePhaseClk0 = 0;
  FinePhaseClk1 = 0;
  FinePhaseLed0 = 0;
  FinePhaseLed1 = 0;
  //HardwareDelay0 = 0;
  //HardwareDelay1 = 0;
}

/** *******************************************************
 \brief Set fine phase delay for Clk0 (6 bits)
 ******************************************************** */
void PhaseReg::SetFinePhase_Clk0(unsigned int value)
{
  if (value > 63){
    std::cout << "WARNING: value passed exeeds max 6-bit value. Setting to 0x3F (63)"<<std::endl;
    value = 0x3f;
  }
  FinePhaseClk0 = value;
}

/** *******************************************************
 \brief Set fine phase delay for Clk1 (6 bits)
 ******************************************************** */
void PhaseReg::SetFinePhase_Clk1(unsigned int value)
{
  if (value > 63){
    std::cout << "WARNING: value passed exeeds max 6-bit value. Setting to 0x3F (63)"<<std::endl;
    value = 0x3f;
  }
  FinePhaseClk1 = value;
}

/** *******************************************************
 \brief Set fine phase delay for Led0 (6 bits)
 ******************************************************** */
void PhaseReg::SetFinePhase_Led0(unsigned int value)
{
  if (value > 63){
    std::cout << "WARNING: value passed exeeds max 6-bit value. Setting to 0x3F (63)"<<std::endl;
    value = 0x3f;
  }
  FinePhaseLed0 = value;
}

/** *******************************************************
 \brief Set fine phase delay for Led1 (6 bits)
 ******************************************************** */
void PhaseReg::SetFinePhase_Led1(unsigned int value)
{
  if (value > 63){
    std::cout << "WARNING: value passed exeeds max 6-bit value. Setting to 0x3F (63)"<<std::endl;
    value = 0x3f;
  }
  FinePhaseLed1 = value;
}



/** *******************************************************
 \brief Returns a vector of 2 16-bit words to be used as payload when writing config to the TRB.
 1 MSB is the frame number, remaining 15 LSB contain the config bits.
 ******************************************************** */
std::vector<uint16_t> PhaseReg::GetPayloadVector()
{
  std::vector<uint16_t> output;
  uint16_t word = 0, tmp = 0;
  
  word = 0; // wordID 0
  word = (FinePhaseClk0 & 0x3F); // 6-bits
  tmp = (FinePhaseClk1 & 0x3F); // use only 6-bits
  tmp <<= 6;
  word |= tmp; // use only 6-bits
  tmp = (FinePhaseLed0  & 0x7); // use only 3-bits
  tmp <<= 12;
  word |= tmp;
  
  output.push_back(word);
  
  word = 0x8000; //  wordID 1
  tmp  = (FinePhaseLed0 & 0x3F); // 6-bit value
  tmp >>= 3; // remaining 3 bits out of 6
  word |= tmp;
  tmp = (FinePhaseLed1& 0x3F); // 6 bits
  tmp <<=3;
  word |= tmp;
  /*
  tmp = (HardwareDelay0 & 0x7); // only use 3-bits
  tmp <<= 3+6;
  word |= tmp ;
  tmp = (HardwareDelay1 & 0x7); // only use 3-bits
  tmp <<= 3+6+3;
  word |= tmp;
  */
  output.push_back(word);
  
  return output;
}

/** *******************************************************
 \brief Default configuration set in the constructor
 ******************************************************** */
SCT_SlowCommandReg::SCT_SlowCommandReg()
{
  Field3 = 0xC; //
  Field4 = 0x3F; // all chips
  Field5 = 0x28; // default for enabling DT mode
  Field6.resize(10); //  we will always upload 10x16 configuration bits to fill the register in the TRB
}

/** *******************************************************
\brief Set Field 3 - command type (8-bits)
******************************************************** */
void SCT_SlowCommandReg::SetField3(unsigned int value)
{
  if (value > 0xff){
    std::cout << "WARNING: value passed exeeds max 8-bit value. Setting to 0xff (255)"<<std::endl;
    value = 0xff;
  }
  Field3 = value;
}

/** *******************************************************
 \brief Set Field 4 - chip address (6-bits)
 ******************************************************** */
void SCT_SlowCommandReg::SetField4(unsigned int value)
{
  if (value > 0x3f){
    std::cout << "WARNING: value passed exeeds max 6-bit value. Setting to 0x3f (63)"<<std::endl;
    value = 0x3f;
  }
  Field4 = value;
}

/** *******************************************************
 \brief Set Field 5 - command  (6-bits)
 ******************************************************** */
void SCT_SlowCommandReg::SetField5(unsigned int value)
{
  if (value > 0x3f){
    std::cout << "WARNING: value passed exeeds max 6-bit value. Setting to 0x3f (63)"<<std::endl;
    value = 0x3f;
  }
  Field5 = value;
}

/** *******************************************************
 \brief Set Field 6 - configuration data
 This sets the first 16-bit word fo configuration data, sufficient for all settings
 except strip mask and calibration pulse generation
 ******************************************************** */
void SCT_SlowCommandReg::SetField6(unsigned int value)
{
  Field6.clear();
  if (value > 0xffff){
    std::cout << "WARNING: value passed exeeds max 16-bit value. Setting to 0xffff"<<std::endl;
    value = 0xffff;
  }
  Field6.push_back(value);
}

/** *******************************************************
 \brief Set Field 6 - configuration data
 This sets up to 10 16-bit word containing the configuration data.
 Used to configure the strip mask and issue calibration pulses
 ******************************************************** */
void SCT_SlowCommandReg::SetField6(std::vector<uint16_t> values) // The Mask register requires 128 bits
{
  Field6.clear();
  if (values.size() > 10){
    std::cout << "WARNING: too many 16-bit configuration words provided, only 10 allowed at most."<<std::endl;
    values.resize(10);
  }
  Field6 = values;
}

/** *******************************************************
 \brief Returns a vector of 15 16-bit words to be used as payload when writing to the SCT Slow command register in the TRB.
 4 MSB are used as frame number, remaining 12 LSB contain the config bits. LSB of payload bits is sent first in bit-stream!
 I.e.: LSB word 0 = MSB Field 3
 ******************************************************** */
std::vector<uint16_t> SCT_SlowCommandReg::GetPayloadVector()
{
  std::vector<uint16_t> output;
  uint16_t word = 0, tmp = 0;
  
  word = 0; // wordID 0
  word = (reverseBitOrder(Field3) ); // 8-bits
  tmp = ( (reverseBitOrder(Field4) >> 2) & 0xF ); // 6 bit field, use only 4-bits
  tmp <<= 8;
  word |= tmp; // use only 6-bits
 
  output.push_back(word);
  
  word = 0x1000; // wordID 1
  tmp = (reverseBitOrder(Field4) >> 6 ); // remaining 2 bits
  // tmp <<= 8;
  word |= tmp;
  tmp = (reverseBitOrder(Field5) >>2); // all 6-bits of Field5
  tmp <<= 2;
  word |= tmp;
  
  //
  // start writing field 6
  //
  uint16_t wordID = 0x2000;
  uint16_t wordIDincr = 0x1000;
  // std::cout << "Field 6      = 0x"<<std::hex<<Field6[0]<<std::endl;
  // std::cout << "rev(Field 6) = 0x"<<std::hex<<reverseBitOrder(Field6[0])<<std::endl;
  for (unsigned int id = 0; id < Field6.size(); id +=3) {
    tmp = (reverseBitOrder(Field6[id]) & 0xf); // 4-bits of Field6
    tmp <<= 8;
    word |= (tmp & 0xFFF);
    
    output.push_back(word);
    
    word = wordID;
    wordID += wordIDincr;
    tmp = reverseBitOrder(Field6[id]);
    tmp >>= 4; // 4 bits already used, will write remaining 12
    word |= (tmp & 0xFFF); // add at most 12 LSB
    
    output.push_back(word);
    
    if (id == Field6.size() - 1){
      // exit loop when the last full 16-bit word is written. This happens usually fir id=9, i.e. after the 10th 16-bit word of field6 is written
      break;
    }
    word = wordID;
    wordID += wordIDincr;
    tmp = reverseBitOrder(Field6[id+1]);
    word |= (tmp & 0xFFF); // add at most 12 LSB
    
    output.push_back(word);
    
    word = wordID;
    wordID += wordIDincr;
    tmp = reverseBitOrder(Field6[id+1]);
    tmp >>= 12; // write remaining 4 MSB
    word |= (tmp & 0xFFF); // add at most 12 LSB
    tmp = reverseBitOrder(Field6[id+2]);
    tmp <<= 4; //
    word |= (tmp & 0xFFF); // add at most 12 LSB -> 8 bits
    
    output.push_back(word);
    
    word = wordID;
    wordID += wordIDincr;
    tmp = reverseBitOrder(Field6[id+2]);
    tmp >>= 8; // write remaining 8 MSB
    word |= (tmp & 0xFFF); // add at most 12 LSB
    
  }
  return output;
}

/** \brief Issues a calibration pulse followed by a L1A after delay <= 157 clock cycles.
 */
void SCT_SlowCommandReg::CalibrationPulse(unsigned int delay, unsigned int chipID)
{
  if (delay > 157){
    std::cout << "WARNING: CalibrationPulse(): maximum delay between calibratino pulse and L1A is 157BC. Set to 157BC."<<std::endl;
    delay = 157;
  }
  
  SetField3(CMD_CLASS_DT);
  SetField4(chipID);
  SetField5(IssueCalibPusle);
  
  std::vector<uint16_t> field6Words;
  uint16_t word = 0x0;
  while (delay >=  16){
    field6Words.push_back(word); // 16BCs delay
    delay -= 16;
  }
  word = 0xC000;
  word >>= delay;
  field6Words.push_back(word);
  if (delay >= 14){
    word = 0xC000;
    word <<= (16-delay);
    field6Words.push_back(word);
  }
  
  SetField6(field6Words);
}
