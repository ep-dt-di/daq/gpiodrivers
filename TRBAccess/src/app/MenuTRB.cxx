/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 

#include "../../TrackerReadout/TRBAccess.h"
#include "../../TrackerReadout/TRBEventDecoder.h"
#include "GPIOBase/DummyInterface.h"
#include "../../TrackerReadout/ConfigurationHandling.h"
#include "../../TrackerReadout/ROR_Header.h"
// #include "cxxopts.hpp" -> requires gcc version >= 4.9
#include <unistd.h>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <chrono>
#include <iomanip>

using namespace FASER;
using std::cout;
using std::endl;
using std::cin;

unsigned int m_moduleMask;
int TRBID;
TRBAccess *trb = nullptr;
bool useEthernet=true;
int m_calLoopDelay = 1000;
bool m_dynamicCalLoopDelay = true;
int m_calLoopTriggers = 50; 

void SetupTRB ()
{
  // check if module mask makes sense:
  int finePhaseDelay = 55;

  if (m_moduleMask > 0xff) {
    std::cout << "ERROR: We hvae at most 8 modules per TRB, but you specified 0x"<<std::hex<<m_moduleMask<< " as moduleMask!"<<std::endl;
    return;
  }
  if (useEthernet) {
    //trb = new TRBAccess("10.11.65.7", "10.11.65.7", 0, true,  TRBID); ((dummyInterface*)(trb->m_interface))->SetInputFile("CalibrationLoop_in.daq");
    trb = new TRBAccess("10.11.65.6", "10.11.65.6", 0, false,  TRBID);
  } else {
    std::cout << "Connecting to TRB via USB"<<std::endl;
    trb = new TRBAccess(0, false,  TRBID);
  }
  trb->SetDebug(false);
  trb->ShowDataRate(true);

  trb->SetDirectParam(TRBDirectParameter::L1CounterReset|TRBDirectParameter::FifoReset|TRBDirectParameter::ErrCntReset);
  
  trb->GetConfig()->Set_Module_L1En(0x0); // disable hardware L1A for all modules
  trb->GetConfig()->Set_Module_ClkCmdSelect(0x0); // all modules use CMD_0
  trb->GetConfig()->Set_Module_LedRXEn(m_moduleMask); // enable led lines
  trb->GetConfig()->Set_Module_LedxRXEn(m_moduleMask); // enable ledx lines
  
  trb->GetConfig()->Set_Global_RxTimeoutDisable(true);
  trb->GetConfig()->Set_Global_L1TimeoutDisable(false);
  trb->GetConfig()->Set_Global_L2SoftL1AEn(true);
  trb->GetConfig()->Set_Global_Overflow(4095);
  
  trb->WriteConfigReg();
  trb->SetDirectParam(TRBDirectParameter::L1CounterReset);
  trb->SetDirectParam(TRBDirectParameter::SoftCounterMuxEn);

  trb->GetPhaseConfig()->SetFinePhase_Clk0(0);
  trb->GetPhaseConfig()->SetFinePhase_Led0(finePhaseDelay);
  trb->GetPhaseConfig()->SetFinePhase_Clk1(0);
  trb->GetPhaseConfig()->SetFinePhase_Led1(finePhaseDelay);

  trb->WritePhaseConfigReg();
  trb->ApplyPhaseConfig();
}

/** \brief This function configures given modules.
 * If no configuration is specidfied the default config will be used.
 * \param moduleMask: bit mask for modules to be used
 * \param configFile: (optional) JSON file holding module configuration
 */
void ConfigureModules (std::string configFile)
{
  if (trb == nullptr){
    std::cout << "Setup TRB first! "<<std::endl;
    return;
  }
  std::cout << "Loading configuration for modules 0x"<<std::hex<<m_moduleMask <<std::endl;
  ConfigHandlerSCT *cfg = new ConfigHandlerSCT();
  if (configFile != ""){
    cfg->ReadFromFile(configFile);
  }
  std::cout << "Writing strip mask for calMode 0 and setting calMode to 0"<<std::endl;
  trb->SCT_WriteStripmask(m_moduleMask, 0);
  cfg->SetCalMode(0);
  trb->ConfigureSCTModule(cfg, m_moduleMask);
  
  std::cout << "Setting threshold to 45 and calibration charge to 0x40"<<std::endl;
  uint16_t thrValue = 45<<8;   // threshold is stored in the 8 MSB
  thrValue |= 0x40;             // 0x40 = 4fC calibration charge to be injected is set in the 8 LSB
  trb->GetSCTSlowCommandBuffer()->SetThreshold(0x3f, thrValue);
  trb->WriteSCTSlowCommandBuffer();
  trb->SendSCTSlowCommandBuffer(m_moduleMask);
  
  std::cout << "Setting strobe delay to 15"<<std::hex<<m_moduleMask<<std::endl;
  trb->GetSCTSlowCommandBuffer()->SetStrobeDelay(0x3f, 15);
  trb->WriteSCTSlowCommandBuffer();
  trb->SendSCTSlowCommandBuffer(m_moduleMask);
}

/** \brief Set a specified threshold to all active modules
 */
void SetThreshold(unsigned int thr)
{
  if (trb == nullptr){
    std::cout << "Setup TRB first! "<<std::endl;
    return;
  }
  
  uint16_t thrValue = thr<<8;   // threshold is stored in the 8 MSB
  std::cout << "Setting threshold to 0x"<<std::hex<<thrValue<<std::dec<<std::endl;
  trb->GetSCTSlowCommandBuffer()->SetThreshold(0x3f, thrValue);
  trb->WriteSCTSlowCommandBuffer();
  trb->SendSCTSlowCommandBuffer(m_moduleMask);
}

void startDataTaking()
{
  std::stringstream outfileName;
  outfileName << "/var/tmp/DataTaking.daq";
  trb->SetupStorageStream(outfileName.str());
  
  /*
  trb->SetDirectParam(TRBDirectParameter::L1CounterReset);

  //Testing PLL error counter before TLBClockSel true
  int PLLAttempts = 10;
  for(int i = 0; i < PLLAttempts; i++){
    uint16_t PLLErrorCount;
    PLLErrorCount = trb->ReadPLLErrorCounter();
    std::cout << "PLL error counter value when TLBClockSel false (" << i << " of " << PLLAttempts << "): " << PLLErrorCount << std::endl;
    usleep(100);
  }
  
  // enable hardware L1A
  trb->GetConfig()->Set_Module_L1En(m_moduleMask);
  trb->GetConfig()->Set_Module_BCREn(m_moduleMask);
  trb->GetConfig()->Set_Global_L2SoftL1AEn(false);
  trb->WriteConfigReg();
  trb->SetDirectParam(TRBDirectParameter::L1AEn | TRBDirectParameter::BCREn | TRBDirectParameter::TLBClockSelect);
  trb->PrintStatus();
  
  for(unsigned int i = 5; i >0; i--){
    uint16_t status;
    trb->ReadStatus(status);
    if ((status & TRBStatusParameters::STATUS_TLBCLKSEL)){ // check if TLBClkSel is TRUE
      std::cout << "   TLB CLK synchronized"<<std::endl;
      break;
    }
    std::cout << "    TLB clock NOT OK ..."<<std::endl;
    sleep(1);
  }
  
  //Testing PLL error counter after TLBClockSel is true
  for(int i = 0; i < PLLAttempts; i++){
    uint16_t PLLErrorCount;
    PLLErrorCount = trb->ReadPLLErrorCounter();
    std::cout << "PLL error counter value when TLBClockSel true (" << i << " of " << PLLAttempts << "): " << PLLErrorCount << std::endl;
    usleep(100);
  }

  std::cout << "TRB configuration at start of DAQ: "<<std::endl;
  ConfigReg actualCfg;
  actualCfg.FillFromPayload(trb->ReadConfigReg());
  actualCfg.Print();
  
  // start data taking
  trb->FIFOReset();
  trb->SCT_EnableDataTaking(m_moduleMask);
  trb->GenerateSoftReset(m_moduleMask);
  */
  // start readout
  std::cout << "starting readout"<<std::endl;
  trb->StartReadout();
  
  //actualCfg.FillFromPayload(trb->ReadConfigReg());
  //actualCfg.Print();
}


void stopDataTaking()
{
  trb->StopReadout();
}

/** \brief Rcord N events, software triggered
 */
void RecordData(int nL1A)
{
  if (trb == nullptr){
    std::cout << "Setup TRB first! "<<std::endl;
    return;
  }
  
  std::stringstream outfileName;
  outfileName << "/var/tmp/DataTaking.daq";
  trb->SetupStorageStream(outfileName.str());
  //trb->GenerateL1A(m_moduleMask); // output module config
  
  trb->SCT_WriteStripmask(m_moduleMask, 0);
  //scanConfig->CalMode = calMode;
  ConfigHandlerSCT *cfg = new ConfigHandlerSCT();
  cfg->SetCalMode(0);
  trb->ConfigureSCTModule(cfg, m_moduleMask);
 
  uint16_t thrValue = 45<<8;   // threshold is stored in the 8 MSB
  thrValue |= 0x40;             // 0x40 = 4fC calibration charge to be injected is set in the 8 LSB
  trb->GetSCTSlowCommandBuffer()->SetThreshold(0x3f, thrValue);
  trb->WriteSCTSlowCommandBuffer();
  trb->SendSCTSlowCommandBuffer(m_moduleMask);

  trb->GetSCTSlowCommandBuffer()->SetStrobeDelay(0x3f, 18);
  trb->WriteSCTSlowCommandBuffer();
  trb->SendSCTSlowCommandBuffer(m_moduleMask);

  trb->FIFOReset();
  trb->SCT_EnableDataTaking(m_moduleMask);
  trb->GenerateSoftReset(m_moduleMask);
  trb->StartReadout();
  trb->SetDebug(false); 
 
  // send some L1As
  for (unsigned i = 0; i < nL1A; i++){
    // Put TRB & module into data taking mode
    //trb->SCT_CalibrationPulse(modBitmask);
    if ((i % 5000) == 0) std::cout << "Triggers sent: "<<std::dec<<i<<std::endl;
    //usleep(100);
    trb->SCT_CalibrationPulse(m_moduleMask, 130);
    //trb->GenerateL1A(m_moduleMask);
  }
  
  // stop DT
  sleep(1); //sleep too long - causes problems with ethernet
  std::cout << "Readout stopped "<<std::endl;
  trb->StopReadout();
}

void RunSafePhaseDetection()
{
  const int N_STEPS = 64;
  const int N_FINE_PHASE_CLK = 5;

  
  std::cout << "====================================================================" << std::endl << std::endl;
  std::cout << "BEFORE CONTINUING MAKE SURE THAT:" << std::endl << std::endl;
  std::cout << " 1.- A module is connected to trbChannel 0" << std::endl;
  std::cout << " 2.- The module has been power-cycled" << std::endl << std::endl;
  std::cout << "====================================================================" << std::endl;
  std::cout << "When above is done, press enter to continue..." << std::endl;
  // while(std::cin.get()!='\n');

   int v0[N_FINE_PHASE_CLK][N_STEPS];
   int v1[N_FINE_PHASE_CLK][N_STEPS];
   for(int i=0; i<N_FINE_PHASE_CLK; i++){
     for(int j=0; j<N_STEPS; j++){
       v0[i][j]=-1;
       v1[i][j]=-1;
     }
   }

  PhaseReg *phaseReg = trb->GetPhaseConfig();
  
  uint16_t status;
  unsigned int finePhaseClk[N_FINE_PHASE_CLK]={0,16,32,48, 63};
  for(unsigned int idx=0; idx<N_FINE_PHASE_CLK; idx++){
    std::cout << "[" << idx << "] running FinePhaseClk = " 
	      << std::dec << finePhaseClk[idx] << "..." << std::endl;
    phaseReg->SetFinePhase_Clk0(finePhaseClk[idx]);
    phaseReg->SetFinePhase_Clk1(finePhaseClk[idx]);
    trb->WritePhaseConfigReg();
    trb->ApplyPhaseConfig();

    usleep(1e5);
    
    for(unsigned int step=0; step<64; step++){
      phaseReg->SetFinePhase_Led0(step);
      phaseReg->SetFinePhase_Led1(step);
      trb->WritePhaseConfigReg();
      trb->ApplyPhaseConfig();

      usleep(1e5);

      trb->ReadStatus(status);
      v0[idx][step] = int(status & TRBStatusParameters::STATUS_SAFEPHASEDET0) != 0 ? 1 : 0;
      v1[idx][step] = int(status & TRBStatusParameters::STATUS_SAFEPHASEDET1) != 0 ? 1 : 0;
    }
  }


  for ( unsigned j = 0; j< N_FINE_PHASE_CLK; j++ ){
    std::cout << "----------------" << std::endl;
    std::cout << "Results for CLK fine phase "<<finePhaseClk[j]<<": " << std::endl;
    unsigned int idx0(0), idxbef0(0), transition0(0);
    unsigned int idx1(0), idxbef1(0), transition1(0);
    for(unsigned int step=0; step<64; step++){
      idx0 = v0[j][step];
      idx1 = v1[j][step];
      
      if(step >= 1){
        idxbef0 = v0[j][step-1];
        if(idx0 != idxbef0){
          transition0=step-1;
        }
        idxbef1 = v1[j][step-1];
        if(idx1 != idxbef1){
          transition1=step-1;
        }
      }

      std::cout << " step " << std::setfill('0') << std::setw(2) << std::dec << step << " =>";    
      std::cout << " SafePhaseDetect [0] = [" << v0[j][step] <<  "] ;";
      std::cout << " SafePhaseDetect [1] = [" << v1[j][step] <<  "]";
      std::cout << std::endl;
    }

    cout << "Transition for Led0 at : " << transition0 << endl;
    cout << "Transition for Led1 at : " << transition1 << endl;

    cout << endl;
    cout << "---------------------------------------------" << endl;
    cout << "Optimal finePhase led 0 for input CLK finePhase "<<finePhaseClk[j]<<" = " << (transition0 + 32) % 64 << endl;
    cout << "---------------------------------------------" << endl;
    cout << "---------------------------------------------" << endl;
    cout << "Optimal finePhase led 1 for input CLK finePhase "<<finePhaseClk[j]<<" = " << (transition1 + 32) % 64 << endl;
    cout << "---------------------------------------------" << endl;
  }

  cout << endl << "Bye ! " << endl;

  
}

/*** Run the strobe delay scan
  * will do on the fly decoding of occupency */
void RunStrobeDelayScan()
{
  auto time_start = std::chrono::high_resolution_clock::now();
  auto time_end = std::chrono::high_resolution_clock::now();
  std::cout << " Configuring modules "<<std::endl;
   ConfigHandlerSCT *cfg = new ConfigHandlerSCT();
   // cfg->SetReadoutMode(2); // on top of default config set ReadoutMode to 01X = edge (physics)
   // cfg->SetEdgeDetection(1);
   int calMode = 0;
   cfg->SetCalMode(calMode);
   trb->SetupStorageStream("/var/tmp/StrobeDelayScan.daq");
   trb->ConfigureSCTModule(cfg, m_moduleMask); //  explicitely specify channel
   std::cout << "   ... Done."<<std::endl;
  
   ScanStepConfig *scanConfig = new ScanStepConfig; // This object is used to store the scan configuration in every scan step
   
   for (unsigned int calMode = 0; calMode < 4; calMode++){

     std::cout << std::endl<<"Mask Step: "<< calMode<<std::endl<<std::endl;
     // write Stripmask to module and store stripMask steip in scan config
     trb->SCT_WriteStripmask(m_moduleMask, calMode);
     scanConfig->CalMode = calMode;
     cfg->SetCalMode(calMode);
     trb->ConfigureSCTModule(cfg, m_moduleMask);
     
     uint16_t thrValue = 45<<8;   // threshold is stored in the 8 MSB
     thrValue |= 0x40;             // 0x40 = 4fC calibration charge to be injected is set in the 8 LSB
     trb->GetSCTSlowCommandBuffer()->SetThreshold(0x3f, thrValue);
     trb->WriteSCTSlowCommandBuffer();
     trb->SendSCTSlowCommandBuffer(m_moduleMask);

     
     // This is the loop over the threshold setting
     for (unsigned int delay = 0; delay < 64; delay += 2){
       int expectedL1A = 200; // here you can set how many triggers are sent in every scan step
       std::cout << "StrobeDelay scan loop: "<<std::dec<<delay<<std::endl;
       time_start = std::chrono::high_resolution_clock::now();
       // set configuration
       scanConfig->Threshold = delay;
       scanConfig->ExpectedL1A = expectedL1A;
       trb->SaveScanStepConfig(*scanConfig); // this writes the scan step config to the data file
       
       trb->GetSCTSlowCommandBuffer()->SetStrobeDelay(0x3f, delay);
       trb->WriteSCTSlowCommandBuffer();
       trb->SendSCTSlowCommandBuffer(m_moduleMask);
       
       // start data taking
       trb->FIFOReset();
       trb->SCT_EnableDataTaking(m_moduleMask);
       trb->GenerateSoftReset(m_moduleMask);
       
       // slepp a bit to avoid communication errors, could be tuned ...
       trb->StartReadout();
       time_end = std::chrono::high_resolution_clock::now();
       auto duration = std::chrono::duration_cast<std::chrono::microseconds>(time_end - time_start);
       std::cout << "Timing: setup tribe delay step: "<<duration.count()<<" µs"<<std::endl;
       time_start = std::chrono::high_resolution_clock::now();
       // send some L1As and monitor the L1A rate
       
       for (unsigned i = 0; i < expectedL1A; i++){
         // send a calibration pulse followed by an L1A after 130 BCs
         trb->SCT_CalibrationPulse(m_moduleMask, 130);
         
         // Adding some sleep after every L1A, could be tuned
         //usleep(600);
         if (i % 10000 == 0 && i != 0){
           time_end = std::chrono::high_resolution_clock::now();
          duration = std::chrono::duration_cast<std::chrono::microseconds>(time_end - time_start);
           std::cout << "L1A cnt: "<<i<<" Trigger Rate = "<<10.0e6/duration.count()<<" kHz"<<std::endl;
           time_start = std::chrono::high_resolution_clock::now();
         }
       }
       time_end = std::chrono::high_resolution_clock::now();
      duration = std::chrono::duration_cast<std::chrono::microseconds>(time_end - time_start);
       std::cout << "Timing: TriggerPhase: "<<duration.count()<<" µs"<<std::endl;
       time_start = std::chrono::high_resolution_clock::now();
       std::cout << "Readout stopped "<<std::endl;
       trb->StopReadout();
       // wait for last events to arrive
       usleep(20000);
       std::cout << "Decoding data"<<std::endl;
       time_end = std::chrono::high_resolution_clock::now();
        duration = std::chrono::duration_cast<std::chrono::microseconds>(time_end - time_start);
       std::cout << "Timing: Stopping DAQ: "<<duration.count()<<" µs"<<std::endl;
       time_start = std::chrono::high_resolution_clock::now();
       TRBEventDecoder ed;
       ed.LoadTRBEventData(trb->GetTRBEventData());
       std::vector<int> hitsPerModule;
       hitsPerModule.resize(8);
       for (auto event : ed.GetEvents() ) {
         for (unsigned int mod = 0; mod < 8; mod++){
           auto module = event->GetModule(mod);
           if (module == nullptr) continue;
           hitsPerModule[mod] += module->GetNHits();
         }
       }
       std::cout << "Strobe Delay = "<<delay << "  Hits per module: ";
       for (auto hits : hitsPerModule) std::cout <<hits<<" ";
       std::cout << std::endl;
       time_end = std::chrono::high_resolution_clock::now();
       duration = std::chrono::duration_cast<std::chrono::microseconds>(time_end - time_start);
       std::cout << "Timing: Decoding data: "<<duration.count()<<" µs"<<std::endl;
       time_start = std::chrono::high_resolution_clock::now();
     }
   }
   
   delete cfg;
}


/*** Run the strobe delay scan
  * will do on the fly decoding of occupency */
void RunStrobeDelayScan_CalLoop()
{
  auto time_start = std::chrono::high_resolution_clock::now();
  auto time_end = std::chrono::high_resolution_clock::now();
  
  trb->GetConfig()->Set_Global_CalLoopNb(200);
  trb->GetConfig()->Set_Global_CalLoopDelay(2000);
  trb->WriteConfigReg();
  
  std::cout << " Configuring modules "<<std::endl;
   ConfigHandlerSCT *cfg = new ConfigHandlerSCT();
   // cfg->SetReadoutMode(2); // on top of default config set ReadoutMode to 01X = edge (physics)
   // cfg->SetEdgeDetection(1);
   int calMode = 0;
   cfg->SetCalMode(calMode);
   trb->ConfigureSCTModule(cfg, m_moduleMask); //  explicitely specify channel
   std::cout << "   ... Done."<<std::endl;
  
   ScanStepConfig *scanConfig = new ScanStepConfig; // This object is used to store the scan configuration in every scan step
   
   for (unsigned int calMode = 0; calMode < 4; calMode++){

     std::cout << std::endl<<"Mask Step: "<< calMode<<std::endl<<std::endl;
     // write Stripmask to module and store stripMask steip in scan config
     trb->SCT_WriteStripmask(m_moduleMask, calMode);
     scanConfig->CalMode = calMode;
     cfg->SetCalMode(calMode);
     trb->ConfigureSCTModule(cfg, m_moduleMask);
     
     uint16_t thrValue = 45<<8;   // threshold is stored in the 8 MSB
     thrValue |= 0x40;             // 0x40 = 4fC calibration charge to be injected is set in the 8 LSB
     trb->GetSCTSlowCommandBuffer()->SetThreshold(0x3f, thrValue);
     trb->WriteSCTSlowCommandBuffer();
     trb->SendSCTSlowCommandBuffer(m_moduleMask);

     
     // This is the loop over the threshold setting
     for (unsigned int delay = 0; delay < 64; delay += 2){
       int expectedL1A = 50; // here you can set how many triggers are sent in every scan step
       std::cout << "StrobeDelay scan loop: "<<std::dec<<delay<<std::endl;
       time_start = std::chrono::high_resolution_clock::now();
       // set configuration
       scanConfig->Threshold = delay;
       scanConfig->ExpectedL1A = expectedL1A;
       trb->SaveScanStepConfig(*scanConfig); // this writes the scan step config to the data file
       
       trb->GetSCTSlowCommandBuffer()->SetStrobeDelay(0x3f, delay);
       trb->WriteSCTSlowCommandBuffer();
       trb->SendSCTSlowCommandBuffer(m_moduleMask);
       
       // start data taking
       trb->FIFOReset();
       trb->SCT_EnableDataTaking(m_moduleMask);
       trb->GenerateSoftReset(m_moduleMask);
       
       // slepp a bit to avoid communication errors, could be tuned ...
       trb->StartReadout();
       time_end = std::chrono::high_resolution_clock::now();
       auto duration = std::chrono::duration_cast<std::chrono::microseconds>(time_end - time_start);
       std::cout << "Timing: setup tribe delay step: "<<duration.count()<<" µs"<<std::endl;
       time_start = std::chrono::high_resolution_clock::now();
       // send some L1As and monitor the L1A rate
       
       
         // send a calibration pulse followed by an L1A after 130 BCs
      trb->SCT_CalibrationPulse(m_moduleMask, 130, true);
       while (trb->IsCalibrationLoopRunning()){
         //std::cout << "Calib Loop still running "<<std::endl;
         usleep(10);
       }
 
      
       time_end = std::chrono::high_resolution_clock::now();
      duration = std::chrono::duration_cast<std::chrono::microseconds>(time_end - time_start);
       std::cout << "Timing: TriggerPhase: "<<duration.count()<<" µs"<<std::endl;
       time_start = std::chrono::high_resolution_clock::now();
       // stop data taking
       //usleep(10000);
       std::cout << "Readout stopped "<<std::endl;
       trb->StopReadout();
       // wait for last events to arrive
       usleep(90000);
       std::cout << "Decoding data"<<std::endl;
       time_end = std::chrono::high_resolution_clock::now();
        duration = std::chrono::duration_cast<std::chrono::microseconds>(time_end - time_start);
       std::cout << "Timing: Stopping DAQ: "<<duration.count()<<" µs"<<std::endl;
       time_start = std::chrono::high_resolution_clock::now();
       TRBEventDecoder ed;
       ed.LoadTRBEventData(trb->GetTRBEventData());
       std::vector<int> hitsPerModule;
       hitsPerModule.resize(8);
       for (auto event : ed.GetEvents() ) {
         for (unsigned int mod = 0; mod < 8; mod++){
           auto module = event->GetModule(mod);
           if (module == nullptr) continue;
           hitsPerModule[mod] += module->GetNHits();
         }
       }
       std::cout << "Strobe Delay = "<<delay << "  Hits per module: ";
       for (auto hits : hitsPerModule) std::cout <<hits<<" ";
       std::cout << std::endl;
       time_end = std::chrono::high_resolution_clock::now();
       duration = std::chrono::duration_cast<std::chrono::microseconds>(time_end - time_start);
       std::cout << "Timing: Decoding data: "<<duration.count()<<" µs"<<std::endl;
       time_start = std::chrono::high_resolution_clock::now();
     }
   }
   
   delete cfg;
}


void RunL1ALoop()
{

   std::ofstream outfile;
   outfile.open("/var/tmp/L1ALoop.trb", std::ios::out | std::ofstream::binary);
   
   trb->SCT_WriteStripmaskAllON(m_moduleMask);
   
   trb->FIFOReset();
   trb->SCT_EnableDataTaking(m_moduleMask);
   trb->GenerateSoftReset(m_moduleMask);
   trb->StartReadout();
   ///
   auto start = std::chrono::high_resolution_clock::now();
   std::cout << "L1A loop started " <<std::endl;
   trb->GenerateL1A(m_moduleMask, true /* Loop*/);
   while (trb->IsCalibrationLoopRunning())
   {
      char c;
      std::cin.get(c);
      if (c == 'a'){ // abort loop
         trb->AbortLoop();
      }
      auto data = trb->GetTRBEventData();
      std::cout << "Writing "<<data.size()<<" events"<<std::endl;
      for (auto &event : data){
         outfile.write(reinterpret_cast<const char *>(&event[0]), event.size()*sizeof(uint32_t));
      }
    
      usleep(100);
   }
   auto stop = std::chrono::high_resolution_clock::now();
   std::cout << "L1A Loop ended"<<std::endl;
   usleep(100);
   auto data = trb->GetTRBEventData();
   for (auto &event : data){
      outfile.write(reinterpret_cast<const char *>(&event[0]), event.size()*sizeof(uint32_t));
   }
   outfile.close();
   auto stopWriting = std::chrono::high_resolution_clock::now();
   ///
   auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
   std::cout << "   after "<<(double)duration.count()/1000<<" ms"<<std::endl;
   trb->StopReadout();
   duration = std::chrono::duration_cast<std::chrono::microseconds>(stopWriting - stop );
   std::cout << "   Writing stopped"<<(double)duration.count()/1000<<" ms later"<<std::endl;

}

int PrintMenuCalLoop(int level)
{
  int choice;
  auto start = std::chrono::high_resolution_clock::now();
  auto stop = std::chrono::high_resolution_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
  TRBEventDecoder *ed;
  int errors = 0;
  int runCounter = 0;
  while (1){
  
  std::stringstream outfileName;
  
  cout << "Calibration Loop config: "<<endl;
  cout << " 0: Main Menu"<<std::endl;
  cout << " 1: RunCalibLoop (with charge injection) "<<std::endl;
  cout << " 2: Run L1A loop (no charge injection)"<<std::endl;
  
  cout << " 3: Set Delay (default = 1000) " << endl;
  cout << " 4: Set Nr of triggers (default = 50) " << endl;
  cout << " 5: Set DynamicDelay ON/OFF " << endl;
  cout << " 9: Is calibration loop running "<< endl;
  cout << "--------------------------------------"<<endl;
  std::cout << "Your choice: ";
  std::cin >> choice;
  cin.get(); 
      switch (choice) {
        case 0:
          return 0;
          break;
          
        case 1:
          
          if (trb == nullptr){
            std::cout << "Setup TRB first! "<<std::endl;
            return 0;
          }
          outfileName << "/var/tmp/CalibrationLoop_"<<runCounter<<".daq";
          runCounter++;
          trb->SetupStorageStream(outfileName.str());

          std::cout << "starting calibration Loop with "<<m_calLoopTriggers<<" triggers and delay = "<<m_calLoopDelay
                    << " Dynamic CalLoopDelay = "<<m_dynamicCalLoopDelay <<std::endl;
          
          trb->GetConfig()->Set_Global_CalLoopNb(m_calLoopTriggers);
	        trb->GetConfig()->Set_Global_CalLoopDelay(m_calLoopDelay);
          trb->GetConfig()->Set_Global_DynamicCalLoopDelay(m_dynamicCalLoopDelay);
	        trb->WriteConfigReg();

          trb->FIFOReset();
          trb->SCT_EnableDataTaking(m_moduleMask);
          trb->GenerateSoftReset(m_moduleMask);
          trb->StartReadout();
          ///
          start = std::chrono::high_resolution_clock::now();
	       for (unsigned int i = 0; i < 10; i++){
          trb->SCT_CalibrationPulse(m_moduleMask, 
				    130 /* strobe-L1A delay*/,
				    true /* CalLoop*/);
	  
          std::cout << "calibration loop started " <<std::endl;
          while (trb->IsCalibrationLoopRunning())
          {
            //std::cout << "Calib Loop still running "<<std::endl;
             usleep(10);
          }
          std::cout << "Calibration Loop ended"<<std::endl;
          }
          ///
          stop = std::chrono::high_resolution_clock::now();
          duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
          std::cout << "   after "<<(double)duration.count()/1000<<" ms"<<std::endl;
          sleep (2);
          std::cout << "Readout stopped "<<std::endl;
          trb->StopReadout();
          // decoding events on the fly for timing: 
          start = std::chrono::high_resolution_clock::now();
          /*
          ed = new TRBEventDecoder();
          ed->LoadTRBEventData(trb->GetTRBEventData());
          
          std::cout << "Decoded "<<std::dec<<ed->GetEvents().size()<< " Events in ";
          stop = std::chrono::high_resolution_clock::now();
          duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
          std::cout << "  "<<(double)duration.count()/1000<<" ms"<<std::endl;
          
          for (auto trbEvent : ed->GetEvents()){
            errors += trbEvent->GetErrors().size();
          }
          std::cout << " Found "<<errors<<" TRB errors"<<std::endl;

          delete ed; 
          */
          
          break;
          
        case 2:
          std::cout << "Starting L1A loop. TRB Event data will be retrieved via GetTRBEventData() and written to file: /var/tmp/L1ALoop.trb"<<std::endl;
          
          if (trb == nullptr){
            std::cout << "Setup TRB first! "<<std::endl;
            return 0;
          }
          outfileName << "/var/tmp/L1ALoop_"<<runCounter<<".daq";
          runCounter++;
          // trb->SetupStorageStream(outfileName.str());

          std::cout << "starting L1A Loop with "<<m_calLoopTriggers<<" triggers and delay = "<<m_calLoopDelay
                    << " Dynamic CalLoopDelay = "<<m_dynamicCalLoopDelay <<std::endl;
          
          trb->GetConfig()->Set_Global_CalLoopNb(m_calLoopTriggers);
          trb->GetConfig()->Set_Global_CalLoopDelay(m_calLoopDelay);
          trb->GetConfig()->Set_Global_DynamicCalLoopDelay(m_dynamicCalLoopDelay);
          trb->WriteConfigReg();
          
          RunL1ALoop();
          
          
          break;
       case 3:
          std::cout << "Enter delay (default = 1000): "<<endl;
          std::cin >> m_calLoopDelay;
          break;
       case 4:
          std::cout << "Enter Nr of triggers (default = 50): "<<endl;
          std::cin >> m_calLoopTriggers;
          break;
        case 5:
          std::cout << "Enable Dynamic delay depending on FIFO status? (0,1)"<<endl;
          std::cin >> m_dynamicCalLoopDelay;
          break;
        case 9:
          std::cout << "Is calibration loop running = "<<trb->IsCalibrationLoopRunning()<<std::endl;
          break;
     }
}
    return 0;
}

int PrintMenu(int level)
{
  int choice = 0;
  std::string cfgFile;
  std::string arg;
  char* pEnd;

  ConfigReg actualCfg;
  unsigned int l1a, bcid;
  
  int nL1A;
  unsigned int thr;
  
  switch (level){
    case 0:
      cout << "Basic control of FASER tracker: "<<endl;
      cout << " 0: Exit"<<std::endl;
      cout << " 1: SetupTRB"<<std::endl;
      cout << " 2: Configure modules "<<endl;
      cout << " 3: Set Threshold "<<endl;
      cout << " 4: Record N events "<<endl;
      cout << " 5: Start Data taking "<<endl;
      cout << " 6: Stop Data taking"<<endl;
      cout << " 7: Read TRB counters"<<endl;
      cout << " 8: Print TRB configuration"<<endl;
      cout << " 9: Read TRB Status "<<endl;
      cout << "10: Calibration Loop "<<endl;
      cout << "11: Run safe phase detection "<<endl;
      cout << "12: Run Strobe Delay Scan "<<endl;
      cout << "12: Run Strobe Delay Scan - calLoop "<<endl;
      cout << "--------------------------------------"<<endl;
      std::cout << "Your choice: ";
      std::cin >> choice;
      if (choice == 0) return level-1;
      return choice;
      
    case 1: // config modules
      cout << " Enter ID of TRB to be used (-1 for don't care)"<<std::endl;
      cin >> arg;
      cin.get(); // also get \n from input stream
      TRBID = strtoul(arg.c_str(), &pEnd, 0);
      cout << " Enter module mask: ";
      cin >> arg;
      cin.get(); // also get \n from input stream
      m_moduleMask = strtoul(arg.c_str(), &pEnd, 0);
      SetupTRB();
      return 0;
      
    case 2: // config modules
      cout << " Enter cfg file (return for default): ";
      //getline(cin, cfgFile);
      ConfigureModules(cfgFile);
      return 0;
      
    case 3: // set thr
      cout << " Enter thr value: ";
      cin >> arg;
      cin.get(); // also get \n from input stream
      thr = strtoul(arg.c_str(), &pEnd, 0);
      SetThreshold(thr);
      return 0;
      
    case 4: // record data
      cout << " Enter Number of L1As to send: ";
      cin >> arg;
      cin.get(); // also get \n from input stream
      nL1A = strtoul(arg.c_str(), &pEnd, 0);
      RecordData(nL1A);
      return 0;
      
      
    case 5:
      cout << "Enabeling DT on SCT moduels and starting DAQ thread ... "<<std::endl;
      startDataTaking();
      return 0;
      
    case 6:
      cout << "Stopping DAQ thread "<<std::endl;
      stopDataTaking();
      return 0;
      
    case 7:
      trb->ReadSoftCounters(bcid, l1a);
      std::cout << "current counter values: L1A = "<<l1a<<" BCID = "<<bcid<<std::endl;
      return 0;
      
    case 8:
      cout << "Reading TRB configuration: "<<std::endl;
      uint16_t FWv;
      trb->GetSingleFirmwareVersion(0x0,  FWv);
      actualCfg.SetFWVersion(FWv);
      actualCfg.FillFromPayload(trb->ReadConfigReg());
      actualCfg.Print();
      return 0;

    case 9:
      trb->PrintStatus();
      return 0;
      
    case 10: // record data
      return PrintMenuCalLoop(0);
      return 0;
      break;
      
    case 11:
      RunSafePhaseDetection();
      return 0;
      break;
      
    case 12:
      RunStrobeDelayScan();
      return 0;
      break;
      
    case 13:
      RunStrobeDelayScan_CalLoop();
      return 0;
      break;

    default:
      std::cout << "Unknown option"<<std::endl;
      return level;
  }
  return -1;
}


int main ( int argc, char ** argv)
{
  int menuLevel = 0;
  int choice;
  while (menuLevel > -1){
    menuLevel = PrintMenu(menuLevel);
  }
  delete trb; 
  return 0;
}
