/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#include "../../TrackerReadout/TRBEventDecoder.h"
#include <string>
#include <iostream>
#include <fstream>

using namespace FASER;


void TestModuleDataDecoding(std::string filename)
{
  TRBEventDecoder *ed = new TRBEventDecoder();
  ed->LoadModuleDataFromFile(filename);
  // std::cout << "Decoding TRB Data"<<std::endl;
  // ed->DecodeData();
  std::cout << std::endl << "Decoding module Data"<<std::endl;
  ed->DecodeModuleData(0);
  delete ed;
}


void PrintEventSummary(TRBEventDecoder *ed)
{
  int BCIDMismatches = 0;
  int IncompleteEvents = 0;
  int Errors = 0;
  std::vector<int> hitsPerChip;
  hitsPerChip.resize(13);
  
  auto evts = ed->GetEvents();
  std::cout << "Event recorded: "<<evts.size()<<std::endl;
  for (auto evt : evts){
    if (evt->GetModule(0)->BCIDMismatch()) {BCIDMismatches++;}
    if (evt->GetModule(0)->MissingData()) {IncompleteEvents++;}
    if (evt->GetModule(0)->HasError()) {Errors++;}
    int id = 0;
    for (unsigned int chip = 0x20; chip <= 0x2d; chip++) {
      if (chip == 0x25) chip = 0x28;
      hitsPerChip[id] += evt->GetModule(0)->GetNHits(chip);
      id++;
    }
    hitsPerChip.back() += evt->GetModule(0)->GetNHits(0);
    
    if (evt->GetL1ID() < 5){
      std::cout << "L1ID = "<< evt->GetL1ID()<< " Hits per Chip : ";
      for (unsigned int chip = 0x20; chip <= 0x2d; chip++) {
        if (chip == 0x26) chip = 0x28;
        std::cout << " " <<std::dec<< evt->GetModule(0)->GetNHits(chip);
      }
      std::cout << std::endl;
    }
  }
  std::cout << "Total hits per chip: ";
  for (unsigned int chip = 0; chip <= 12; chip++) {
    std::cout << " " << hitsPerChip[chip];
    hitsPerChip[chip] = 0;
  }
  std::cout << "Unkown chips occurences: " << hitsPerChip[13]<<std::endl;
  std::cout << std::endl;
  std::cout << "Events with BCID mismatches between led and ledx data lines: "<<BCIDMismatches<<std::endl;
  std::cout << "Events with missing data: "<<IncompleteEvents<<std::endl;
  std::cout << "Events with module errors: "<<Errors<<std::endl;
  
}

void DecodePhysics(TRBEventDecoder *ed, unsigned int module)
{
  std::vector <int> hitsPerStrip;
  hitsPerStrip.resize(128*13);
  std::vector < std::vector <int> > histogram;
  for (unsigned int i = 0; i < 8; i++) histogram.push_back(hitsPerStrip);
  
  
  ed->SetEventInfoVerbosity(0);
  auto evts = ed->GetEvents();
  std::cout << "# DecodePhysics(): Decoding "<<evts.size()<<" Events"<<std::endl;
  double avgHitsPerEvent = 0;
  int evtCnt = 0;
  for (auto trbEvt : evts){
    evtCnt++;
    
    int hitsPerEvent = 0;
    auto trbErrors = trbEvt->GetErrors();
    if (trbErrors.size() !=0){
      std::cout << "found "<<trbErrors.size()<<" TRBErrors: "<<std::endl;
      for (auto err : trbErrors) std::cout << " 0x"<<std::hex<<(int)err;
      std::cout << std::dec<<std::endl;
    }
    for (unsigned int module = 0; module < 8; module++){
      int chipsWithErrors = 0;
      int hitsPerModule = 0;
      auto sctEvt = trbEvt->GetModule(module);
      if (sctEvt == nullptr ) continue;
      auto sctEvtErrors = sctEvt->GetErrors();
      for (auto chipErrors : sctEvtErrors) { if (chipErrors.size() > 0 ) {chipsWithErrors++;}}
      if (chipsWithErrors > 0) std::cout << chipsWithErrors<<" chips reported errors for module "<<module<<std::endl;
      std::cout << "Module = "<<module<<" L1ID = "<<trbEvt->GetL1ID()<<" BCID = "<<trbEvt->GetBCID()<<std::endl;
      if (sctEvt == nullptr){
        std::cout << "No SCT data for module "<<module << std::endl;
        continue;
      }
      auto allHits = sctEvt->GetHits();
      int chip = 0;
      for (auto hitsPerChip : allHits){
        hitsPerModule += hitsPerChip.size();
        for (auto hit : hitsPerChip){ // hit is an std::pair<uint8 strip, uint8 pattern>
          histogram[module][(int)(hit.first)+(chip*128)]++;
          //  std::cout << chip <<","<< (int)hit.first << " ";
        }
      chip++;
      }
      hitsPerEvent += hitsPerModule;
    } //  end module loop
    avgHitsPerEvent += (double)hitsPerEvent;
    //std::cout << std::endl;
    if (evtCnt % 10000 == 0) std::cout << "processing Event: "<<evtCnt<< " total hits = "<<avgHitsPerEvent<<std::endl;
    if (evtCnt == evts.size()-1) {break;}
  } // end event loop
  avgHitsPerEvent /= evts.size();
 
  std::cout << " histogram: module strip nHits"<<std::endl;
  for (unsigned int module = 0; module < 8; module++){
    for (unsigned int strip = 0; strip < 128*13; strip++){
      std::cout << module << " " << strip << " " << histogram[module][strip]<<" #HIST#"<<std::endl;
    }
  }
  std::cout << " average hits per event = "<<avgHitsPerEvent<<std::endl;
  
}
    
void DecodeThrScan(TRBEventDecoder *ed, unsigned int module_unused )
{
  ed->SetEventInfoVerbosity(0);
  std::cout << "# Decoding data for all modules  "<<std::endl;
  
  // loop over all scan steps found
  for (unsigned int i = 0; i < ed->GetEventsPerScanStep().size(); i++){
    auto scanConfig = ed->GetScanStepConfigs()[i];
    
    std::cout << std::endl<<"# StripMask = "<<std::dec<<(int)scanConfig.CalMode<<" Threshold = "<<(int)scanConfig.Threshold<<std::endl;

    auto evts = ed->GetEventsPerScanStep()[i];
    std::cout << "# evnts = "<<std::dec<<evts.size()<< " "<<std::endl;
    std::cout << "# CalMode Threshold module chipAddr stripAddr nHits: "<<std::endl;
    
    std::vector< std::vector<unsigned int> > hitsPerModule;
    std::vector<unsigned int> hitsPerStrip;
    for (unsigned int i = 0; i < 128; i++) hitsPerStrip.push_back(0);
    for (unsigned int i = 0; i < 8; i++) hitsPerModule.push_back(hitsPerStrip);
    
    // loop over all events and sum up hits per strip
    std::vector<int> chipAddr = {0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x28, 0x29, 0x2a, 0x2b, 0x2c, 0x2d};
    for (auto chip : chipAddr){
      std::cout << "# analyzing data from chip: 0x"<<std::hex<<chip<<std::endl;
      int lastL1ID = -1;
      for (unsigned int module = 0; module < 8; module++){
        hitsPerModule[module].resize(0);
        for (unsigned int i = 0; i < 128; i++) hitsPerModule[module].push_back(0);
      }
      for (auto evt : evts){
        auto L1ID = evt->GetL1ID();
        if (lastL1ID == -1) {lastL1ID = L1ID;}
        else {
          if (L1ID - lastL1ID > 1){ std::cout <<std::dec<< "## L1IDs not consecutive: last = "<<lastL1ID<<" this = "<<L1ID<<std::endl;}
          lastL1ID = L1ID;
        }
        for (unsigned int module = 0; module < 8; module++){
          auto sctEvt = evt->GetModule(module);
          if (sctEvt == nullptr){
            //std::cout << "# WARNING: no SCT event attached to TRB event! " <<std::endl;
            continue;
          }
          // get hits for one chip: 0x20
          auto hits = sctEvt->GetHits(chip); // vector of pairs <strip addr, hit pattern>
          // loop over hits
          for (auto hit : hits){
            if (hit.first > 127){
              std::cout << "ERROR: strip address > 127"<<std::endl;
              continue;
            }
            if (hit.second <= 0x7){
              hitsPerModule[module][hit.first]++;
            }
          }
        }
      }
      for (int module = 0; module < 8; module++){
        for (int i = 0; i < 128; i++){
          if (hitsPerModule[module][i] > 0){
            std::cout <<std::dec<<(int)scanConfig.CalMode<<" " << (int)scanConfig.Threshold << " "<<module << " "<<chip
            << " " << i << " "<< hitsPerModule[module][i]<<std::endl;
          }
        }
      }
    }
  }
}



int main(int argc, char **argv)
{
  std::string filename;
  unsigned int module = 0;
  
  if (argc == 3){
    filename = argv[2];
    module = atoi(argv[1]);
  } else if (argc == 2){
    filename = argv[1];
  } else {
    TestModuleDataDecoding("ModuleData.bin");
    return 0;
  }
  
  std::vector< std::vector <uint16_t> > configData;
  configData.resize(16);
  
  TRBEventDecoder *ed = new TRBEventDecoder();
  
  ed->SetEventInfoVerbosity(0);
  std::ofstream outstream;
  outstream.open("TRBRawData.daq");
  ed->LoadRORDataFromFile(filename, module, &configData, &outstream);
  outstream.close();
  
  DecodePhysics(ed, module);
  std::cout << " Decoding done."<<std::endl;
  //DecodeThrScan(ed, module );
  
  return 0;
  
  for (unsigned int chip = 0; chip < configData.size(); chip++){
    if (configData[chip].size() > 0) {
      uint16_t tmp = configData[chip][0];
      std::cout << "Chip "<<chip<<": config = 0x"<<std::hex<<tmp<<" number of reads: "<<std::dec<< configData[chip].size()<<std::endl;
      int errors = 0;
      for (auto value : configData[chip]){
        if (value != tmp){
          std::cout << "Config found: "<<std::hex<<value<<std::endl;
          errors++;
        }
      }
      std::cout << "   errors: "<<std::dec<<errors<<std::endl;
    }
  }
  
  std::cout << std::endl<< "ScanSteps decoded: "<< ed->GetEventsPerScanStep().size() << std::endl;
  std::vector<unsigned int> AllBCIDMismatches;
  std::vector<unsigned int> AllIncompleteEvents;
  std::vector<unsigned int> AllErrors;
  
  std::vector<int> hitsPerChip;
  hitsPerChip.resize(13);
  
  std::cout << "Will print: allEvents ThrValue eventsPerChip "<<std::endl;
  for (unsigned int i = 0; i < ed->GetEventsPerScanStep().size(); i++){
    
    auto evts = ed->GetEventsPerScanStep()[i];
    std::cout << " evnts = "<<std::dec<<evts.size()<< " "<<std::endl;
    // std::cout << " thr = " <<(int)ed->GetScanStepConfigs()[i].Threshold << " ";
    ed->GetScanStepConfigs()[i].Print();
    unsigned int BCIDMismatches = 0;
    unsigned int IncompleteEvents = 0;
    unsigned int Errors = 0;
    
    // std::cout << "Events in scan step: "<<evts.size()<<std::endl;
    for (auto evt : evts){
      if (evt->GetNModulesPresent() == 0) {
        // This happens in TRB FW versino 1.3 every second event. Usually should not happen.
        continue;
      }
      if (evt->GetModule(module) == nullptr) {
        std::cout << "module "<< module << " not found in data! "<<std::endl;
        continue;
      }
      if (evt->GetModule(module)->BCIDMismatch()) {BCIDMismatches++;}
      if (evt->GetModule(module)->MissingData()) {IncompleteEvents++;}
      if (evt->GetModule(module)->HasError()) {Errors++;}
      int id = 0;
      //std::cout << std::endl;
      for (unsigned int chip = 0x20; chip <= 0x2d; chip++) {
        if (chip == 0x26) chip = 0x28;
        hitsPerChip[id] += evt->GetModule(module)->GetNHits(chip);
        //std::cout << " " << evt->GetModule(module)->GetNHits(chip);
        id++;
      }
      //std::cout << " | ";
      hitsPerChip.back() += evt->GetModule(module)->GetNHits(0);
      /*
      if (evt->GetL1ID() < 5){
        std::cout << "L1ID = "<< evt->GetL1ID()<< " Hits per Chip : ";
        for (unsigned int chip = 0x20; chip <= 0x2d; chip++) {
          if (chip == 0x26) chip = 0x28;
          std::cout << " " << evt->GetModule(0)->GetNHits(chip);
        }
        std::cout << std::endl;
      }
       */
    }
    std::cout << std::endl << " Total number of hips per chip: " <<std::endl;
    for (unsigned int chip = 0; chip < 12; chip++) {
      std::cout << " " << hitsPerChip[chip];
      hitsPerChip[chip] = 0;
    }
    std::cout << std::endl;
    
    AllBCIDMismatches.push_back(BCIDMismatches);
    AllErrors.push_back(Errors);
    AllIncompleteEvents.push_back(IncompleteEvents);
  
  }
  
  for (auto tmp : AllBCIDMismatches) { if (tmp > 0)  std::cout << "Events with BCID mismatches between led and ledx data lines: "<<tmp<<std::endl; }
  for (auto tmp : AllIncompleteEvents) { if (tmp > 0) std::cout << "Events with missing data: "<<tmp<<std::endl;}
  for (auto tmp : AllErrors) { if (tmp > 0)std::cout << "Events with module errors: "<<tmp<<std::endl;}
  
  return 0;

}
