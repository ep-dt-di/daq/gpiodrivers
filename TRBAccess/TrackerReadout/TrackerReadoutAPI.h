/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#ifndef __TrackerReadoutAPI
#define __TrackerReadoutAPI

namespace FASER {
  /** *******************************************************
   \brief API to acces Tracker Readout Board and SCT modules.
   
   This class defines the API to comminucate with the Tracker
   modules (SCT modules) via the tracker readout board (TRB)
   ******************************************************** */
  class TrackerReadoutAPI
  {
  public:
    // CONSTRUCTOR / DESTRUCTOR
    TrackerReadoutAPI();
    ~TrackerReadoutAPI();
    
    public:
    // High level functions
    /** \brief Configure the TrackerReadoutBoard from given configuration
     */
    void ConfigureReadoutBoard(/* config file or DB entry should be passed */);
    /** \brief Send configuration to the given tracker modules
        \param Destination module
     \param Configuration: The format of the configuration still has to be decided (binary file, human readable file, database? )
     */
    void ConfigureTrackerModules(/* config file or DB entry should be passed */);
    /** \brief Read the current configuration from the given module.
     */
    void ReadTrackerModuleConfiguration(){;}
    /** \brief Uploads the given strip mask to the specified module
     \param Destination module
     \param Strip mask: The format of the configuration still has to be decided (binary file, human readable file, database? )
     */
    void LoadStripMask(/* config file or DB entry should be passed */);
    /** \brief Enable data taking mode on TRB and tracker modules
     */
    void StartDataTaking(){;}
    /** \brief Stop data taking mode on TRB.
        Modules have to be reconfigured to enter calibration mode.
     */
    void StopDataTaking(){;}
    /** \brief Reads last event from TRB fifo
     */
    void GetLastEvent(){;}
    /** \brief To be decided what will be part of the status. Counters, health flags, ...
     */
    void GetStatus(){;}
    
  
};
  
}

#endif /* __TrackerReadoutAPI */
