/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#ifndef __TRB_ConfigRegisters
#define __TRB_ConfigRegisters

#include <vector>

namespace FASER {
  
  /** *******************************************************
   \brief Representation of the config register including getter / setter
   ******************************************************** */
  
  class ConfigReg {
  public:
    ConfigReg();
    
    void SetFWVersion(int FWVersion) { m_FPGA_version = FWVersion; }
    void Set_Module_L1En(short module, bool value); // deprecated
    void Set_Module_L1En(short moduleMask);
    void Set_Module_BCREn(short module, bool value); // deprecated
    void Set_Module_BCREn(short moduleMask);
    void Set_Module_ClkCmdSelect(short module, bool value); // deprecated
    void Set_Module_ClkCmdSelect(short module);
    void Set_Module_LedxRXEn(short module, bool endableLed, bool endableLedx); // deprecated
    void Set_Module_LedRXEn(short moduleMask);
    void Set_Module_LedxRXEn(short moduleMask);
    void Set_Global_RxTimeout(uint8_t value); // 8 bits
    void Set_Global_Overflow(uint16_t value); // 12 bits
    void Set_Global_L1Timeout(uint8_t value); // 8 bits
    void Set_Global_HardwareDelay0(uint8_t value); // 3 bits
    void Set_Global_HardwareDelay1(uint8_t value); // 3 bits
    void Set_Global_L1TimeoutDisable(bool value);
    void Set_Global_RxTimeoutDisable(bool value);
    void Set_Global_L2SoftL1AEn(bool value);
    // only available in FW version >= 1.5
    void Set_Global_CalLoopNb(uint32_t value); // 20 bits (FW > 1.C, otherwise 16 bit)
    void Set_Global_CalLoopDelay(uint16_t value); // 11 bits
    void Set_Global_DynamicCalLoopDelay(bool value);
    void Set_Global_BCREn(bool value);
    void Set_Global_TLBClockSel(bool value);
    void Set_Global_L1AEn(bool value);
    void Set_Global_SoftCounterMuxEn(bool value);

    std::vector<uint16_t> GetPayloadVector();
    uint16_t GetGlobalDirectParam(){return m_GlobalDirectParam;}
    
    void FillFromPayload(std::vector<uint16_t> payload);
    
    void Print();
    
    // operators
    friend bool operator==(const ConfigReg& lhs, const ConfigReg& rhs) {
        if (lhs.Module_L1En != rhs.Module_L1En) {  return false; }
        if (lhs.Module_BCREn != rhs.Module_BCREn) {  return false; }
        if (lhs.Module_ClkCmdSelect != rhs.Module_ClkCmdSelect) {  return false; }
        if (lhs.Led0RXEn != rhs.Led0RXEn) {  return false; }
        if (lhs.Led1RXEn != rhs.Led1RXEn) {  return false; }
        if (lhs.Global != rhs.Global) {  return false; }
      return true;
    }
    friend bool operator!=(const ConfigReg& lhs, const ConfigReg& rhs){ return !(lhs == rhs); }
    
    
    // storage
  public:
    uint8_t Module_L1En;
    uint8_t Module_BCREn;
    uint8_t Module_ClkCmdSelect;
    uint8_t Led0RXEn;
    uint8_t Led1RXEn;
    uint64_t Global; // 36bits (FW >= 1.5 63 bits) (FW >= 1.C 37 bits) used
    uint32_t GlobalLoop; // 32 bits for FW >= 1.C
    
    // bit Masks for the global register
  private:
    void Set_Module_ConfigBit(uint8_t &config, short module, bool value);
   
    static const uint64_t MASK_Global_CalLoopDelay			= 0xFFE0000000000000; // 11-bit
    static const uint8_t  SHIFT_Global_CalLoopDelay			= 8+12+8+1+1+3+3+1+16;
    static const uint64_t MASK_Global_CalLoopNb        	= 0x1FFFE000000000; // 16-bit
    static const uint8_t  SHIFT_Global_CalLoopNb				= 8+12+8+1+1+3+3+1;
    static const uint64_t MASK_Global_RxTimeoutDisable  = 0x1000000000;
    static const uint8_t  SHIFT_Global_RxTimeoutDisable = 8+12+8+1+1+3+3 ;
    static const uint64_t MASK_Global_HardwareDelay1  = 0xE00000000;
    static const uint8_t  SHIFT_Global_HardwareDelay1 = 8+12+8+1+1+3 ;
    static const uint64_t MASK_Global_HardwareDelay0  = 0x1C0000000;
    static const uint8_t  SHIFT_Global_HardwareDelay0 = 8+12+8+1+1 ;
    static const uint64_t MASK_Global_L2SoftL1AEn     = 0x20000000 ; // 1-bit
    static const uint64_t MASK_Global_TimeoutDisable  = 0x10000000 ; // 1-bit
    //static const uint32_t MASK_Global_L1AEn           = 0x20000000 ; // 1-bit
    //static const uint32_t MASK_Global_BCREn           = 0x10000000 ; // 1-bit
    //static const uint8_t  SHIFT_Global_BCREn           = 8+12+8 ;
    static const uint64_t MASK_Global_L1Timeout       = 0x0FF00000 ; // 8-bit
    static const uint8_t  SHIFT_Global_L1Timeout       = 8+12 ;
    static const uint64_t MASK_Global_Overflow        = 0x000FFF00 ; // 12-bit
    static const uint8_t  SHIFT_Global_Overflow        = 8 ;
    static const uint64_t MASK_Global_RxTimeout       = 0x000000FF ; // 8-bit
    static const uint8_t  SHIFT_Global_RxTimeout       = 0 ;
    
    int m_FPGA_version;
    uint16_t m_GlobalDirectParam = 0x0000;
    
  };
  
  
  /** *******************************************************
   \brief definitions of Direct parameter register values
   ******************************************************** */
  class TRBDirectParameter {
  public:
    static const uint16_t SoftCounterMuxEn        = 0x0001;
    static const uint16_t TLBClockSelect          = 0x0002;
    static const uint16_t L1CounterReset          = 0x0004;
    static const uint16_t BCREn                   = 0x0008;
    static const uint16_t L1AEn                   = 0x0010;
    static const uint16_t ErrCntReset             = 0x4000;
    static const uint16_t FifoReset               = 0x8000;
  };
  
  
  /** *******************************************************
   \brief Representation of the Phase register including getter / setter
   ******************************************************** */
  
  class PhaseReg {
  public:
    PhaseReg();
    
    void SetFinePhase_Clk0(unsigned int value);
    void SetFinePhase_Clk1(unsigned int value);
    void SetFinePhase_Led0(unsigned int value);
    void SetFinePhase_Led1(unsigned int value);
    //void SetFinePhase_HardwareDelay0(unsigned int value);
    //void SetFinePhase_HardwareDelay1(unsigned int value);
    
    std::vector<uint16_t> GetPayloadVector();
    
  private:
    uint8_t FinePhaseClk0;
    uint8_t FinePhaseClk1;
    uint8_t FinePhaseLed0;
    uint8_t FinePhaseLed1;
    //uint8_t HardwareDelay0;
    //uint8_t HardwareDelay1;
    
  };
  
  /** *******************************************************
   \brief Representation of the SCT SlowCommand register in the TRB including getter / setter
   ******************************************************** */
  class SCT_SlowCommandReg {
  public:
    
    // definitions of Field 3 commands calss
    static const uint8_t CMD_CLASS_DT             = 0x0C;
    static const uint8_t CMD_CLASS_CONF           = 0x1C;
    static const uint8_t CMD_CLASS_MASK           = 0x8C;
    
    // definitions of Field 5 commands
    static const uint8_t PULSE_INPUT_REGISTERS    = 0x20;
    static const uint8_t ENABLE_DATA_TAKING       = 0x28;
    static const uint8_t IssueCalibPusle          = 0x30;
    static const uint8_t WriteConfigRegister      = 0x00;
    static const uint8_t WriteTrimDac             = 0x04;
    static const uint8_t WriteStrobeDelayReg      = 0x10;
    static const uint8_t WriteThresholdReg        = 0x18;
    static const uint8_t WriteBiasDac             = 0x38;
    static const uint8_t WriteStripMask           = 0x08;
    
    // chip broadcast
    static const uint8_t ALL_CHIPS                = 0x3F;
    
  public:
    
    SCT_SlowCommandReg();
    
    void SetField3(unsigned int value);
    void SetField4(unsigned int value);
    void SetField5(unsigned int value);
    void SetField6(unsigned int value); // usually one 16-bit word in Field 6 suffices
    void SetField6(std::vector<uint16_t> values); // The Mask register requires 128 bits
    
    // high level functions
    void EnableDataTaking(unsigned int chipID = ALL_CHIPS) {SetField3(CMD_CLASS_DT);SetField4(chipID);SetField5(ENABLE_DATA_TAKING); SetField6(0);}
    void CalibrationPulse(unsigned int delay = 0, unsigned int chipID = ALL_CHIPS); 
    void PulseInputRegisters(unsigned int chipID) { SetField3(CMD_CLASS_CONF);SetField4(chipID);SetField5(PULSE_INPUT_REGISTERS); SetField6(0);}
    void SetStrobeDelay(unsigned int chipID, unsigned int value) {SetField3(CMD_CLASS_CONF);SetField4(chipID);SetField5(WriteStrobeDelayReg); SetField6(value);}
    void SetThreshold(unsigned int chipID, unsigned int value) { SetField3(CMD_CLASS_CONF);SetField4(chipID);SetField5(WriteThresholdReg);SetField6(value);}
    void SetBiasDac(unsigned int chipID, unsigned int value) { SetField3(CMD_CLASS_CONF);SetField4(chipID);SetField5(WriteBiasDac); SetField6(value);}
    void SetTrimDac(unsigned int chipID, unsigned int value) { SetField3(CMD_CLASS_CONF);SetField4(chipID);SetField5(WriteTrimDac); SetField6(value);}
    void SetConfigReg(unsigned int chipID, unsigned int value) { SetField3(CMD_CLASS_CONF);SetField4(chipID);SetField5(WriteConfigRegister); SetField6(value);}
    /** Set the strip mask per chip.
     \param chipID: the ID of the readout chip the chipmask is applied to
     \param mask: 128 bits defining the strip mask, packed in a vector of uint16_t
     */
    void SetStripMask(unsigned int chipID, std::vector<uint16_t> mask) {
      SetField3(CMD_CLASS_MASK);SetField4(chipID);SetField5(WriteStripMask); SetField6(mask);}
    
    
    
    std::vector<uint16_t> GetPayloadVector();
    
    
    /** *****************************
     * Need to reverse bit-order for loading SCT bitstream into TRB registers.
     * Function for revering bit order in one byte copied from stackloverflow
     */
    //Index 1==0b0001 => 0b1000
    //Index 7==0b0111 => 0b1110
    //etc
    const unsigned char lookup[16] = {
      0x0, 0x8, 0x4, 0xc, 0x2, 0xa, 0x6, 0xe,
      0x1, 0x9, 0x5, 0xd, 0x3, 0xb, 0x7, 0xf, };
    
  
    uint8_t reverseBitOrder(uint8_t n) {
      // Reverse the top and bottom nibble then swap them.
      return (lookup[n&0b1111] << 4) | lookup[n>>4];
    }
    
    uint16_t reverseBitOrder(uint16_t n) {
      uint8_t byte0 = n;
      uint8_t byte1 = n>>8;
      
      uint16_t result = reverseBitOrder(byte0);
      result <<= 8;
      result |= reverseBitOrder(byte1);
      return result;
    }
    
    // Detailed breakdown of the math
    //  + lookup reverse of bottom nibble
    //  |       + grab bottom nibble
    //  |       |        + move bottom result into top nibble
    //  |       |        |     + combine the bottom and top results
    //  |       |        |     | + lookup reverse of top nibble
    //  |       |        |     | |       + grab top nibble
    //  V       V        V     V V       V
    // (lookup[n&0b1111] << 4) | lookup[n>>4]
    
    
  private:
    uint8_t Field3; // 8 bits
    uint8_t Field4; // 6 bits Chip address -> see geographical addressing in ABCD documentation
    uint8_t Field5; // 6 bits
    std::vector<uint16_t> Field6; // 10x16bits
    
  };
  
  
}
#endif // __TRB_ConfigRegisters
