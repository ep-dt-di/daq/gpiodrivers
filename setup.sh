# Export package specific environmental variables
echo "Setting up GPIODrivers environment"

# CMake3 complains when this is not set.
export CC=/opt/ohpc/pub/compiler/gcc/8.3.0/bin/gcc
export CXX=/opt/ohpc/pub/compiler/gcc/8.3.0/bin/g++

# So that dynamically linked libraries can be found
export LD_LIBRARY_PATH=/opt/ohpc/pub/compiler/gcc/8.3.0/lib64/:/usr/local/lib64/:$LD_LIBRARY_PATH
export PATH=/opt/ohpc/pub/compiler/gcc/8.3.0/bin:$PATH

#export THIRD_PARTY_DIRECTORIES=""
echo "Done"
