# FaserTrackerReadout

This package contains the readout libraries to access:
 - FASER's trigger logic board (TLB).
 - FASER's tracker readout board (TRB) and via this the SCT modules.

Each readout library depends on the GPIObase library, the base library for access to a UniGe GPIO board.
The UniGe GPIO front-end interface specifications are documented here:
[GPIO front-end interface specifcations in sharepoint](https://espace.cern.ch/faser-share/Shared%20Documents/Trigger,%20DAQ%20and%20DCS/Unige%20FrontEnd%20Interface%20-%20UFE_Protocol-v1.4.pdf)

## Requirements:
 - libusb. Installation on CC7 - like system: 
```
 sudo yum install libusbx-devel 
```

## Installation:

NOTE: It is important to source the setup script now!

### On DAQ server
```
source setup.sh
mkdir build
cd build
cmake3 ../
make
```

### Local/non-DAQ

 - modify setup.sh:
    - Set `CC` and `CXX` to point to compiler.
    - Update `LD_LIBRARY_PATH` and `PATH` to include the GCC compiler lib and bin directory.
    - `THIRD_PARTY_DIRECTORIES` to list directories that need to be included as third party dependencies. This must include the location of the nlohmann json header.

Then
```
source setup.sh
mkdir build
cd build
cmake3 ../
make
```

### Switching cmake or c++ standard versions

The software is configured to run on `cmake3` (version 3.4.3 minimum) and `c++ standard 17`. 

In order to run with a older cmake version, modify the line
`cmake_minimum_required (VERSION 3.4.3)` in `CMakeLists.txt`. 

In order to run with `c++ standard 11`, change the line
`use_cxx17()` to `use_cxx11()` in `CMakeLists.txt`.


## TRBAccess:

### How to run
A simple command line program exists to provide basic functionality. Please see the help to get detailed information:
```
MenuTRB -h
```
E.g., to configure modules 0 and 1  just do
```
MenuTRB -c 3
```
Where 3 is the bitmask of modules to configure. To configure all 8 modules use -c 255.

### Testing communication via ethernet
In order to test status of ethernet communication via UDP interface (which might be useful to do if MenuTRB from previous section is not working), there is a program called ethernetTestApp. It is located in gpiodrivers repository in TRBAccess/src/app/ethernetTestApp.cxx. It is compiled to executable binary during every compilation of gpiodrivers. Running this binary without parameters displays help on how to use this program. It takes three mandatory parameters which are SCIP (-s), DAQIP (-d) and boardID (-b). Meaning of these parameters is the same as in the constructor of TRBAccess class. For more information see help after running the program. Running the program with all the parameters displays following menu:

```
1: Test socket creation
2: Test connection
3: Test configuration 
4: Test DAQ
5: Test commands
```

- Option 1 - Tests creation of the sockets (does not test communication with the TRB board). If this fails, no communication is possible because connection between TRB and PC cannot even be established.
- Option 2 - Tests communication with a board. This sends some simple command to the board and expects answer. If this fails, it is possible (but not necessary) that even the first test would fail. In case this test failed, refer to the paragraph below this list.
- Option 3 - Tests configuration of the board and reads it out and checks if configuration sent/read back match.
- Option 4 - Tests data acquisition by generating internal L1As. Checks format of the data and other basic checks (i.e. if L1A ID is increasing by 1). If something unexpected appears, user gets error message. This is also tested in a loop several times. This number is also specified by user in a similar was as in option 3.
- Option 5 - Tests various commands and checks if it receives expected answer. If not, user gets error message. This is tested in a loop several times. This number of repetitions is specified by user in a similar way as in options 3 and 4.

In case that test in options 1 or 2 failed, PC or network might not be configured properly. There can be various reasons for this but main things to do/check are:

- Open ports 50000, 50001, 50002 and 50005 for UDP protocol. Status of open ports can be checked by calling 'iptables' on given linux machine.
- If the ports listed in bullet point above has been opened, they should also not be blocked by firewall
- If this is done and communication still does not work, it is better to ask someone who has already set up ethernet communication with the TRB. Program wireshark can be used for debugging of the problem.

## TLBAccess:

### Testing communication via ethernet
Similarly as in the case with the TRB it is usually good to check that communication between PC and TLB is working and is set up properly. There is a program called TLBethernetTestApp. It is located in gpiodrivers repository in TLBAccess/src/app/TLBethernetTestApp.cxx. Since options on what to test are exactly the same as for the TRB ethernetTestApp, one can refer to the subsection 'Testing communication via ethernet' under TRBAcess for more detailed description.

## Documentation:

Documentation is automatically built when commits are pushed to gitlab and are available here:
[http://faserdaq.web.cern.ch/faserdaq/gpiodrivers/doc/html/annotated.html](http://faserdaq.web.cern.ch/faserdaq/gpiodrivers/doc/html/annotated.html)

In order to manually generate the documentation pleae run doxygen using the Doxyfile in the repository.

