/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#ifndef __TLBAccess
#define __TLBAccess

#include "GPIOBase/GPIOBaseClass.h"
#include "GPIOBase/CommunicationInterface.h"
#include "TLB_ConfigRegisters.h"
#include <fstream>
#include <thread>
#include <mutex>
#include "GPIOBase/CRC.h"
#include <bitset>
#include <unistd.h>
#include "nlohmann/json.hpp"
#include "Exceptions/Exceptions.hpp"
#include <iostream>
#include <atomic>

using json = nlohmann::json;

class TLBAccessException : public Exceptions::BaseException { using Exceptions::BaseException::BaseException; };

namespace FASER {
 
  using namespace TLB;  

  /** *******************************************************
   \brief API to communicate with the Trigger Logic Board (TLB).
   ******************************************************** */


  class TLBAccess : public GPIOBaseClass
  {
 
  const uint32_t SUBCMDID_LSHIFT = 12;

  public:
    // CONSTRUCTOR / DESTRUCTOR
    TLBAccess(bool emulateInterface = false) : GPIOBaseClass(emulateInterface){}
    TLBAccess(std::string SCIP, std::string DAQIP, bool emulateInterface = false, int boardID = -1) : GPIOBaseClass(SCIP, DAQIP, emulateInterface, boardID){}
    ~TLBAccess();
    
    void SendDirectParameters(const uint16_t&);
    void DisableTrigger();
    void EnableTrigger(bool ECR, bool Reset);
    void SendECR();
    void SendDataReadoutParameters();
    void ConfigureTLB(const json& myjson);
    void ConfigureAndVerifyTLB(const json& myjson);
    bool VerifyConfiguration();
    void ConfigureLUT(const json& trigjson);
    bool SetLUT(uint8_t x_device, const json trigjson);
    bool VerifyLUT_byCRC(uint8_t x_device);
    std::vector<uint16_t> GetLUT(uint16_t x_device, uint16_t *CRC = nullptr);
    std::vector<uint16_t> DecodeLUT(std::vector<uint16_t> x_LUT);
    void StartReadout(const uint16_t& param);
    void StopReadout();
    std::vector< std::vector<uint32_t> > GetTLBEventData();
    float GetDataRate() {return m_dataRate;}
  
    //bool ApplyConfig(uint8_t x_device);
  
  private:
    std::thread *m_daqThread;
    bool m_daqRunning;
    void PollData();
    
    ConfigReg m_tlb_config = ConfigReg();
    std::mutex mMutex_TLBEventData; //mutex object vector of vectors
    std::vector <uint32_t> m_DataWordOutputBuffer; //vector of words
    std::vector<std::vector<uint32_t>> m_TLBEventData; // complete TRB raw data for every event
    std::atomic<float> m_dataRate;
    
    void SendConfig();
    bool LoadLUT(std::string x_PathToLUT, std::vector<uint16_t> *x_LUT); 
    bool VerifyLUT_byBits(uint8_t x_device, std::vector<uint16_t> x_LUT);
    bool IsBinary(std::string l_value);
    bool GenerateLUT(std::vector<uint16_t>& lut, std::vector<std::string> configs, std::map<std::string, std::string> trig_bit_map);
    void TranslateConfigs(std::vector<std::string>& configs, std::map<std::string, std::string> trig_bit_map);
    void VerifyConfigs(std::vector<std::string> configs, std::map<std::string, std::string> trig_bit_map);
    void replaceAll(std::string& str, std::string from, std::string to);
    std::string ConvertIntToWord(unsigned int word, int nbits);
  };

/** *******************************************************
 * \brief Functions for decoding TLB data.
 ******************************************************** */
  class TLBDecode {
    private:
    
      static const uint32_t TRIGGER_HEADER_V1    = 0xFEAD000A;
      static const uint32_t TRIGGER_HEADER_V2    = 0xFEAD00A0;
      static const uint32_t MONITORING_HEADER_V1 = 0xFEAD0050;
      static const uint32_t MONITORING_HEADER_V2 = 0xFEAD0005;
      static const uint32_t END_OF_DAQ = 0xCAFEDECA;
    
    public:
      
      static bool IsTriggerHeader(const uint32_t& data){ 
        if (data == TRIGGER_HEADER_V1 || data == TRIGGER_HEADER_V2) {
          return true;
        }
        return false;
      };
      static bool IsMonitoringHeader(const uint32_t& data){
        if (data == MONITORING_HEADER_V1 || data == MONITORING_HEADER_V2) {
          return true;
        }
        return false;
      };
      static bool IsEndOfDAQ(const uint32_t& data){
        if (data == END_OF_DAQ) {
          return true;
        }
        return false;
      };
  };

}

/** *******************************************************
 * \brief Definitions of CommandRequest IDs.
 ******************************************************** */
class TLBCmdID:public GPIOCmdID {
public:
  static const uint8_t USER_SET_CONFIG     = 0x08;
  static const uint8_t USER_GET     = 0x10;
};

/** *******************************************************
 * \brief Definitions of readout parameters.
 ******************************************************** */
struct TLBReadoutParameters {
  public:
    static const uint16_t EnableTriggerData = 0x2000;
    static const uint16_t EnableMonitoringData = 0x4000;
    static const uint16_t ReadoutFIFOReset = 0x8000;
};

/** *******************************************************
 \brief Definitions of ERROR codes retrieved from TLB.
 ******************************************************** */

class TLBErrors:public GPIOErrors {
};

/** *******************************************************
 \brief Definitions of WORD ID (bit 31-28 of command request header)
 ******************************************************** */
class TLBWordID:public GPIOWordID{

};

#endif /* __TLBAccess */
