/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#ifndef _TLBStreaming
#define _TLBStreaming

#include <string>
#include <cstdint>
#include <vector>
#include <fstream>
#include <map>
#include <iostream>

namespace FASER {

  /** *******************************************************
   \brief class to stream TLB data to terminal or file.
   ******************************************************** */

  // keeping class as an alternative method of decoding data to official event decoder in faser-commons.
  class TLBStreaming { 
    private:
    
      const uint32_t MASK_TAP_DATA = 0x3F000000;
      const uint8_t  RSHIFT_TAP_DATA = 24;
      const uint32_t MASK_TBP_DATA = 0x3F0000;
      const uint8_t  RSHIFT_TBP_DATA = 16;
      const uint32_t MASK_INPUT_BITS_DATA = 0x00FF00;
      const uint8_t  RSHIFT_INPUT_BITS_DATA = 8;
      const uint32_t MASK_INPUT_BITS_NEXT_CLK_DATA = 0x0000FF;

      std::ofstream m_dataOutStreamMonitoring;
      std::ofstream m_dataOutStreamTrigger;
      bool m_storageSetup;
    
    public:
      ~TLBStreaming();
      
      void SetupStorageDecodedStream();
      void CloseStorageDecodedStream();
      void PrintRawDataVector(const std::vector<uint32_t>& Vector);
      void DecodeTriggerDataVector(const std::vector<uint32_t>& TriggerVector, bool to_file);
      void DecodeMonitoringDataVector(const std::vector<uint32_t>& MonitoringVector, bool to_file);
      void RawDataCheckIncompleteEvents();
  };
  
}
#endif // _TLBStreaming
