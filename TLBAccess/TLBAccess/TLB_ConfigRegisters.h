/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#ifndef __TLB_ConfigRegisters
#define __TLB_ConfigRegisters

#include <bitset>

namespace FASER {
namespace TLB {

/** *******************************************************
 \brief Defines configuration mappings, maintains configurations in registery and provides getters and setters to configurations.
 ******************************************************** */

class ConfigReg {
 private:

  //Definition of the default values for the configuration:
  uint16_t DirectParam=0x0;
  uint16_t DataReadoutParam=0x0;

  /** *******************************************************
   * \brief Definition of ConfigMap.
   * \details An object of type ConfigMap is used to define the position
   * of a TLB configuration setting in the configuration layout mapped in the FPGA.
   * A ConfigMap object is initialised with the index of its first bit, the index of its last bits and the size in bits
   * that a configuration value occupies. The total size of the configuration setting is calculated
   * from the starting bit index and end bit index.
   * \note For configurations that set an array of values (a value per channel),
   * the bit size corresponds to the size of each value in the array. Thus, the bit size is not always equal to the total size.
   * \note A check is done at compile time to verify that
   *       - both the bit size and total size are greater than zero. 
   *       - the bit size a multiple of the total size.
   ******************************************************** */
  template<uint8_t P1, uint8_t P2, size_t P3=1>
  struct ConfigMap {
    static const uint8_t start_bit_idx = P1;/**< index of first bit*/
    static const uint8_t end_bit_idx = P2;/**< index of last bit*/
    static const size_t bit_size = P3;/**< size in bits of (each) configuration value*/
    static const size_t size = end_bit_idx+1 - start_bit_idx;/**< total number of bits mapped to this configuration*/
    static_assert(bit_size>0 && size>0, "FIXME bit_size and size must be larger than zero.");
    static_assert(size%bit_size==0, 
                  "FIXME bit_size for ConfigMap object incompatible with total expected size.");
  };
  
  static constexpr ConfigMap<0,7,1> map_SamplingPhase = {};
  static constexpr ConfigMap<8,23,2> map_InputDelay = {};
  static constexpr ConfigMap<24,26,3> map_InternalTriggerRate = {};
  static constexpr ConfigMap<27,74,8> map_Prescales = {};
  static constexpr ConfigMap<75,81,7> map_TrackerDelay = {};
  static constexpr ConfigMap<82, 88, 7> map_DigitizerDelay = {};
  static constexpr ConfigMap<89, 89> map_LHC_CLK = {};
  static constexpr ConfigMap<90, 101, 12> map_OrbitDelay = {};
  static constexpr ConfigMap<102, 113, 12> map_Deadtime = {};
  static constexpr ConfigMap<114, 133, 20> map_MonitoringRate = {};
  static constexpr ConfigMap<134, 165, 32> map_OutputDest = {};
  static constexpr ConfigMap<166, 173> map_InputEnable = {};
  static constexpr ConfigMap<174, 174> map_Derandomize = {};
  static const uint8_t CONFIGWORD_SIZE = 12;
  static const uint8_t TOTAL_CONFIGWORDS = 15;
  std::bitset<TOTAL_CONFIGWORDS*CONFIGWORD_SIZE> m_configBits;


  /** *******************************************************
   * Direct parameter bit indices
   ******************************************************** */
  static const uint8_t kRESET_BITIDX = 0;
  static const uint8_t kECR_BITIDX = 1;
  static const uint8_t kTRIGENABLE_BITIDX = 2;
  static const uint8_t kSWTRIGGER_BITIDX = 3;
  static const uint8_t kBUSY0DIS_BITIDX = 4;
  static const uint8_t kBUSY1DIS_BITIDX = 5;

  /** *******************************************************
   * Readout parameter bit indices
   ******************************************************** */
  const uint8_t kENABLETRIGDATA_BITIDX = 0;
  const uint8_t kENABLEMONDATA_BITIDX = 1;
  const uint8_t kFIFORESET_BITIDX = 2;

  static const uint8_t kUINT2_MAX = 3;
  static const uint8_t kUINT3_MAX = 7;
  static const uint8_t kUINT7_MAX = 127;
  static const uint16_t kUINT12_MAX = 4095;
  static const uint32_t kUINT20_MAX = 1048575;

 public:  

  template<uint8_t start_bit_idx, uint8_t end_bit_idx, size_t bit_size=1>
  void SetConfig(const ConfigMap<start_bit_idx, end_bit_idx, bit_size>&, unsigned int value, uint8_t ch=0);
  void SetSamplingPhase(uint8_t ch, bool);
  void SetInputDelay(uint8_t ch, unsigned int);
  void SetInternalTriggerRate(unsigned int);
  void SetPrescale(uint8_t ch, unsigned int);
  void SetTrackerDelay(unsigned int);
  void SetDigitizerDelay(unsigned int);
  void SetLHC_CLK(bool);
  void SetOrbitDelay(unsigned int);
  void SetDeadtime(unsigned int);
  void SetMonitoringRate(uint32_t);
  void SetOutputDestination(uint32_t);
  void SetInputEnable(uint8_t ch, bool);
  void SetInternalTriggerGeneratorDerandomizer(bool);

  /** *******************************************************
   * \brief Definitions of direct parameters.
   ******************************************************** */
  struct TLBDirectParameters {
      static const uint16_t Reset = 1<<kRESET_BITIDX;
      static const uint16_t ECR = 1<<kECR_BITIDX;
      static const uint16_t TriggerEnable = 1<<kTRIGENABLE_BITIDX;
      static const uint16_t SoftwareTrigger = 1<<kSWTRIGGER_BITIDX;
      static const uint16_t Busy0Disable = 1<<kBUSY0DIS_BITIDX;
      static const uint16_t Busy1Disable = 1<<kBUSY1DIS_BITIDX;
  };
  void SetDirectParamBit(uint8_t, bool);
  void SetReset(bool bit);
  void SetECR(bool bit);
  void SetTriggerEnable(bool bit);
  void SetSoftwareTrigger(bool bit);
  void SetBusy0Disable(bool bit);
  void SetBusy1Disable(bool bit);
  
  void SetReadoutParamBit(uint8_t, bool);
  void SetEnableTriggerData(bool bit);
  void SetEnableMonitoringData(bool bit);
  void SetReadoutFIFOReset(bool bit);

  uint16_t GetDirectParameters(){ return DirectParam; };

  std::vector<uint16_t> GetConfigWords();
  void PrintConfigs( const std::vector<uint16_t>&);
  template<class T, uint8_t P1, uint8_t P2, size_t P3=1>
  T GetConfigSetting( std::string cfgStr, const ConfigMap<P1,P2,P3>& configMap);
  template<class T, uint8_t P1, uint8_t P2, size_t P3=1>
  void GetConfigSetting( std::string cfgStr, const ConfigMap<P1,P2,P3>& configMap, T& value);
  template<uint8_t P1, uint8_t P2, size_t P3=1>
  void GetConfigSetting( std::string cfgStr, const ConfigMap<P1,P2,P3>& configMap, std::string& value);
  void print(std::string);

  void PrintDirectParam();
  void PrintDataReadoutParam();

};
}
}
#endif /* __TLB_ConfigRegisters */
