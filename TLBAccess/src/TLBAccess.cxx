/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
//Author: Eliott Johnson
#include<iostream>
#include <iomanip>
#include<bitset>
#include<vector>
#include<unistd.h> //for usleep
#include <fstream>
#include <string>
#include "TLBAccess/TLBAccess.h"
#include <string> 

#include "muParser.h"

#define _ms 1000 // used for usleep
#define MAX_VERIFY 2

using namespace FASER;
using namespace FASER::TLB;
using json = nlohmann::json;

// defining NOT function to be used in muParser
// taken from https://github.com/beltoforion/muparser/blob/master/samples/example1/example1.cpp
static mu::value_type Not(mu::value_type v) { return v == 0; }

TLBAccess::~TLBAccess()
{
}

 /**
 *  @brief Send the configuration.
 *
 *  @details
 *   Sends all the Configuration Words at once
 *   using 15 SendAndRetrieve.
 *
 *  @see   GPIOBaseClass::SendAndRetrieve
 *  @see   TLBCmdID
 *  @see   ConfigReg
 *  @see   ConfigureTLB
 * 
 *  @pre  The ConfigWords need to be set with the ConfigureTLB().
 *
 *  Example: 
 *  @code SendAndRetrieve(TLBCmdID::USER_SET_CONFIG,m_tlb_config.ConfigWord0);
    @endcode
 *  @return void
 */
void TLBAccess::SendConfig()
{
 
  auto configWords = m_tlb_config.GetConfigWords();

  for ( auto word: configWords ) {
    SendAndRetrieve(TLBCmdID::USER_SET_CONFIG, word);
  }
}

 /**
 *  @brief Sends the direct parameters.
 *
 *  @details
 *   Sends the direct parameters Reset, ECR, TriggerEnable, Software Trigger and BusyDisable.
 *
 *  @see   GPIOBaseClass::SendAndRetrieve
 *  @see   TLBCmdID
 *  @see   ConfigReg
 *
 *  Example: 
 *  @code SendAndRetrieve(TLBCmdID::SET_DIRECT_PARAM, m_tlb_config.GetDirectParameters());
    @endcode
 *  @return void
 */
void TLBAccess::SendDirectParameters( const uint16_t& param)
{

  std::bitset<16> paramBits = param;
  
  INFO("Setting following direct parameters:\n"
  <<std::setw(20)<<" RESET : "<<std::setfill(' ')<<std::setw(3)<<(paramBits.test(0)?"ON":"OFF")<<std::setfill(' ')<<std::endl
  <<std::setw(20)<<" ECR : "<<std::setfill(' ')<<std::setw(3)<<(paramBits.test(1)?"ON":"OFF")<<std::setfill(' ')<<std::endl
  <<std::setw(20)<<" TriggerEnable : "<<std::setfill(' ')<<std::setw(3)<<(paramBits.test(2)?"ON":"OFF")<<std::setfill(' ')<<std::endl
  <<std::setw(20)<<" SoftwareTrigger : "<<std::setfill(' ')<<std::setw(3)<<(paramBits.test(3)?"ON":"OFF")<<std::setfill(' ')<<std::endl
  <<std::setw(20)<<" Busy0Disable : "<<std::setfill(' ')<<std::setw(3)<<(paramBits.test(4)?"ON":"OFF")<<std::setfill(' ')<<std::endl
  <<std::setw(20)<<" Busy1Disable : "<<std::setfill(' ')<<std::setw(3)<<(paramBits.test(5)?"ON":"OFF")<<std::setfill(' '));

  SendAndRetrieve(TLBCmdID::SET_DIRECT_PARAM, param);
}

 /**
 *  @brief Disables the Trigger.
 *
 *  @details
 *   Sends the direct parameters with Reset = 0, ECR = 0, TriggerEnable = 0, Software Trigger as in the config and BusyDisable as in the config.
 *
 *  @see   GPIOBaseClass::SendAndRetrieve
 *  @see   TLBCmdID
 *  @see   ConfigReg
 *
 *  Example: 
 *  @code SendAndRetrieve(TLBCmdID::SET_DIRECT_PARAM, m_tlb_config.GetDirectParameters());
    @endcode
 *  @return void
 */
void TLBAccess::DisableTrigger()
{
  uint16_t copy = m_tlb_config.GetDirectParameters();
  copy&=~(ConfigReg::TLBDirectParameters::Reset | ConfigReg::TLBDirectParameters::ECR | ConfigReg::TLBDirectParameters::TriggerEnable ); // sets //000
  SendDirectParameters( copy );
}

 /**
 *  @brief Enables the Trigger.
 *
 *  @details
 *   Sends the direct parameters with ECR and Reset as a boolean arguments. First argument is ECR, second argument is Reset.
 *   TriggerEnable is set to 1, Software Trigger, Busy0Disable and Busy1Disable as in the config.
 *
 *  @see   GPIOBaseClass::SendAndRetrieve
 *  @see   TLBCmdID
 *  @see   ConfigReg
 *
 *  @return void
 */
void TLBAccess::EnableTrigger(bool ECR, bool Reset)
{
  uint16_t copy = m_tlb_config.GetDirectParameters();
  copy&=~(ConfigReg::TLBDirectParameters::Reset | ConfigReg::TLBDirectParameters::ECR | ConfigReg::TLBDirectParameters::TriggerEnable ); //Reset, ECR and TriggerEn to OFF //000
  if (ECR){
   copy|=ConfigReg::TLBDirectParameters::ECR; //sets the ECR to ON ///1/
  }
  if (Reset){
   copy|=ConfigReg::TLBDirectParameters::Reset; //sets the Reset to ON ////1
  }
  copy|=ConfigReg::TLBDirectParameters::TriggerEnable; //sets Trigger Enable to ON //1//
  SendDirectParameters( copy );
}

 /**
 *  @brief Send ECR.
 *
 *  @details 
 *  Send the direct parameters with TriggerEnable=0, ECR=1, Reset=0. SoftwareTrigger and BusyDisable as in the config.
 *
 *  @see   GPIOBaseClass::SendAndRetrieve
 *  @see   TLBCmdID
 *  @see   ConfigReg
 *
 *  Example: 
 *  @code SendAndRetrieve(TLBCmdID::SET_DIRECT_PARAM, m_tlb_config.GetDirectParameters());
    @endcode
 *  @return void
 */
void TLBAccess::SendECR()
{
  uint16_t copy = m_tlb_config.GetDirectParameters();
  copy&=~(ConfigReg::TLBDirectParameters::Reset | ConfigReg::TLBDirectParameters::ECR | ConfigReg::TLBDirectParameters::TriggerEnable ); //set Trigger, Reset and ECR to OFF to clean the bits if they were on, //000
  copy|=ConfigReg::TLBDirectParameters::ECR; //sets ECR to ON ///1/
  SendDirectParameters( copy );
}

//THIS I THINK WE DON'T NEED
/**
 *  @brief Function that will send the Readout Parameters.
 *
 *
 *  @see   GPIOBaseClass::SendAndRetrieve
 *  @see   TLBCmdID
 *  @see   ConfigReg
 *
 *  @note  I think we can delete this function because we actually use the values when we start readout.
 *
 *  Example: 
 *  @code SendAndRetrieve(TLBCmdID::SET_DIRECT_PARAM, m_tlb_config.DataReadoutParam);
    @endcode
 *  @return void
 */
/*void TLBAccess::SendDataReadoutParameters()
{
  INFO("Sending Data Readout parameters."<<std::endl);
  SendAndRetrieve(TLBCmdID::SET_DIRECT_PARAM, m_tlb_config.DataReadoutParam);
  INFO("   Done."); 
}*/

/**
 *  @brief Sends the TLB configuration.
 *
 *  @details
 *   Cleans the board, reads the configuration JSON then sets and sends configuration + direct parameters. 
 *   After that, verifies if the configuration has been applied correctly. If the verification has failed
 *   it will try again to configure the board.
 *
 *  @see   ConfigReg
 * 
 *  @note  This uses multiple smaller functions.
 *
 *  Example: 
 *  @code SendAndRetrieve(TLBCmdID::USER_SET_CONFIG,m_tlb_config.ConfigWord0);
    @endcode
 *  @return void.
 */
void TLBAccess::ConfigureTLB(const json& myjson)
{
  try {  
    for (uint8_t ch=0; ch<8; ch++){
      m_tlb_config.SetSamplingPhase(ch, myjson["SamplingPhase"][ch].get<bool>());
      m_tlb_config.SetInputDelay(ch, myjson["InputDelay"][ch].get<unsigned int>());
      m_tlb_config.SetInputEnable(ch,myjson["Input"][ch].get<bool>());
    }
    m_tlb_config.SetInternalTriggerRate(myjson["RandomTriggerRate"].get<uint16_t>());
    for (uint8_t ch=0; ch<6; ch++){
      m_tlb_config.SetPrescale(ch, myjson["Prescale"][ch].get<uint16_t>());
    }
    if ( myjson.contains("Derandomize")){
      m_tlb_config.SetInternalTriggerGeneratorDerandomizer(myjson["Derandomize"].get<bool>());
    }
    else m_tlb_config.SetInternalTriggerGeneratorDerandomizer(false);
    m_tlb_config.SetTrackerDelay(myjson["TrackerDelay"].get<uint16_t>());
    m_tlb_config.SetDigitizerDelay(myjson["DigitizerDelay"].get<uint16_t>()); 
    m_tlb_config.SetLHC_CLK(myjson["LHC_CLK"].get<bool>());
    m_tlb_config.SetOrbitDelay(myjson["OrbitDelay"].get<uint16_t>());
    m_tlb_config.SetDeadtime(myjson["Deadtime"].get<uint16_t>());
    m_tlb_config.SetMonitoringRate(myjson["MonitoringRate"].get<uint32_t>());
    m_tlb_config.SetOutputDestination(myjson["OutputDestination"].get<uint32_t>());

    m_tlb_config.SetReset(myjson["Reset"].get<bool>());
    m_tlb_config.SetECR(myjson["ECR"].get<bool>());
    m_tlb_config.SetTriggerEnable(myjson["TriggerEnable"].get<bool>());
    m_tlb_config.SetSoftwareTrigger(myjson["SoftwareTrigger"].get<bool>());
    m_tlb_config.SetBusy0Disable(myjson["Busy0Disable"].get<bool>());
    if ( myjson.contains("Busy1Disable")){
      m_tlb_config.SetBusy1Disable(myjson["Busy1Disable"].get<bool>());
    }
    else m_tlb_config.SetBusy1Disable(true);
  }
  catch ( nlohmann::detail::type_error&e) {
    std::stringstream error_string;
    error_string<<"Invalid value type in parsed configurations:"<<e.what();
    THROW(TLBAccessException, error_string.str());
  }
  SendConfig();
  SendDirectParameters( m_tlb_config.GetDirectParameters());
  //m_tlb_config.PrintDirectParam();
}

/**
 *  @brief Sends the TLB configuration and Verifies it.
 *
 *  @details
 *   Cleans the board, reads the configuration JSON then sets and sends configuration + direct parameters. 
 *   After that, verifies if the configuration has been applied correctly. If the verification has failed
 *   it will try again to configure the board.
 *
 *  @see   ConfigReg
 * 
 *  @note  This uses multiple smaller functions.
 *
 *  Example: 
 *  @code SendAndRetrieve(TLBCmdID::USER_SET_CONFIG,m_tlb_config.ConfigWord0);
    @endcode
 *  @return void
 *  @throws TLBAccessException Thrown if configuration failed after `NumberOfVerify` number of tries.
 */
void TLBAccess::ConfigureAndVerifyTLB(const json& myjson)
{
  bool ConfigSuccess = false;
  uint8_t NumberOfVerify = 0;
  
  while (!ConfigSuccess && NumberOfVerify<=MAX_VERIFY){//if there is an error then you can retry a number of time to reconfigure the TLB
    NumberOfVerify+=1;
    ConfigureTLB(myjson);
    if (VerifyConfiguration()){
      ConfigSuccess=true;
      INFO("TLB configuration successful.");
    }
    else {
      WARNING("   Verification failed - there was an error configuring the board. Number of attemps : "<<NumberOfVerify<<"/"<<MAX_VERIFY);
    }
  }
  
  if (!ConfigSuccess) THROW(TLBAccessException, "TLB Configuration failed.");
}

/**
 *  @brief Sends the LUT.
 *
 *  @details Sends the LUT to the board and verifies (by CRC check) if 
 *  the configuration has been done properly (i.e. there has not been any transfer error).
 *
 *  @return void
 *  @throws TLBAccessException Thrown if LUT configuration failed.
 */
void TLBAccess::ConfigureLUT(const json& trigjson) 
{
  INFO("Configuring TLB lookup table.");
  uint8_t DevNum = 0;
  
  if (!SetLUT(DevNum, trigjson)){ 
    THROW(TLBAccessException, "Setting LUT failed.");
  }

  if (VerifyLUT_byCRC(DevNum)){ INFO("   LUT Verification OK");}
  else { THROW(TLBAccessException, "LUT Verification failed.");}
}

/**
 *  @brief Verifies the configuration on the board.
 *
 *  @details
 *   Read the configuration on the board and compares it to the ConfigWords.
 *
 * 
 *  @warning I run twice 15 SendAndRetrieve to "refresh the buffer" because otherwise you get the old configuration
 *  but I'm not sure why you need to do this.
 *
 *  @return a boolean called VerifySuccess that is true if no error.
 */
bool TLBAccess::VerifyConfiguration()
{
  bool VerifySuccess = true;

  std::vector<uint16_t> returnedConfigs;
  returnedConfigs.resize(15);
  std::vector<uint16_t> configWords = m_tlb_config.GetConfigWords();

  uint16_t param = 0x0,answer = 0x0;
  for (int i = 0; i < 15; i++){
    SendAndRetrieve(TLBCmdID::USER_GET, param, &answer);
    if (i==0 && (answer>>12)!=0) {i=-1;} //avoids not starting at zero
   }
  for (int i = 0; i < 15; i++){ //I don't know why but you need to read twice to get a correct readout TODO : why?
    SendAndRetrieve(TLBCmdID::USER_GET, param, &answer);
    if (i==0 && (answer>>12)!=0) {i=-1;} //avoids not starting at zero
    else{
      returnedConfigs.at(i) = answer;
      if (configWords.at(i) != answer){ERROR("Configuration verification failed for word "<<i);VerifySuccess=false;}
    }
  }

  m_tlb_config.PrintConfigs( returnedConfigs );

  return (VerifySuccess);
}


/**
 *  @brief Starts the data readout.
 *
 *  @details
 *   Sends a SendAndRetrieve command to the TLB to start the data readout using a param
 *   that specifies if you want trigger or monitoring. It also check wether the param==answer.
 *   It also starts a new thread for data polling.
 *
 *  @param param is a unint16_t that combines EnableTriggerData EnableMonitoringData and ReadoutFIFOReset.
 *
 *  @return void
 */
void TLBAccess::StartReadout(const uint16_t& param)
{
  INFO("Enabling data readout");
  m_DataWordOutputBuffer.clear();
  mMutex_TLBEventData.lock(); //here we have to lock because its a mutex object and we are accessing it in a thread with shared ressources
  m_TLBEventData.clear(); //this is the vector of vector being cleared
  mMutex_TLBEventData.unlock();
  uint16_t answer = 0x0;
  SendAndRetrieve(TLBCmdID::DATA_READOUT, param, &answer);
  if (answer!=param) {ERROR("Board answer does not match parameters sent. This is a sign of a configuration communication error.");} // TODO: catch this error.
  // now start the data polling in a separate thread
  m_daqRunning = true;
  m_daqThread = new std::thread(&TLBAccess::PollData, this);
}

 /**
 *  @brief Send 'stop readout'command to TLB
 *
 *  @details
 *   Sends a SendAndRetrieve with a special parameter that stops the readout.
 *   It also closes the data polling thread.
 *
 *  @param param is a unint16_t = 0x1.
 *
 *  @return void
 */
void TLBAccess::StopReadout(){
  INFO("Stopping read out ...");
  uint16_t param = 0x1;
  SendAndRetrieve(TLBCmdID::DATA_READOUT, param);
  // and now stop the data polling thread
  if (m_daqThread != nullptr){
    m_daqRunning = false;
    if (m_daqThread->joinable()) m_daqThread->join();
    delete m_daqThread;
    m_daqThread = nullptr;
  }
  INFO("Stopped readout thread."); 
}
 
 /**
 *  @brief Return a vector of TLB RAW data per event.
 *
 *  @details
 *   
 *
 *
 *  @note The internally stored raw data will be deleted when called!
 *
 *  @return Returns a vector<std::vector<uint32_t>> containing events in the first vector and the event's word in a vector inside the first vector.
 */
std::vector<std::vector<uint32_t>> TLBAccess::GetTLBEventData() 
{
   mMutex_TLBEventData.lock(); 
   auto copy = m_TLBEventData; 
   m_TLBEventData.clear();
   mMutex_TLBEventData.unlock(); 

   return copy;
}

 /**
 *  @brief Read any data pending in the data-out buffer of the TLB
 *
 *  @details
 *   
 *
 *  @return void
 */
void TLBAccess::PollData(){
  std::vector<unsigned char> tmp;
  uint32_t TLBWord = 0;
  int TLBWord_ID = 0;
  int bytesRead = 0;
  m_dataRate = 0;
  unsigned long long totalBytesRead(0);
  int sumBytesTransfered = 0;
  int transfers = 0;
  int HeaderNumber = 0;
  bool waitingForEOD(true); // wait for EOD word before terminating the processing
  unsigned long waitCnt(0);

  auto start = std::chrono::high_resolution_clock::now();
  auto stop = start; 
  
  while (m_daqRunning || bytesRead !=0 || waitingForEOD ){
  //usleep(50*_ms);
  
    try {
      if (m_DEBUG){TRACE("Issuing read, last read returned "<< bytesRead<< " bytes");}
      tmp = m_interface->ReadData(bytesRead);
      sumBytesTransfered += bytesRead;
      totalBytesRead += bytesRead;
      transfers++;
      
      stop = std::chrono::high_resolution_clock::now();
      auto duration = std::chrono::duration_cast<std::chrono::seconds>(stop - start);
      if (duration.count() > 10){
        m_dataRate = (double)sumBytesTransfered/(double)(1000*duration.count());
        start =  std::chrono::high_resolution_clock::now();
        if (m_DEBUG){TRACE("   Data rate = "<<m_dataRate << " kB/sec");}
        sumBytesTransfered = 0;
      } 
    }
    catch (...){
    ERROR("Read Serial threw an error. Continuing anyways");
    }

    for (unsigned int i = 0; i < bytesRead; i++){
    TLBWord |= ((uint32_t)tmp[i]) << (TLBWord_ID*8);
    TLBWord_ID++;
      if (TLBWord_ID == 4) { // word complete 
        if (TLBDecode::IsTriggerHeader(TLBWord) || TLBDecode::IsMonitoringHeader(TLBWord) || TLBDecode::IsEndOfDAQ(TLBWord)){ //if it finds any header or trailer then stores an event
          if (m_DataWordOutputBuffer.size()!=0){
            mMutex_TLBEventData.lock();
            m_TLBEventData.push_back(m_DataWordOutputBuffer); // store complete (or incomplete) event
            mMutex_TLBEventData.unlock();
          }
          m_DataWordOutputBuffer.clear();
          HeaderNumber+=1;
          if (m_DEBUG){
            TRACE("   Wrote header number: "<<HeaderNumber);
          }
        }
        if (!TLBDecode::IsEndOfDAQ(TLBWord)){
          m_DataWordOutputBuffer.push_back(TLBWord); //Once the word is complete, we push it to this vector.
        }
        else waitingForEOD = false;
        TLBWord_ID = 0;
        TLBWord = 0;
      }
    }

   if ( !bytesRead && !m_daqRunning) {
     waitCnt++;
     if (waitCnt > 100) {
       WARNING("Forcing PollData thread to exit! Probably lost / corrupted data. Remaining words in buffer: "<< m_DataWordOutputBuffer.size() << "  EOD word found: " << (waitingForEOD == false));
       return;
     } // quitting if DAQ stopped a while ago
    }

  }
  INFO("   DAQ: bytes transfered "<<std::dec<<totalBytesRead<<" in "<<transfers<<" transfers");
}

 /**
 *  @brief Load LUT from file to x_LUT
 *
 *  @details Loads LUT from config file to x_LUT vector. One vector component 
 *  contains four 4-bit numbers from LUT. Basic checks of config file 
 *  validiy are performed. Path to file has to be absolute. 
 *   
 *
 *  @return true
 */
bool TLBAccess::LoadLUT(std::string x_PathToLUT, std::vector<uint16_t> *x_LUT)
{   
    std::ifstream LUT_File(x_PathToLUT);
    std::string l_val1, l_val2, l_val3, l_val4;
    int VectorComponent;   

    x_LUT->clear(); //Check that vector is empty 

    while (LUT_File >> l_val1 >> l_val2 >> l_val3 >> l_val4){
        if (l_val1.size() != 4 | l_val2.size() != 4 | l_val3.size() != 4 | l_val4.size() != 4) {
            ERROR(" Provided LUT contains value with size different from 4 bits!");
            return false;
        }

        if (!IsBinary(l_val1) | !IsBinary(l_val2) | !IsBinary(l_val3) | !IsBinary(l_val4)){
            ERROR(" Provided LUT contains character different from 0 or 1!");
            return false;
        }

        //concatenate 4 4-bit values from the file and convert them to an integer       
        VectorComponent = std::stoi(l_val4 + l_val3 + l_val2 + l_val1, nullptr, 2); 

        x_LUT->push_back(VectorComponent & 0xffff);
    }    
   
    if (x_LUT->size() != 64){
        ERROR("Suspicious LUT dimension. Input file is expected to have 256 lines!");
        return false;
    }    
 
    return true;
}

/**
*  @brief Checks if given string contains only binary characters (i.e. 0 or 1)
*
*   @details Checks if provided string contains only characters 0 or 1.
*   
*
*  @return true
*/
bool TLBAccess::IsBinary(std::string l_value)
{
    if (l_value.find_first_not_of("01", 0) != -1){
        return false;
    }
    return true;
}

/**
*  @brief Send LUT to the board.
*
*  @details Send LUT to the board.
*   
*
*  @return true
*/
bool TLBAccess::SetLUT(uint8_t x_device, const json trigjson)
{    
    uint8_t l_CmdID = 0x04;
    std::vector<uint16_t> l_LUT;
    
    // cast the LUT trigger stream configs
    std::vector<std::string> configs;
    configs.push_back(trigjson.value("Trig0", "0"));
    configs.push_back(trigjson.value("Trig1", "0"));
    configs.push_back(trigjson.value("Trig2", "0"));
    configs.push_back(trigjson.value("Trig3", "0"));
    
    // hardcoded translation dictionary for physical to config space
    // should be in a config at some point
    std::map<std::string, std::string> trig_bit_map;
    trig_bit_map["Tin0"]="FirstVetoLayer";
    trig_bit_map["Tin1"]="SecondVetoLayer";
    trig_bit_map["Tin2"]="TimingLayerTop";
    trig_bit_map["Tin3"]="TimingLayerBottom";
    trig_bit_map["Tin4"]="PreshowerLayer";
    trig_bit_map["Tin5"]="CalorimeterBottom";
    trig_bit_map["Tin6"]="CalorimeterTop";
    trig_bit_map["Tin7"]="Unused";
    
    // these mapping is included to translate from logical syntax to necessary muParser syntax
    trig_bit_map["&&"]="&";
    trig_bit_map["||"]="|";

    if(!GenerateLUT(l_LUT, configs, trig_bit_map)){
        return false;
    }

    DEBUG("Printing LUT that has been read : N="<<l_LUT.size());
    for(int ilut=0; ilut<(int)l_LUT.size(); ilut++){
      DEBUG("LUTCHECK : "<<ilut<<"  "<<l_LUT.at(ilut));
    }

    if(!SendMultiargAndRetrieve(l_CmdID, x_device, l_LUT)){
        ERROR("SetLUT_Send() failed.");       
        return false;
    }
    
    if(!VerifyLUT_byBits(x_device, l_LUT)){
        ERROR("Error during transmission. Try loading LUT another time.");
        return false;
    }

    return true;
}

/**
*  @brief Get LUT from the board (+ optionally CRC).
*
*  @details  Read out LUT from the board. LUT is encoded in form of vector of uint16_t.
 Can retrieve CRC red out from the board.
*   
*
*  @return l_LUT
*/
std::vector<uint16_t> TLBAccess::GetLUT(uint16_t x_device, uint16_t *x_CRC)
{
    uint8_t l_CmdID = 0x05;
    std::vector<uint16_t> l_LUT;

    if(!SendAndRetrieveMultiarg(l_CmdID, x_device, &l_LUT, x_CRC)){
        ERROR("GetLUT() failed.");
    }

    return l_LUT;
}

/**
*  @brief Decode LUT encoded to vector of 16-bit integers to readable format.
*
*  @details  Decodes LUT (encoded to vector of uint16_t) to vector of individual values
 stored to the LUT cells.
*
*  @return l_DecodedLUT
*/
std::vector<uint16_t> TLBAccess::DecodeLUT(std::vector<uint16_t> x_LUT)
{
    std::vector<uint16_t> l_DecodedLUT(x_LUT.size()*4);

    for (int i = 0; i < x_LUT.size(); i++)
    {   
        l_DecodedLUT[4*i]     =  x_LUT[i] & 0x000f;
        l_DecodedLUT[4*i + 1] = (x_LUT[i] & 0x00f0) >> 4;
        l_DecodedLUT[4*i + 2] = (x_LUT[i] & 0x0f00) >> 8;
        l_DecodedLUT[4*i + 3] = (x_LUT[i] & 0xf000) >> 12;
    } 

    return l_DecodedLUT;   
}

/**
*  @brief Verification of the configuration to avoid transfer errors. Comparing only CRC.
*
*  @details  Reads out LUT from the board, localy computes its CRC and compares it to the 
 CRC which was retrieved from the board. Returns true for matching CRCs.
*
*  @return true
*/
bool TLBAccess::VerifyLUT_byCRC(uint8_t x_device)
{
    uint16_t l_computedCRC;
    uint16_t l_receivedCRC;
    std::vector<uint16_t> l_receivedLUT;

    CRC_16_1A2EB crc16_1A2EB;
    UFE_CRC CRC_Class = UFE_CRC(crc16_1A2EB.CRC);

    l_receivedLUT = GetLUT(x_device, &l_receivedCRC); 
    l_computedCRC = CRC_Class.crc(CRC_Class.convert16to8bit(l_receivedLUT));

    if (l_computedCRC != l_receivedCRC){
        WARNING("Received CRC does not match the one localy computed from the LUT.");
        return false;
    }
    
    return true;      
}

/**
*  @brief Verification of the configuration to avoid transfer errors. Bit by bit comparison of LUT from the device with LUT provided by user.
*
*  @details   Reads out LUT from the board and compares it bit by bit to LUT (encoded 
 to vector of uint16_t) provided by user. Returns true for matching LUTs.
*
*  @return true
*/
bool TLBAccess::VerifyLUT_byBits(uint8_t x_device, std::vector<uint16_t> x_LUT)
{
    std::vector<uint16_t> l_receivedLUT;

    l_receivedLUT = GetLUT(x_device);
 
    if (x_LUT != l_receivedLUT){
        WARNING("LUT provided does not match LUT received from TLB.");
        return false;
    }    
   
    return true;
}

//Following comand is not used for TLB
/*bool TLBAccess::ApplyConfig(uint8_t x_device)
{
    uint8_t l_CmdID = 0x06;
    uint8_t l_CmdArg = (0x1 << x_device);
    
    if (x_device > 7){
        std::cout << "WARNING: Device number should be between 0-7." << std::endl;
        return false;
    }

    if (!SendAndRetrieve(l_CmdID, l_CmdArg)){
        std::cout << "ApplyConfig() failed." << std::endl;
    }
    
    return true;
}*/

/**
*  @brief Creates a vector of LUT integers given a set of trigger config logical expressions
*
*  @details   Takes a vector of logical expressions like "(Tin1&&Tin2)||(Tin2&&Tin6)"
*             and produces a vector of LUT configuration integers
*
*  @return true/false
*/
bool TLBAccess::GenerateLUT(std::vector<uint16_t>& lut, std::vector<std::string> configs, std::map<std::string, std::string> trig_bit_map){
  for(int icon; icon<(int)configs.size(); icon++){
    DEBUG("Initial Config "<<icon<<" : "<<configs.at(icon));
  }

  if(configs.size()!=4){
    WARNING("You don't have enough trigger config strings : "<<configs.size());    
  }
  
  // convert from the input syntax to that used by muParser
  TranslateConfigs(configs, trig_bit_map);
  
  for(int icon; icon<(int)configs.size(); icon++){
    DEBUG("Readable Config "<<icon<<" : "<<configs.at(icon));
  }
  
  // some verbose parsing of the syntax and whether it is proper
  VerifyConfigs(configs, trig_bit_map);
    
  mu::Parser p0,p1,p2,p3;
  p0.DefineInfixOprt(_T("!"), Not);  
  p1.DefineInfixOprt(_T("!"), Not);  
  p2.DefineInfixOprt(_T("!"), Not);  
  p3.DefineInfixOprt(_T("!"), Not);  

  p0.SetExpr(configs.at(0));
  p1.SetExpr(configs.at(1));
  p2.SetExpr(configs.at(2));
  p3.SetExpr(configs.at(3));

  double Tin[8]    = {0,0,0,0,0,0,0,0};

  for(int ibit=0; ibit<8; ibit++){
    p0.DefineVar("Tin"+std::to_string(ibit),&Tin[ibit]);
    p1.DefineVar("Tin"+std::to_string(ibit),&Tin[ibit]);
    p2.DefineVar("Tin"+std::to_string(ibit),&Tin[ibit]);
    p3.DefineVar("Tin"+std::to_string(ibit),&Tin[ibit]);
  }

  std::vector<int> expanded_lut;
  expanded_lut.clear();
  
  for(int inp=0; inp<256; inp++){
  
    // get the appropriate bitmask representation of the slot
    // and its inverse saved in Tin and notTin
    for(int ibit=0; ibit<8; ibit++){
      int val=0;
      if(inp & (1<<ibit)){
        val=1;
      }
      Tin[ibit] = val;
    }
      
    // evaluate the four trigger bits if the expressions are valid
    int tbp_bit[4];
    try{
      tbp_bit[0] = (int)p0.Eval();
      tbp_bit[1] = (int)p1.Eval();
      tbp_bit[2] = (int)p2.Eval();
      tbp_bit[3] = (int)p3.Eval();
    }
    catch (mu::Parser::exception_type &e){
      ERROR("Not possible to parse trigger configs : ");
      ERROR("Trig0 : " << configs.at(0));
      ERROR("Trig1 : " << configs.at(1));
      ERROR("Trig2 : " << configs.at(2));
      ERROR("Trig3 : " << configs.at(3));
      ERROR("muParser Message : "<<e.GetMsg());
      ERROR("muParser Expression : "<<e.GetExpr());
      ERROR("muParser Token : "<<e.GetToken());
      ERROR("muParser Position : "<<e.GetPos());
      ERROR("muParser Error Code : "<<e.GetCode());
      return false;
    }
    
    // enter the trigger stream bit into the slot from the right
    int tbp = (tbp_bit[3]<<3) | (tbp_bit[2]<<2) | (tbp_bit[1]<<1) | (tbp_bit[0]);
          
    std::string trig_combo = ConvertIntToWord(inp, 8);
    std::string tbp_word = ConvertIntToWord(tbp, 4);

    DEBUG("Trigger Before Prescale(TBP) : "<<trig_combo<<"  "<<tbp_word<<"  "<<tbp);
    
    expanded_lut.push_back(tbp);
  }

  // check for size of 256
  if(expanded_lut.size()!=256){
    ERROR("The generated expanded LUT size is not 256 - something went awry");
    return false;
  }

  // translate the TBP strings to the condensed LUT format expected
  lut.clear();
    
  int l_val1, l_val2, l_val3, l_val4;
  int VectorComponent;

  for(int ilut=0; ilut<expanded_lut.size(); ilut+=4){

    l_val1 = expanded_lut.at(ilut+0);
    l_val2 = expanded_lut.at(ilut+1);
    l_val3 = expanded_lut.at(ilut+2);
    l_val4 = expanded_lut.at(ilut+3);

    DEBUG("LUT "<<ilut<<" : "<<l_val1<<" "<<l_val2<<" "<<l_val3<<" "<<l_val4);

    //concatenate 4 4-bit values from the file and convert them to an integer
    VectorComponent = (l_val4<<12) | (l_val3<<8) | (l_val2<<4) | l_val1;
    
    DEBUG("VectorComponent : "<<VectorComponent);

    lut.push_back(VectorComponent & 0xffff);
  }

  DEBUG("LUT size : "<<lut.size());
  
  return true;
}

/**
*  @brief Translation of config logical expressions from detector-based to trigger-based
*
*  @details   Relies on a mapping of detector subcomponents like TimingLayerBottom and translates
*             these to their logical equivalent Tin3 with which the LUT expression is evaluated.
*             The full set of pairings is :
*         
*             trig_bit_map["Tin0"]="FirstVetoLayer";
*             trig_bit_map["Tin1"]="SecondVetoLayer";
*             trig_bit_map["Tin2"]="TimingLayerTop";
*             trig_bit_map["Tin3"]="TimingLayerBottom";
*             trig_bit_map["Tin4"]="PreshowerLayer";
*             trig_bit_map["Tin5"]="CalorimeterBottom";
*             trig_bit_map["Tin6"]="CalorimeterTop";
*             trig_bit_map["Tin7"]="Unused";
*
*  @return VOID
*/
void TLBAccess::TranslateConfigs(std::vector<std::string>& configs, std::map<std::string, std::string> trig_bit_map){
  DEBUG("Running TranslateLUT");
  
  for (auto& [key, value]: trig_bit_map) {
    for(int icon=0; icon<(int)configs.size(); icon++){
      std::string key_str = (std::string)key;
      std::string value_str = (std::string)value;
      replaceAll(configs.at(icon), value_str, key_str);
    }    
  }
}

/**
*  @brief Verifies the validity of the config prior to evaluating the LUT trigger bit
*
*  @details   Checks the syntax of the expression before evaluation my muParser library
*
*  @return VOID
*/
void TLBAccess::VerifyConfigs(std::vector<std::string> configs, std::map<std::string, std::string> trig_bit_map){
  DEBUG("Running VerifyConfigs");

  for(int icon=0; icon<(int)configs.size(); icon++){
  
    std::string parse_config = configs.at(icon);
  
    // remove all of the logical variables 
    for (auto& [key, value]: trig_bit_map) {
      replaceAll(parse_config, value, " ");
      replaceAll(parse_config, key, " ");
      replaceAll(parse_config, "not", " ");
    }    
    
    // look for even number of &
    size_t n_ampersand = std::count(parse_config.begin(), parse_config.end(), '&');
    if(n_ampersand%2==1){
      ERROR("Improper expression of logic with &");
    }
    replaceAll(parse_config, "&", "");
    
    // look for even number of |
    size_t n_bar = std::count(parse_config.begin(), parse_config.end(), '|');
    if(n_bar%2==1){
      ERROR("Improper expression of logic with |");
    }    
    replaceAll(parse_config, "|", "");
    
     // look for equal number of occurences of parentheses
    size_t n_paren_left  = std::count(parse_config.begin(), parse_config.end(), '(');
    size_t n_paren_right = std::count(parse_config.begin(), parse_config.end(), ')');
    if(n_paren_left!=n_paren_left){
      ERROR("Improper balancing of parentheses");
    }
    replaceAll(parse_config, "(", "");
    replaceAll(parse_config, ")", "");
    
    size_t n_space = std::count(parse_config.begin(), parse_config.end(), ' ');
    if(parse_config.size()!=n_space){
      if(configs.at(icon).compare("0")!=0){
	ERROR("There is something wrong with the parsing of the trigger stream config : "<<configs.at(icon));
      }
    }
    
  }
}

/**
*  @brief Replaces a substring every time it occurs in a string
*
*  @details  Replaces a substring every time it occurs in a string 
*
*  @return VOID
*/
void TLBAccess::replaceAll(std::string& str, std::string from, std::string to) {
    if(from.empty())
        return;
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); 
    }
}

/**
*  @brief converts an unsigned int to a string of 0's and 1's formatted in an appropriate manner as a 32 bit word                        
*
*  @details  
*
*  @return std::string
*/
std::string TLBAccess::ConvertIntToWord(unsigned int word, int nbits){
  std::string wordout;

  for(int iBit=nbits-1; iBit>=0; iBit--){
    int bitval = (word & (1<<iBit)) >> iBit;
    wordout += std::to_string(bitval);
    if(iBit%4==0)
      wordout += " ";
  }

  return wordout;
}
