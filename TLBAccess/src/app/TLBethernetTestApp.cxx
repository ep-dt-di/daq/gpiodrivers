/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#include <iostream>
#include <string>
#include <bitset>
#include <chrono>
#include <ctime>    
#include <unistd.h>
#include "../../../GPIOBase/GPIOBase/udpInterface.h"
#include "../../../GPIOBase/GPIOBase/GPIOBaseClass.h"
#include "../../TLBAccess/TLBAccess.h"
#include "EventFormats/TLBDataFragment.hpp"

using namespace FASER;
using namespace std;
using namespace TLBDataFormat;


void TestSockets(std::string SCIP, std::string DAQIP, int l_boardID)
{
  std::stringstream boardIP;
  boardIP << "10.11.65."<<16+l_boardID;

  std::cout << std::endl << "---------------Testing UDP interface standalone (connect + transfer UDP packet)---------------" << std::endl;
  std::cout << "Connecting to GPIO board with IP " << boardIP.str() << std::endl;

  CommunicationInterface *udp = new udpInterface(SCIP, DAQIP, boardIP.str());

  try {
    udp->Connect();
    std::cout << "\nIf no errors appeared, test was successful." << std::endl;
    } 
  catch (...) {
    std::cout << "Could not open connection to GPIO. Good luck next time"<<std::endl;
    return;
    }

  delete udp;
}


void TestConnection(std::string SCIP, std::string DAQIP, int l_boardID)
{
  std::cout << "-------------Initializing TRBAccess using UDP interface--------------" << std::endl;
  TLBAccess *tlb = new TLBAccess(SCIP, DAQIP, false, l_boardID);

  cout << "\nIf no errors appeared, test was successful." << std::endl;
  delete tlb;
}


void TestConfiguration(std::string SCIP, std::string DAQIP, int l_boardID, std::string cfg_fileName)
{  
  std::cout << "-------------Initializing TRBAccess using UDP interface--------------" << std::endl; 
  TLBAccess *tlb = new TLBAccess(SCIP, DAQIP, false, l_boardID); 
  
  nlohmann::json TLBConfigJSON; 
  std::ifstream cfg_file(cfg_fileName);
  try { 
    TLBConfigJSON = nlohmann::json::parse(cfg_file);
  }catch ( std::exception& e) {
    std::cout<<"ERROR Invalid configuration file provided: "<<e.what()<<
    "\nCheck file path and name?"<<std::endl;
    return;
  }

  tlb->ConfigureTLB(TLBConfigJSON);
  if (tlb->VerifyConfiguration()){
    cout << "Configuration successful." << endl;
  }
  else {
    cout << "ERROR: Configuration failed. Verification unsuccessful." << endl;
  }
 
  delete tlb;
}


void TestDataTaking(std::string SCIP, std::string DAQIP, int Interval, int l_boardID, std::string cfg_fileName)
{
  std::cout << "-------------Initializing TRBAccess using UDP interface--------------" << std::endl;
  TLBAccess *tlb = new TLBAccess(SCIP, DAQIP, false, l_boardID); 
  std::vector<std::vector<uint32_t>> vector_of_raw_events;
  std::chrono::duration<double> elapsed_seconds =  std::chrono::duration<double>(0);
  std::chrono::duration<double> time_difference =  std::chrono::duration<double>(0);
  uint32_t BCID = -1;
  uint32_t  BCID_previous = -1;
  uint32_t EventID = -1; 
  uint32_t EventID_previous = -1; //TODO What other data should be checked?  

  nlohmann::json TLBConfigJSON; 
  std::ifstream cfg_file(cfg_fileName);
  try { 
    TLBConfigJSON = nlohmann::json::parse(cfg_file);
  }catch ( std::exception& e) {
    std::cout<<"ERROR Invalid configuration file provided: "<<e.what()<<
    "\nCheck file path and name?"<<std::endl;
    return;
  }

  tlb->ConfigureTLB(TLBConfigJSON);
  if (tlb->VerifyConfiguration()){
    cout << "Configuration successful." << endl;
  }
  else {
    cout << "ERROR: Configuration failed. Verification unsuccessful." << endl;
  }


  tlb->EnableTrigger(true, true); 
  sleep(1);
  tlb->StartReadout(0x2000);
  sleep(1);
  auto start_time = std::chrono::system_clock::now();
  auto cout_time = std::chrono::system_clock::now();
  
  while (elapsed_seconds.count() < Interval || vector_of_raw_events.size() != 0){
    vector_of_raw_events = tlb->GetTLBEventData();
    time_difference = std::chrono::system_clock::now() - cout_time;
    //if (time_difference.count() > 1){
    //if (vector_of_raw_events.size() != 0){
      //cout <<  "Size of vector_of_raw_events: " << vector_of_raw_events.size() << endl;
      //cout_time = std::chrono::system_clock::now();
    //}
    for (auto event : vector_of_raw_events){
      TLBDataFragment TLBData = TLBDataFragment(event.data(), sizeof(event.data()));
  
      //TODO What else checks can be done?
      if (BCID_previous == -1 && EventID_previous == -1){
        BCID_previous = TLBData.bc_id();
        EventID_previous = TLBData.event_id();
        //cout << "Everything is fine for event with EventID " << TLBData.event_id() << endl;
      }
      else {
        BCID = TLBData.bc_id();
        EventID = TLBData.event_id();
        //cout << "Everything is fine for event with EventID " << TLBData.event_id() << endl;
        if (EventID != EventID_previous + 1){
         std::cout << "ERROR: EventID did not increase by 1. Two consecutive EventIDs are: " << EventID_previous << " and " << EventID << std::endl; 
         //return;
        }

        BCID_previous = BCID; //Can we do some checks on BCID?
        EventID_previous = EventID;  
      }
    }
    elapsed_seconds = std::chrono::system_clock::now() - start_time;
  }
  tlb->DisableTrigger();
  usleep(15);
  tlb->StopReadout();
  
  delete tlb;       
}


void TestFunctions(std::string SCIP, std::string DAQIP, int NumberOfAttempts, int l_boardID, std::string cfg_fileName){
  std::cout << "-------------Initializing TRBAccess using UDP interface--------------" << std::endl;
  TLBAccess *tlb = new TLBAccess(SCIP, DAQIP, false, l_boardID);

  nlohmann::json TLBConfigJSON; 
  std::ifstream cfg_file(cfg_fileName);
  try { 
    TLBConfigJSON = nlohmann::json::parse(cfg_file);
  }catch ( std::exception& e) {
    std::cout<<"ERROR Invalid configuration file provided: "<<e.what()<<
    "\nCheck file path and name?"<<std::endl;
    return;
  }
 
  cout << "----------Testing GetFirmwareVersion------------" << endl;
  static const uint16_t argFPGAtop       = 0x0000;
  static const uint16_t argFPGAdecoder   = 0x0001;
  static const uint16_t argHardware      = 0x0002;
  static const uint16_t argProductID     = 0x0003;
  
  unsigned int FPGAmajor = 0, FPGAminor = 0, FPGAencoder = 0, hardware = 0, product = 0;
  uint16_t answArg = 0;
  
  if (!tlb->GetSingleFirmwareVersion(argFPGAtop, answArg)){
    std::cout << "ERROR: GetSingleFirmwareVersion(argFPGAtop) failed"<<std::endl;
    }
      
  uint16_t m_FPGA_version = answArg;        
  FPGAminor = 0x000f & answArg;
  FPGAmajor = (0xfff0 & answArg) >> 4;
               
  if (!tlb->GetSingleFirmwareVersion(argFPGAdecoder, answArg)){
    std::cout << "ERROR: GetSingleFirmwareVersion(argFPGAdecoder) failed"<<std::endl;
    return;
  }
  FPGAencoder = answArg;
  if (!tlb->GetSingleFirmwareVersion(argHardware, answArg)){
    std::cout << "ERROR: GetSingleFirmwareVersion(argHardware) failed"<<std::endl;
    return;
  }
  hardware = answArg;
   
  if (!tlb->GetSingleFirmwareVersion(argProductID, answArg)){
    std::cout << "ERROR: GetSingleFirmwareVersion(argProductID) failed"<<std::endl;
    return;
  }
  product = answArg;
    
  cout << "   Firmware versions: "<<endl;
  cout << "   FPGA:       "<<FPGAmajor<<"."<<FPGAminor<<" = 0x"<<std::hex<<m_FPGA_version<<endl;
  cout << "   Encoder:    "<<FPGAencoder<<endl;
  cout << "   Hardware:   "<<hardware<<endl;
  cout << "   Product ID: "<<product<<endl  <<endl;
                                                           
  for (int i = 0; i <= NumberOfAttempts; i++){
    tlb->GetSingleFirmwareVersion(argFPGAtop, answArg);
    if ((FPGAminor != (0x000f & answArg)) || (FPGAmajor != ((0xfff0 & answArg) >> 4))){
      cout << "Get Firmware Version failed in attempt: " << i << endl;
      return;
    }
    tlb->GetSingleFirmwareVersion(argFPGAdecoder, answArg);
    if (FPGAencoder != answArg){
      cout << "Get Firmware Version failed in attempt: " << i << endl;
      return;
    } 
    tlb->GetSingleFirmwareVersion(argHardware, answArg);
    if (hardware != answArg){
      cout << "Get Firmware Version failed in attempt: " << i << endl;
      return;
    }
    tlb->GetSingleFirmwareVersion(argProductID, answArg);
    if (product != answArg){
      cout << "Get Firmware Version failed in attempt: " << i << endl;
      return;
    }
  } 


  cout <<  "----------Testing ReadBoardID------------" << endl;
  uint16_t boardID = 0;
  if (!tlb->SendAndRetrieve(GPIOCmdID::GET_BOARD_ID, 0, &answArg)){ 
    cout << "Read BoardID failed" << endl;
    return;
  }
  
  std::cout << "GPIO board ID = 0x"<<std::hex<<boardID<<std::endl;
  boardID = answArg;

  for( int i = 0; i <= NumberOfAttempts; i++){
    tlb->SendAndRetrieve(GPIOCmdID::GET_BOARD_ID, 0, &answArg);
    if (boardID != answArg){
      cout << "Read BoardID failed in attempt: " << i << endl;
      return;
    }
  }   


  cout <<  "\n----------Testing User Set/Get configuration------------" << endl;
  for(int i = 0; i < NumberOfAttempts; i++){ 
    tlb->ConfigureTLB(TLBConfigJSON);
    if (!(tlb->VerifyConfiguration())) {
      cout << "ERROR: Configuration failed. Verification unsuccessful. Stopping testing." << endl;
      return;
    }
  }
  
  cout << "\n----------Testing Set/Get LUT------------" << endl;
  json json_LUTConfig = TLBConfigJSON["LUTConfig"];
  if (json_LUTConfig == nullptr) {
     std::cout << "ERROR: LUT configuration invalid. FIX ME!"<<std::endl;
     return;
  }

  try {
    tlb->ConfigureLUT(json_LUTConfig);
  } catch (TLBAccessException &e) {
      cout << e.what() << endl;
      return;
  }

  cout << "\nIf no errors appeared, test was successful." << endl; 
  delete tlb;
}

void usage(){
    cout << "\nUsage: ethernetTestApp -b <tlb_ip> -s <server_ip> -d <daq_ip>" << endl;
    cout << "   -b     Board ID of the TLB (this is in range 0-15)" << endl;
    cout << "   -s     Server IP addres in format X.X.X.X" << endl;
    cout << "   -d     DAQ PC IP address in format X.X.X.X" << endl;
    cout << "\nNote: values of parameters -s and -d will be probably always the same." << endl;
}

int main(int argc, char **argv)
{
  char choice;
  std::string l_SCIP, l_DAQIP, cfg_str;
  int opt, n, l_BoardID;
  bool s_param, d_param, b_param;

  s_param = false;
  d_param = false;
  b_param = false;

  if(argc<2){
    usage();
    return 0;
  }
  
  while ( (opt = getopt(argc, argv, "s:d:b:")) != -1 ) {
    switch ( opt ) {
    case 's':
      l_SCIP = optarg;
      s_param = true;  
      break;
    case 'd':
      l_DAQIP = optarg;
      d_param = true;
      break;
    case 'b':
      l_BoardID = atoi(optarg);   
      b_param = true;
      if (l_BoardID < 0 || l_BoardID > 15){
        cout << "ERROR: Board ID out of range." << endl;
        usage();
        return 0;
      } 
      break;
    case ':':
      std::cout<<"ERROR: Missing value for some of parameters : "<<optopt<<std::endl;
      usage();
      return 0;
    case '?':  // unknown option...
      cout << "\nERROR: Unknown parameter. For allowed parameters see hint bellow." << endl;
      usage();
      return 0;
    }
  }

  if ((b_param || d_param || s_param) == false){
    cout << "ERROR: All three arguments have to be provided." << endl;
    usage();
    return 0;
  }

  cout << "\nUsing Server/client PC IP:    " << l_SCIP
       << "\n      DAQ PC IP:              " << l_DAQIP
       << "\n      Board ID:               " << l_BoardID << endl;
 
  choice = '0';
  std::cout << "\nSelect one of following options:" << std::endl;
  std::cout << "   0: Exit" <<
               "\n   1: Test sockets" <<
               "\n   2: Test connection" <<
               "\n   3: Test configuration" << 
               "\n   4: Test DAQ" <<
               "\n   5: Test commands" << endl;
  std::cin >> choice;
  
  switch(choice){
    case '0':
      return 0;
      break;
    
    case '1':
      TestSockets(l_SCIP, l_DAQIP, l_BoardID);
      break;

    case '2':
      TestConnection(l_SCIP, l_DAQIP, l_BoardID);
      break;

    case '3':
      std::cout << "Enter TLB configuration file: " << std::endl;
      std::cin >> cfg_str;
      TestConfiguration(l_SCIP, l_DAQIP, l_BoardID, cfg_str);
      break;

    case '4':
      std::cout << "Enter TLB configuration file: " << std::endl;
      std::cin >> cfg_str;
      std::cout << "Enter duration of DAQ test in seconds:" << std::endl;
      std::cin >> n;
      TestDataTaking(l_SCIP, l_DAQIP, n, l_BoardID, cfg_str);
      break;

    case '5':
      std::cout << "Enter TLB configuration file: " << std::endl;
      std::cin >> cfg_str;
      std::cout << "Enter number of repetitions for each test" << std::endl;
      std::cin >> n;
      TestFunctions(l_SCIP, l_DAQIP, n, l_BoardID, cfg_str);
      break;

    default:
      std::cout << "Invalid option" << std::endl;
  }
}
