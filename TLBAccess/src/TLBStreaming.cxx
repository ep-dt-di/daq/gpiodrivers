/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#include "TLBAccess/TLBStreaming.h"
#include <iostream>
#include <sstream>      // std::stringstream
#include <fstream>
//#include <iomanip>
#include <bitset>
#include <string>
#include "Logging.hpp"

#define TLBMONITORING_FRAGMENT_SIZE 26
#define TLBDATA_FRAGMENT_SIZE 5

using namespace FASER;

void TLBStreaming::SetupStorageDecodedStream()
{
  m_storageSetup = false;

  if (m_dataOutStreamTrigger.is_open()){
  m_dataOutStreamTrigger.close();
  }
  if (m_dataOutStreamMonitoring.is_open()){
  m_dataOutStreamMonitoring.close();
  }
  
  m_dataOutStreamTrigger.open ("TriggerData.JSON", std::ios::trunc);
  m_dataOutStreamMonitoring.open ("MonitoringData.JSON", std::ios::trunc);
  
  if (!m_dataOutStreamTrigger.is_open()){
    ERROR("could not open file "<<"TriggerData.JSON"<<" no data will be written to disk!");
  }
  if (!m_dataOutStreamMonitoring.is_open()){
    ERROR("could not open file "<<"MonitoringData.JSON"<<" no data will be written to disk!");
  }
  else m_storageSetup = true;
} 

void TLBStreaming::CloseStorageDecodedStream()
{
  m_dataOutStreamTrigger.close();
  m_dataOutStreamMonitoring.close(); 
}

void TLBStreaming::PrintRawDataVector(const std::vector<uint32_t>& Vector)
{
  for (int i=0; i<Vector.size(); i++){
    std::cout<<"Vector["<<i<<"]: "<<std::bitset<32>(Vector[i])<<std::endl;
  }
}

void TLBStreaming::DecodeTriggerDataVector(const std::vector<uint32_t>& TriggerVector, bool to_file=false)
{
  if ( TriggerVector.size() != TLBDATA_FRAGMENT_SIZE) {
    WARNING("Physics event is not of correct size and may be corrupted. Skipping event.");
    return;
  }
  if (to_file && !m_storageSetup) SetupStorageDecodedStream();

  std::stringstream os;
  os<<"{\"EventCounter\": "<<TriggerVector[1]<<","<<std::endl;
  os<<"\"OrbitCounter\": "<<TriggerVector[2]<<","<<std::endl;
  os<<"\"BunchCounter\": "<<TriggerVector[3]<<","<<std::endl;
  os<<"\"TAP\": "<<std::bitset<6>((TriggerVector[4]&MASK_TAP_DATA)>>RSHIFT_TAP_DATA)<<","<<std::endl;
  os<<"\"TBP\": "<<std::bitset<6>((TriggerVector[4]&MASK_TBP_DATA)>>RSHIFT_TBP_DATA)<<","<<std::endl;
  os<<"\"InputBits\": "<<std::bitset<8>((TriggerVector[4]&MASK_INPUT_BITS_DATA)>>RSHIFT_INPUT_BITS_DATA)<<","<<std::endl;
  os<<"\"InputBitsNextCLK\": "<<std::bitset<8>(TriggerVector[4]&MASK_INPUT_BITS_NEXT_CLK_DATA)<<"}\n"<<std::endl;

  if (to_file) m_dataOutStreamTrigger<<os.rdbuf();
  else std::cout<<os.rdbuf();

}

void TLBStreaming::DecodeMonitoringDataVector(const std::vector<uint32_t>& MonitoringVector, bool to_file=false)
{  
  if ( MonitoringVector.size() != TLBMONITORING_FRAGMENT_SIZE) {
    WARNING("Monitoring event is not of correct size and may be corrupted. Skipping event.");
    return;
  }
  if (to_file && !m_storageSetup) SetupStorageDecodedStream();
 
  std::stringstream os;
  os<<"{\"EventCounter\": "<<MonitoringVector[1]<<","<<std::endl;
  os<<"\"OrbitCounter\": "<<MonitoringVector[2]<<","<<std::endl;
  os<<"\"BunchCounter\": "<<MonitoringVector[3]<<","<<std::endl;

  for (size_t i=0; i<6; i++){
    os<<"\"TBP"<<i<<"\": "<<MonitoringVector[4+i]<<","<<std::endl;
  }
  for (size_t i=0; i<6; i++){
    os<<"\"TAP"<<i<<"\": "<<MonitoringVector[10+i]<<","<<std::endl;
  }  
  for (size_t i=0; i<6; i++){
    os<<"\"TAV"<<i<<"\": "<<MonitoringVector[16+i]<<","<<std::endl;
  }    
  
  os<<"\"DeadtimeVetoCounter\": "<<MonitoringVector[22]<<","<<std::endl;
  os<<"\"BusyVetoCounter\": "<<MonitoringVector[23]<<","<<std::endl;
  os<<"\"RateLimiterVetoCounter\": "<<MonitoringVector[24]<<","<<std::endl;
  os<<"\"BCRVetoCounter\": "<<MonitoringVector[25]<<"}\n"<<std::endl;

  if (to_file) m_dataOutStreamMonitoring<<os.rdbuf();
  else std::cout<<os.rdbuf();

}

void TLBStreaming::RawDataCheckIncompleteEvents()
{
  int TriggerCounter=0;
  int MonitoringCounter=0;
  bool WrongTriggerSize=false;
  bool WrongMonitoringSize=false;
  std::fstream newfile;
   newfile.open("DataReadout.txt",std::ios::in); //open a file to perform read operation using file object
   if (newfile.is_open()){   //checking whether the file is open
      std::string tp;
      while(getline(newfile, tp)){ //read data from file object and put it into string.
 
         if ((std::bitset<32>(tp)==0xFEAD000A || std::bitset<32>(tp)==0xFEAD0050) && TriggerCounter>=1){
          if (TriggerCounter!=6){
            std::cout<<"Wrong Trigger Size, the header was: "<<std::hex<<std::bitset<32>(tp)<<" and I was filling a trigger event"<<std::endl;
          }
          TriggerCounter=0;
         }
         if ((std::bitset<32>(tp)==0xFEAD000A || std::bitset<32>(tp)==0xFEAD0050) && MonitoringCounter>=1){
          if (MonitoringCounter!=27){
            std::cout<<"Wrong Monitoring Size, the header was: "<<std::hex<<std::bitset<32>(tp)<<" and I was filling a monitoring event"<<std::endl;
          }
          MonitoringCounter=0;
         }
         
        if (std::bitset<32>(tp)==0xFEAD000A){
            TriggerCounter=1;

        }
        if (std::bitset<32>(tp)==0xFEAD0050){
          MonitoringCounter=1;
        } 
         
          if (TriggerCounter!=0){TriggerCounter=TriggerCounter+1;}
          if (MonitoringCounter!=0){MonitoringCounter=MonitoringCounter+1;}
          if (TriggerCounter==6){TriggerCounter=0;}
          if (MonitoringCounter==27){MonitoringCounter=0;}
      }
      newfile.close(); //close the file object.
   }
}
