/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#ifndef __CRC
#define __CRC

#include <iostream>
#include <vector>
#include <stdexcept>

//TODO Check if Doxygen documentation is sufficient

namespace FASER
{
/** *******************************************************
 \brief Class used for CRC computation
 ******************************************************** */
  class UFE_CRC
  {
  // CRC data structure
  public:
    struct CRCstruct
    {

      uint32_t m_polynomial;
      uint8_t m_size;
      uint32_t m_initRemainder;
      uint32_t m_finalXor;
      bool m_reflectDin;
      bool m_reflectCRC;
 
      CRCstruct(){}; //TODO Is it correct to add this in order to be able to compile UFE_CRC(CRCstruct x_crcStruct)?
      CRCstruct(uint32_t x_polynomial, uint8_t x_size, uint32_t x_initRemainder, uint32_t x_finalXor,
                 bool x_reflectDin, bool x_reflectCRC)
      {

        m_polynomial = x_polynomial;
        m_size = x_size;
        m_initRemainder = x_initRemainder;
        m_finalXor = x_finalXor;
        m_reflectDin = x_reflectDin;
        m_reflectCRC = x_reflectCRC;
      }
    };

    // CONSTRUCTOR / DESTRUCTOR
    UFE_CRC(CRCstruct x_crcStruct): m_crcStruct() 
    { 
 
      if (x_crcStruct.m_size > 32) 
        throw std::overflow_error("CRC size must be lower or equal to 32"); 
      else if (x_crcStruct.m_size < 8) 
        throw std::overflow_error("CRC size must be greater or equal to 8"); 
       
      m_crcStruct = x_crcStruct; 
       
      m_mask = (x_crcStruct.m_size == 32) ? 0xFFFFFFFF : (uint32_t)((1 << x_crcStruct.m_size) - 1); 
 
      buildTable(); 
    }
    ~UFE_CRC();
 
    uint32_t crc(std::vector<uint8_t> x_message);
    std::vector<uint8_t> convert16to8bit(std::vector<uint16_t> x_message);

  private:
    CRCstruct m_crcStruct; // CRC structure
    uint32_t m_table[256]; // CRC pre-computed table
    uint32_t m_mask; //CRC mask from CRC size
    uint32_t reflect(uint32_t x_val, uint8_t x_nbBits);
    void buildTable();
  };

 //This CRC is used in TLB
/** *******************************************************
 \brief 16-bits CRC optimized for HD4 @ 32751-bits = 2046 word16 
 (1A2EB = 0xD175 on https://users.ece.cmu.edu/~koopman/crc/index.html)
 ******************************************************** */
  class CRC_16_1A2EB
  {
  public:
    static UFE_CRC::CRCstruct CRC;
  };

/** *******************************************************
 \brief Standard 16-bits CRC
 ******************************************************** */
  class CRC_16_18005
  {
  public:
    static UFE_CRC::CRCstruct CRC;
    static std::vector<uint8_t> testMessage;
    static uint16_t checkedValue;
  };

 //Class to check correctness of result of CRC computation
/** *******************************************************
 \brief Standard CCITT 16-bits CRC (11021 = 0x8810 on 
 https://users.ece.cmu.edu/~koopman/crc/index.html)
 ******************************************************** */
  class CRC_CCITT_11021
  {
  public:
    static UFE_CRC::CRCstruct CRC;
    static std::vector<uint8_t> testMessage;
    static uint16_t checkedValue;
  };
}

#endif /* __CRC */
