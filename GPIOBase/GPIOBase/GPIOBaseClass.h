/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#ifndef __GPIOBaseClass
#define __GPIOBaseClass

#include <stdint.h>
#include <vector>
#include <chrono>
#include "CommunicationInterface.h"
#include "Exceptions/Exceptions.hpp"

 #define MIN_COMMUNICATION_DELAY 200 // mu sec

class GPIOBaseException : public Exceptions::BaseException { using Exceptions::BaseException::BaseException; };

namespace FASER {
  
 
  /** *******************************************************
   * \brief Class containing basic functionality to access a GPIO board
   * Detector specific software should inherit from this base class. Once
   * the firmware specs for trigger and tracker are settled the content should be revisited.
   ******************************************************** */
  class GPIOBaseClass
  {
  public:
    GPIOBaseClass(std::string SCIP, std::string DAQIP, bool emulateInterface, int boardID = -1);
    GPIOBaseClass(bool emulateInterface, int boardID = -1);
    virtual ~GPIOBaseClass() { delete m_interface;}    
    bool GetFirmwareVersions();
    bool ReadStatus(uint16_t &status);
    
    /** \brief Reads the hardware board ID from the GPIo board. (set via the hardware switch on the GPIO board)
     The board ID is used in all further communication and should be read first thing when connecting to the board.
     */
    bool ReadBoardID();
    
    /** \brief Returns the hardware board ID (set via the hardware switch on the GPIO board)
     */
    uint8_t GetBoardID() {return m_boardID;}
    
    void SetDebug(int v);
    
    void SetBoardID(uint8_t v){m_boardID = v;}
    
    /** Send a command with a single argument and retrieve a single result word.
     Additional consistency checks are performed.
     */
    bool SendAndRetrieve(uint8_t CmdID, uint16_t CmdArg, uint16_t *AnswArg = nullptr);
    /** Send a command with a single argument and retrieve vector of reply words. Relevant only for GET_CONFIG.
     Additional consistency checks are performed.
     */
    bool SendAndRetrieveMultiarg(uint8_t CmdID, uint16_t CmdArg, std::vector<uint16_t> *AnswArg = nullptr, uint16_t *CRC = nullptr); 
    /** Send a command with a vector of arguments and retrieve a single result word. Relevant only for SET_CONFIG.
     Additional consistency checks are performed.
     */
    bool SendMultiargAndRetrieve(uint8_t CmdID, uint8_t SumCmdID, std::vector<uint16_t> CmdArg, uint16_t *AnswArg = nullptr);

    /** \brief Retrieves the version of a given firmware / hardware block
     */
    bool GetSingleFirmwareVersion(uint16_t whichFirmware,  uint16_t &answArg);

    void block(unsigned long microseconds);
    
    
  private:
    // Lower level access to readout board
    /** SendCommand: generates the CommandTrailerword if needed (CRC16 checksum) and
     sends the bitsream for the command to the GPIO board via USB or Ethernet.
     */
    bool SendCommand(uint32_t CommandRequestHeader, std::vector<uint32_t> CommandRequestArguments, uint32_t CommandRequestTrailer);
    bool SendCommand(uint32_t CommandRequestHeader);
    bool SendCommand(uint32_t, uint32_t){ return true;} //FIXME what is this used for?
    
    /** GetReply retrieves the reply from the GPIO board.
     Has to be reimplemented for Ethernet / USB
     */
    bool GetCommandAnswer(uint32_t &CommandAnswerHeader, std::vector<uint32_t> &CommandAnswerArguments);
    /** created a 32bit CommandRequestHeader using the CmdID, SubCmdID, number of arguments and optionally a boardID.
    */
    uint32_t MakeMultiargCommandRequestHeader(uint8_t CmdID, uint8_t SubCmdID, uint16_t ArgNum, uint8_t BoardID = 0);
    /** created a 32bit CommandRequestHeader using the CmdID and optionally an argument and a boardID.
     */
    uint32_t MakeCommandRequestHeader(uint8_t CmdID, uint16_t arg = 0, uint8_t BoardID = 0);
    /** created a 32bit multiple argument CommandRequest using the 16bit payload.
     */
    std::vector<uint32_t> MakeMultiargCommandRequestArgument(std::vector<uint16_t> arg);
    /** created a 32bit CommandRequestArgument using the frameIndex 16bit payload.
     */
    uint32_t MakeCommandRequestArgument(uint8_t CmdID, uint16_t arg, uint8_t BoardID = 0);
    /** created a 32bit CommandRequestArgument using the frameIndex 16bit payload.
     */
    uint32_t MakeCommandRequestTrailer(uint8_t CmdID, uint16_t crc16, uint8_t BoardID = 0);
    
    /** DecodeAnswerArgument: checks the boad response for errors and decodes the argument and trailer of reply (relevant for multiple AnswArg).
     */
    bool DecodeCommandAnswerArgument(std::vector<uint32_t> CmdAnswerArgument, std::vector<uint16_t> *arg, uint16_t *CRC = nullptr); 
    /** DecodeAnswerHeader: checks the boad response for errors and decodes the header of reply.
     */
    bool DecodeCommandAnswerHeader(uint32_t CmdAnswerHeader, uint32_t &CmdID, uint16_t *arg, uint32_t &boardID);
    bool CheckErrors(uint32_t CmdAnswerHeader);
    
    uint8_t m_boardID; /// This is the ID of the board set by the hardware switch!

  public:
    bool m_DEBUG;
    CommunicationInterface *m_interface; // hw interface to trb
    int m_FPGA_version;
  };
}

/** *******************************************************
 * \brief Definitions of CommandRequest IDs.
 ******************************************************** */
class GPIOCmdID {
public:
  static const uint8_t DATA_READOUT        = 0x00;
  static const uint8_t FIRMWARE_VERSION    = 0x01;
  static const uint8_t SET_DIRECT_PARAM    = 0x02;
  static const uint8_t READ_STATUS         = 0x03;
  static const uint8_t SET_CONFIG          = 0x04;
  static const uint8_t GET_CONFIG          = 0x05;
  static const uint8_t APPLY_CONFIG        = 0x06;
  static const uint8_t GET_BOARD_ID        = 0x12;
  static const uint8_t ERROR               = 0x1E;
  static const uint8_t IDLE                = 0x1F;
};

/** *******************************************************
 \brief Definitions of ERROR codes retrieved from GPIO board.
 ******************************************************** */
class GPIOErrors {
public:
  // ERROR words from DAQ flow
  static const uint16_t DAQ_L2TIMEOUT_MISSINGHEADER = 0x0;
  static const uint16_t DAQ_L1FIFO = 0x1;
  static const uint16_t DAQ_L2TIMEOUT_MISSINGTRAILER = 0x2;
  static const uint16_t DAQ_L2FIFO = 0x8;
  
  static const uint16_t TIMEOUT     = 0x0000; // Timeout in beta fw versions 1.B-633-USB/101.B-633-ETH
  static const uint16_t W_WID     = 0x0001; // Wrong word ID
  static const uint16_t W_CID     = 0x0002; // Wrong command ID
  static const uint16_t W_SCID    = 0x0004; // Wrong sub-command ID
  static const uint16_t W_FIDX    = 0x0008; // Wrong frame index during argument frame
  static const uint16_t W_VAL     = 0x2000; // not validaded config for at least 1 of teh selected modules OR wrong valid word during validate config
  static const uint16_t DEV_MASK  = 0x0ff0; // dev error from device 0-7
  static const uint16_t CRC       = 0x4000; // CRC error
  static const uint16_t DEV       = 0x4000; // DEV number error during get_config, same bit as CRC error
};

/** *******************************************************
 \brief Definitions of WORD ID (bit 31-28 of command request header)
 ******************************************************** */
class GPIOWordID {
public:
  static const uint8_t CmdReqHeadID   = 8;
  static const uint8_t CmdReqArgID    = 9;
  static const uint8_t CmdReqTrailID  = 10;
  static const uint8_t CmdAnsHeadID   = 8;
  static const uint8_t CmdAnsArgID    = 9;
  static const uint8_t CmdAnsTrailID  = 10;
};


#endif // __GPIOBaseClass
