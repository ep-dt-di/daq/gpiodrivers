/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#ifndef __usbInterface
#define __usbInterface


#include "libusb.h"
#include <string>
#include <iostream>
#include <vector>
#include <stdexcept>
#include "CommunicationInterface.h"

/** *******************************************************
 * \brief This is the Interface communicates to the tracker readout board via USB
 * To open a USB connection first call ConnectToGPIO. If successfull Write/Read
 * operations are possible. The connection is closed when in the destructor for now.
 * TODO: Error handling needs an overhaul: close any open / initialised connections /
 *  objects in case of an error, automatic retry, throw a proper error object, catch
 *  catchable errors and retry.
******************************************************** */

namespace FASER{
  class usbInterface : public CommunicationInterface {
  public:
    usbInterface();
    ~usbInterface();
  
    void SetDebug(int v) override { if (v > 0){ m_DEBUG = true;} else {m_DEBUG = false;}}
    void SetEmulate(bool v) override { if (m_interfaceOK && !v) {m_interfaceOK = false;} m_EMULATE = v; }
    void Connect() override;
    
    void SendSerial(unsigned char *data, int length, int &bytesTransferred) override;
    std::vector<unsigned char> ReadSerial(int &bytesTransferred) override;
    std::vector<unsigned char> ReadData(int &bytesTransferred) override;
    // void ReadData();
    
    void ListDevices();
    
  public: // helpers for handling 32bit data words
    void ClearSendBuffer() override {m_dataOutBuffer.resize(0);}
    void QueueData(uint32_t data) override;
    int SendBuffer() override;
    std::vector<uint32_t> ReadSerial32(int &bytesTransferred) override;
    std::vector<uint32_t> ReadData32(int &bytesTransferred) override;
  
  private:
    void claimInterface(int interfaceNumber);
    
    libusb_context        *m_ctx; // a libusb session
    libusb_device         **m_usbDevices; // list of attached devices
    libusb_device_handle  *m_dev_handle; // usb device handle
    bool m_interfaceOK;
    
  private:
    
    /// Definition of Endpoints
    const uint16_t VENDOR_ID = 0x206b;
    const uint16_t PRODUCT_ID = 0x0c00;
    const unsigned char EP_SerialWrite  = 0x2;
    const unsigned char EP_SerialRead   = 0x82; // default: 82
    const unsigned char EP_DataRead     = 0x81;
    
    /// Steering options
    bool m_DEBUG;
    bool m_EMULATE;
    
    /// Receiving buffer
    std::vector<unsigned char> m_serialBuffer; // for control data
    std::vector<unsigned char> m_dataBuffer; // for daq data
    std::vector<unsigned char> m_dataOutBuffer; // buffer for output data
    unsigned int m_USBDataBufferSize;
    static const int MAX_READ_RETRIES = 1;
  };
}

#endif /* __usbInterface */
