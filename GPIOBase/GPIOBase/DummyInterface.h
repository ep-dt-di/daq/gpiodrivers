/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#ifndef __dummyInterface
#define __dummyInterface


#include "libusb.h"
#include <vector>
#include <fstream>
#include "CommunicationInterface.h"

/** *******************************************************
 * \brief This is the Interface emulates a TRB. Data will be read from file and
 * streamed to the eradout application.
******************************************************** */

namespace FASER{
  class dummyInterface : public CommunicationInterface {
  public:
    dummyInterface();
    ~dummyInterface();
  
    void SetDebug(int v) override { if (v > 0){ m_DEBUG = true;} else {m_DEBUG = false;}}
    void Connect() override { return; } // do nothing
    void SendSerial(unsigned char *data, int length, int &bytesTransferred) override;
    std::vector<unsigned char> ReadSerial(int &bytesTransferred) override;
    std::vector<unsigned char> ReadData(int &bytesTransferred) override;
    void SetEmulate(bool) override {return;}
    
    // FileChange
    bool SetInputFile(std::string filename);
    
  public: // helpers for handling 32bit data words
    void ClearSendBuffer() override {m_dataOutBuffer.resize(0);}
    void QueueData(uint32_t data) override;
    int SendBuffer() override;
    std::vector<uint32_t> ReadSerial32(int &bytesTransferred) override;
    std::vector<uint32_t> ReadData32(int &bytesTransferred) override;
  
  private:
    std::string m_inputFileName;
    std::ifstream m_inputFileStream;
    
  private:
    /// Steering options
    bool m_DEBUG;
    bool m_EMULATE;
    
    /// Receiving buffer
    std::vector<unsigned char> m_serialBuffer; // for control data
    std::vector<char> m_dataBuffer; // for daq data
    std::vector<unsigned char> m_dataOutBuffer; // buffer for output data
    unsigned int m_USBDataBufferSize;
    static const int MAX_READ_RETRIES = 1;
  };
}

#endif /* __usbInterface */
