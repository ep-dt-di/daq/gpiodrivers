project(GPIOBase)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}) #only needed to be able to include headers in usbInterface - but why? 
if (APPLE)
   SET(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -Wno-c++11-extensions")
	include_directories(/usr/local/include/libusb-1.0/)
	link_directories(/usr/local/lib/)
elseif(UNIX)
	include_directories(/usr/include/libusb-1.0/)
	link_directories(/usr/lib64/)
endif()

add_library(GPIOBase src/GPIOBaseClass.cxx src/CRC.cxx)
add_library(usbInterface src/usbInterface.cxx src/DummyInterface.cxx)
add_library(udpInterface src/udpInterface.cxx)

target_include_directories(GPIOBase PUBLIC
  ${CMAKE_CURRENT_SOURCE_DIR}
)

target_link_libraries(udpInterface Logging)
target_link_libraries(usbInterface Logging)
target_link_libraries(GPIOBase usbInterface udpInterface EventFormats Exceptions)


