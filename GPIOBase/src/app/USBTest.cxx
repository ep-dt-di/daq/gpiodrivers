/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#include "../../GPIOBase/usbInterface.h"

#include <iostream>

using namespace FASER;


int main()
{
  // Testing USB access
  usbInterface *iUSB = new usbInterface();
  
  iUSB->ListDevices();
  
  /*
  std::vector<unsigned char> readBytes;
  int bytesTransferred;
  
  iUSB->Connect();
  for (unsigned int i = 0; i < 10; i++){
    readBytes =  iUSB->ReadSerial(bytesTransferred);
    std::cout << "bytes read = "<<bytesTransferred<<std::endl;
  }
  */
  delete iUSB;
  
  return 0;
  
  
}
