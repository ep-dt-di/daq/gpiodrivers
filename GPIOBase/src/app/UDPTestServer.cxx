/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#define PORT 8080
#define MAXLINE 1024
int main()
{
  
  // open a socket and listen to localhost port 8999
  int sockfd;
  char buffer[MAXLINE];
  struct sockaddr_in servaddr, cliaddr;
  
  // Creating socket file descriptor
  if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
    perror("socket creation failed");
    exit(EXIT_FAILURE);
  }
  
  memset(&servaddr, 0, sizeof(servaddr));
  memset(&cliaddr, 0, sizeof(cliaddr));
  
  // Filling server information
  servaddr.sin_family    = AF_INET; // IPv4
  servaddr.sin_addr.s_addr = INADDR_ANY;
  servaddr.sin_port = htons(PORT);
  
  // Bind the socket with the server address
  if ( bind(sockfd, (const struct sockaddr *)&servaddr,
            sizeof(servaddr)) < 0 )
  {
    perror("bind failed");
    exit(EXIT_FAILURE);
  }
  
  unsigned int len, n;
  
  while (1){
  
    
    len = sizeof(cliaddr);  //len is value/resuslt
  
    std::cout << "Received: ";
    n = recvfrom(sockfd, (char *)buffer, MAXLINE, MSG_WAITALL, ( struct sockaddr *) &cliaddr, &len);
    
    for (unsigned int i = 0; i < n; i++) {std::cout << std::hex<<(((unsigned int)buffer[i]) & 0xff) << " "; }
    std::cout << std::endl;
    
    std::cout << "Sending: ";
    for (unsigned int i = 0; i < n; i++) {std::cout << std::hex<<(((unsigned int)buffer[i]) & 0xff) << " "; }
    std::cout << std::endl;
    
    sendto(sockfd, (const char *)buffer, n, 0, (const struct sockaddr *) &cliaddr, len);
    std::cout << std::endl;
  
    
    /*
    std::vector<char> sendBuffer = {0,0,0,0};

    //sleep(1);
    std::cout << "    Sending Message to Receiver: 0x";
    for (auto byte : sendBuffer) {std::cout << (int)byte << " "; }
    std::cout << std::endl;

    sendto(sockfd, (const char*) sendBuffer[0], sendBuffer.size(), 0, (const struct sockaddr *) &cliaddr, len);
  */
    
  }
  
  return 0;
}
