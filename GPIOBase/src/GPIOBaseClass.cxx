/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include "GPIOBase/GPIOBaseClass.h"
#include "GPIOBase/usbInterface.h"
#include "GPIOBase/udpInterface.h"
#include "GPIOBase/DummyInterface.h"
#include "GPIOBase/CRC.h"

using namespace FASER;

/** *******************************************************
 \brief Constructor: open hw connection to GPIO using udpInterface
 \param SCIP: Server Computer IP address in format X.X.X.X
 \param DAQIP: IP address of the DAQ PC (it will be probably same as SCIP, however, it is possible to have two different PCs)
 \param emulateInterface: this enables/disables emulated communication interface. It is possible to test software without actual GPIO board
 \param boardID: connect to the board with a given board ID. The board ID must be specified when communicating via ethernet.
 ******************************************************** */
GPIOBaseClass::GPIOBaseClass(std::string SCIP, std::string DAQIP, bool emulateInterface, int boardID) : m_DEBUG(false), m_boardID(0)
{
  if (emulateInterface) {
    m_interface = new dummyInterface();
    m_interface->SetDebug(0);
    m_interface->Connect();
  } else {
    INFO("GPIOBase: using udpInterface");
    std::stringstream ss;
    if (boardID == -1){
      ERROR("Board has to be specified when using UDP interface!");
      exit(1);
    }
    ss << "10.11.65."<<16+boardID;
    m_interface = new udpInterface(SCIP, DAQIP, ss.str());
  }

  m_interface->SetDebug(0);
  try {
    m_interface->Connect();
  } catch ( const std::exception &e) {
    ERROR(e.what());
    ERROR("Could not open connection to GPIO. Good luck next time.");
  }
  m_boardID = 0xff;
  ReadBoardID();
  GetFirmwareVersions();
}

/** *******************************************************
 \brief Constructor: open hw connection to GPIO using usbInterface
 \param emulateInterface: this enables/disables emulated communication interface. It is possible to test software without actual GPIO board.
 \param boardID: connect to the board with a given board ID. If boardID = -1: connects to the first board that is found
 ******************************************************** */
GPIOBaseClass::GPIOBaseClass(bool emulateInterface, int boardID) : m_DEBUG(false), m_boardID(0)
{
  std::vector<CommunicationInterface*> interfacesToDelete;
   m_interface = nullptr;

  if (emulateInterface) {
    m_interface = new dummyInterface();
    m_interface->SetDebug(0);
    m_interface->Connect();
    ReadBoardID();
    
  } else {
    do {
      if (m_interface != nullptr){
        interfacesToDelete.push_back(m_interface);
      }
      m_boardID = 0xff; // this supresses error messages at this stage
      m_interface = new usbInterface();
  
      m_interface->SetDebug(0);
      try {
        m_interface->Connect();
      } catch ( const std::exception &e) {
        ERROR(e.what());
        ERROR("Could not open connection to GPIO. Good luck next time");
      }
      ReadBoardID();
      DEBUG("-- found board "<<std::dec<< (int)m_boardID<<", requested board ID "<<std::dec<< boardID);
      } while ( (m_boardID != boardID) && (boardID != -1));
    }
    INFO("Connected to GPIO board with ID = "<<std::dec<< (int)m_boardID);
    GetFirmwareVersions();
    // delete unused interfaces:
    for (auto interface : interfacesToDelete){
      delete interface;
    }
}
     
/** *******************************************************
 \brief Read the board ID, set by the hardware switch on the GPIO board.
 ID is stored in m_boardID and used for all communication with the board. 
 ******************************************************** */
bool GPIOBaseClass::ReadBoardID()
{
  uint16_t boardID = 0;
  if (!SendAndRetrieve(GPIOCmdID::GET_BOARD_ID, 0, &boardID)){ return false;}
  
  m_boardID = boardID;
  return true;
}


/** *******************************************************
 \brief Read parameter on status port of the FPGA
 \param status: variable used to store result
 ******************************************************** */
bool GPIOBaseClass::ReadStatus(uint16_t &status)
{
  return SendAndRetrieve(GPIOCmdID::READ_STATUS, 0, &status);
}

/** *******************************************************
 Send a command with a single arguments and retrieve a vector of 16bit from the answer argument.
 Additional consistency checks are performed.
 \brief Relevant only for GET_CONFIG command.
 \param CmdID: command ID as described in GPIO board firmware documentation
 \param CmdArg: command argument
 \param AnswArg: pointer to vector where the answer should be returned (since this is only relevant for GET_CONFIG the aswer is content of LUT
 \param CRC: pointer to the variable where the CRC computed by the board should be returned
 ********************************************************* */
bool GPIOBaseClass::SendAndRetrieveMultiarg(uint8_t CmdID, uint16_t CmdArg, std::vector<uint16_t> *AnswArg, uint16_t *CRC)
{
  uint32_t CommandAnswerHeader = 0;
  std::vector<uint32_t> CommandAnswerArguments;
  uint32_t CmdAnswID = 0;
  uint32_t boardID = 0;
  //uint16_t CRC;
  

  if (!SendCommand(MakeCommandRequestHeader(CmdID, CmdArg, m_boardID))){
    ERROR("SendAndRetrieve() -> SendCommand() failed" );
    return false;
  }
  // usleep(10000); //This time delay may change for ethernet. 1000mus was too low for USB and caused transfer errors.
  block(10000); // One could try to decrease this timeout again when using block instead of usleep(), in case speed is an issue.
  if (!GetCommandAnswer(CommandAnswerHeader, CommandAnswerArguments)){
    ERROR("SendAndRetrieve() -> GetCommandAnswer() failed" );
    return false;
  }
  if (!DecodeCommandAnswerHeader(CommandAnswerHeader, CmdAnswID, nullptr, boardID)){
    ERROR("SendAndRetrieve() -> DecodeCommandAnswerHeader() failed" );
    return false;
  }

  DecodeCommandAnswerArgument(CommandAnswerArguments, AnswArg, CRC);

  if (CmdAnswID != CmdID){
    ERROR("Answer command ID = 0x"<<std::hex<<(int)CmdAnswID<< " does not match Request command ID = 0x"<<std::hex<< (int)CmdID);
    return false;
  }
  if (m_boardID != boardID) {
      ERROR("Answer board ID = 0x"<<std::hex<<(int)boardID<< " does not match the req. board ID = 0x"<<std::hex<< (int)m_boardID);
  }
  return true;
}

/** *******************************************************
 Send a command with a multiple arguments and retrieve a 16bit result from the answer header.
 Additional consistency checks are performed.
 \brief Relevant only for SET_CONFIG command
 \param CmdID: command ID as described in GPIO board firmware documentation
 \param SubCmdID: sub command ID as described in GPIO board firmware documentation
 \param CmdArg: command argument
 \param AnswArg: pointer to variable where the answer from the board should be returned
 ******************************************************** */
bool GPIOBaseClass::SendMultiargAndRetrieve(uint8_t CmdID, uint8_t SubCmdID, std::vector<uint16_t> CmdArg, uint16_t* AnswArg)
{
  uint32_t CommandAnswerHeader = 0;
  std::vector<uint32_t> CommandAnswerArguments;
  uint32_t CmdAnswID = 0;
  uint32_t boardID = 0;
  uint16_t ArgNum = CmdArg.size() + 2; // number of frames to be sent (+ 2 header and trailer)
  uint32_t commandHeader = MakeMultiargCommandRequestHeader(CmdID, SubCmdID, ArgNum, m_boardID);
  std::vector<uint32_t> commandArg = MakeMultiargCommandRequestArgument(CmdArg);

  CRC_16_1A2EB crc16_1A2EB;
  UFE_CRC CRC_Class = UFE_CRC(crc16_1A2EB.CRC);
  uint16_t crc16 = CRC_Class.crc(CRC_Class.convert16to8bit(CmdArg));  

  uint32_t commandTrailer = MakeCommandRequestTrailer(CmdID, crc16, m_boardID); 

  if (!SendCommand(commandHeader, commandArg, commandTrailer)){
    ERROR("SendCommand() -> SendCommand() failed");
    return false;
  }
  
  block(100);
  if (!GetCommandAnswer(CommandAnswerHeader, CommandAnswerArguments)){
    ERROR("SendAndRetrieve() -> GetCommandAnswer() failed" );
    return false;
  }
  if (!DecodeCommandAnswerHeader(CommandAnswerHeader, CmdAnswID, AnswArg, boardID)){
    ERROR("SendAndRetrieve() -> DecodeCommandAnswerHeader() failed" );
    return false;
  }
  if (CmdAnswID != CmdID){
    ERROR("Answer command ID = 0x"<<std::hex<<(int)CmdAnswID<< " does not match Requets command ID = 0x"<<std::hex<< (int)CmdID);
    return false;
  }
  if (m_boardID != boardID) {
    ERROR("Answer board ID = 0x"<<std::hex<<(int)boardID<< " does not match the req. board ID = 0x"<<std::hex<< (int)m_boardID);
  }
  return true;
}

/** *******************************************************
 Send a command with a single argument and retrieve a single 16bit result from the answer header.
 Additional consistency checks are performed.
 \brief Send command and retrieve answer
 \param CmdID: command ID as described in GPIO board firmware documentation
 \param CmdArg: command argument
 \param AnswArg: pointer to variable where the answer from the board should be returned
 ******************************************************** */
bool GPIOBaseClass::SendAndRetrieve(uint8_t CmdID, uint16_t CmdArg, uint16_t* AnswArg)
{
  uint32_t CommandAnswerHeader = 0;
  std::vector<uint32_t> CommandAnswerArguments;
  uint32_t CmdAnswID = 0;
  uint32_t boardID = 0;
  
  //usleep(MIN_COMMUNICATION_DELAY); // make sure we respect the min. distance between 2 consectuive writes to the TRB
  if (!SendCommand(MakeCommandRequestHeader(CmdID, CmdArg, m_boardID))){
    ERROR("SendAndRetrieve() -> SendCommand() failed" );
    return false;
  }
  block(100);
  if (!GetCommandAnswer(CommandAnswerHeader, CommandAnswerArguments)){
    ERROR("SendAndRetrieve() -> GetCommandAnswer() failed" );
    return false;
  }
  if (!DecodeCommandAnswerHeader(CommandAnswerHeader, CmdAnswID, AnswArg, boardID)){
    ERROR("SendAndRetrieve() -> DecodeCommandAnswerHeader() failed" );
    return false;
  }
  if (CmdAnswID != CmdID){
    ERROR("Answer command ID = 0x"<<std::hex<<(int)CmdAnswID<< " does not match Requets command ID = 0x"<<std::hex<< (int)CmdID);
    return false;
  }
  if (m_boardID != boardID && m_boardID != 0xff) {
    ERROR("Answer board ID = 0x"<<std::hex<<(int)boardID<< " does not match the req. board ID = 0x"<<std::hex<< (int)m_boardID);
  }
  return true;
}


/** *******************************************************
 \brief Retrieves the answer from the FPGA.
 Error bits are checked and errors thrown accordingly !!TODO!!
 \param CommandAnswerHeader: command answer header is returned here
 \param CommandAnswerArguments: command answer arguments are returned here
 ******************************************************** */
bool GPIOBaseClass::GetCommandAnswer(uint32_t &CommandAnswerHeader, std::vector<uint32_t> &CommandAnswerArguments)
{
  std::vector<uint32_t> tmp;
  int bytesRead = 0;
  try {
    tmp = m_interface->ReadSerial32(bytesRead);
  } catch (...){
    ERROR("Read Serial threw an error. Continuing anyways");
  }
  if (bytesRead == 0 || tmp.size() == 0){
    ERROR("Did not get any data back from device ...");
    return false;
  }
  
  CommandAnswerHeader = tmp[0]; // Contains board id, CMD id, arguments (if any) or error words.
  tmp.erase(tmp.begin()); // remove first element
  CommandAnswerArguments.clear(); // we're not sending any additional arguments
  CommandAnswerArguments = tmp;
 
  return true;
}

/** *******************************************************
 \brief Wrapper calling DecodeCommandAnswerHeader() without additional arguments.
 Error decoding happens there.
 \param CommandAnswerHeader: command answer header to be checked for errors
 ******************************************************** */
bool GPIOBaseClass::CheckErrors(uint32_t CommandAnswerHeader)
{
  uint32_t CmdID, boardID;
  
  return DecodeCommandAnswerHeader(CommandAnswerHeader, CmdID, nullptr, boardID);
}

/** *******************************************************
  \brief The 32bit reply word from the FPGA is decoded into the vector of 16bit return answer argument
 (i.e. everything what follows after header answer frame)
 \param CmdAnswerArgument: board answer that should be decoded
 \param *arg: decoded answer argument is returned here
 \param *CRC: decoded CRC checksum is returned here
 ******************************************************** */
bool GPIOBaseClass::DecodeCommandAnswerArgument(std::vector<uint32_t> CmdAnswerArgument, std::vector<uint16_t> *arg, uint16_t *CRC)
{
  if (arg != nullptr){ 
      for (int i = 0; i < CmdAnswerArgument.size(); i++){
        if (((CmdAnswerArgument[i] >> 28) & 0xf) == GPIOWordID::CmdAnsArgID){
          arg->push_back(CmdAnswerArgument[i] & 0x0000ffff); // bits 0 - 15
        }
        if (((CmdAnswerArgument[i] >> 28) & 0xf) == GPIOWordID::CmdAnsTrailID && CRC != nullptr){
          (*CRC) = (CmdAnswerArgument[i] & 0x0000ffff); // bits 0 - 15
        }
      } 
    }
  return true;
}

/** *******************************************************
 \brief The 32bit reply word from the FPGA is decoded into the return argument, CommandAnswerID,
 BoardID and the currently unused command answer header ID
 \param CmdAnswerHeader: header to be decoded
 \param &CmdID: decoded command ID is returned here 
 \param *arg: decoded argument is returned here 
 \param &boardID: decoded board ID is returned here
 ******************************************************** */
bool GPIOBaseClass::DecodeCommandAnswerHeader(uint32_t CmdAnswerHeader, uint32_t &CmdID, uint16_t *arg, uint32_t &boardID)
{
  if (m_DEBUG) { TRACE("Command Answer Header = 0x"<<std::hex<<CmdAnswerHeader); }
  uint16_t CmdAnswerPayload = CmdAnswerHeader       & 0x0000ffff; // bits 0 - 15
  CmdID             = (CmdAnswerHeader>>16) & 0x0000001f;// bits 16 - 20
  boardID           = (CmdAnswerHeader>>21) & 0x0000007f;// bits 21 - 27
  // cmdAnswerHeaderID = (CmdAnswerHeader>>28) & 0x0000000f;// bits 28 - 31
  if (arg != nullptr){
    (*arg)          = CmdAnswerHeader       & 0x0000ffff; // bits 0 - 15
  }
  
  // Check for Errors
  if (CmdID == 0x1e){
    ERROR("Board answered with an ERROR: 0x"<<std::hex<<CmdAnswerPayload);
    if (CmdAnswerPayload & GPIOErrors::W_WID){
      ERROR("   Wrong word ID ");
    }
    if (CmdAnswerPayload & GPIOErrors::W_CID){
      ERROR("   Wrong command ID ");
    }
    if (CmdAnswerPayload & GPIOErrors::W_SCID){
      ERROR("   Wrong sub-command ID ");
    }
    if (CmdAnswerPayload & GPIOErrors::W_FIDX){
      ERROR("   Wrong frame index during argument frame ");
    }
    if (CmdAnswerPayload == GPIOErrors::TIMEOUT){
      ERROR("   Timeout error while waiting for state machine acknowledge ");
    }
    return false;
  }
  return true;
}

/** *******************************************************
 \brief Send vector of 32-bit commands packed in bytes
 \param CommandRequestHeader: header of the command to be sent
 \param CommandRequestArguments: vector of command arguments to be sent
 \param CommandRequestTrailer: trailer of the comand to be sent
  ******************************************************** */
bool GPIOBaseClass::SendCommand(uint32_t CommandRequestHeader, std::vector<uint32_t> CommandRequestArguments, uint32_t CommandRequestTrailer) 
{
  int sendBytes;

  m_interface->ClearSendBuffer();
  m_interface->QueueData(CommandRequestHeader);
  sendBytes = m_interface->SendBuffer();
  if (sendBytes != 4){
    WARNING("Expected to send 4 bytes, but transferred only "<<sendBytes);
    return false;
  }

  for (int i = 0; i < CommandRequestArguments.size(); i++) {
    m_interface->ClearSendBuffer();
    m_interface->QueueData(CommandRequestArguments[i]);
    sendBytes = m_interface->SendBuffer();
    if (sendBytes != 4){
      WARNING("Expected to send 4 bytes, but transferred only "<<sendBytes);
      return false;
    }
  }

  m_interface->ClearSendBuffer();
  m_interface->QueueData(CommandRequestTrailer);
  sendBytes = m_interface->SendBuffer();
  if (sendBytes != 4){
    WARNING("Expected to send 4 bytes, but transferred only "<<sendBytes);
    return false;
  }
  return true;
}

/** *******************************************************
 \brief Send 32-bit command packed in bytes
 \param CommandRequestHeader: single command header to be sent
 ******************************************************** */
bool GPIOBaseClass::SendCommand(uint32_t CommandRequestHeader) {
  m_interface->ClearSendBuffer();
  m_interface->QueueData(CommandRequestHeader);
  int sendBytes = m_interface->SendBuffer();
  if (sendBytes != 4){
    WARNING("Expected to send 4 bytes, but transferred only "<<sendBytes);
    return false;
  }
  return true;
}

/** *******************************************************
 Word content: Bits 31-28: Cmd Req head ID; 27-21: board ID; 20-16: CmdID; 15-0: argument (if single argument)
 \brief Form the 32-bit Command Request Header
 \param CmdID: command ID as documented in GPI board documentation
 \param arg: argument of the command
 \param BoardID: board ID of the board to which we are sending the command
 ******************************************************** */
uint32_t GPIOBaseClass::MakeCommandRequestHeader(uint8_t CmdID, uint16_t arg, uint8_t BoardID)
{
  uint32_t word = GPIOWordID::CmdReqHeadID;
  word <<= 7;
  word |= (BoardID & 0x7F); // 7 bits board ID
  word <<= 5;
  word |= (CmdID & 0x1F); // 5 bits cmdID
  word <<= 16;
  word |= (arg & 0xFFFF); // 16 argument
  
  if (m_DEBUG) {
    TRACE("CommandRequestHeader with CmdID = 0x"<<std::hex<<(int)CmdID
    <<" BoardID = 0x"<<(int)BoardID<<" arg = 0x"<<arg<<": 0x"<<std::setw(8)<<std::setfill('0')<<word);
  }
  
  return word;
}

/** *******************************************************
 Word content: Bits 31-28: Cmd Req head ID; 27-21: board ID; 20-16: CmdID; 15-12: SubCmdID, 11-0 number of frames to be sent (if multiple arguments)
 \brief Form the 32-bit Multiple argument Command Request Header
 \param CmdID: command ID as documented in GPIO board firmware documentation
 \param SubCmdID: subcommand ID as documented in GPIO board firmware documentation
 \param ArgNum: number of arguments the command with this header is going to have
 \param BoardID: board ID of the board to which we are sending the command
 ******************************************************** */
uint32_t GPIOBaseClass::MakeMultiargCommandRequestHeader(uint8_t CmdID, uint8_t SubCmdID, uint16_t ArgNum, uint8_t BoardID)
{
  uint32_t word = GPIOWordID::CmdReqHeadID;
  word <<= 7;
  word |= (BoardID & 0x7F); // 7 bits board ID
  word <<= 5;
  word |= (CmdID & 0x1F); // 5 bits cmdID
  word <<= 4;
  word |= (SubCmdID & 0xF); // 4 bits subCmdID
  word <<= 12;
  word |= (ArgNum & 0xFFF); // 12 bits number of arguments

  if (m_DEBUG) {
    TRACE("MultipleArgCommandRequestHeader with CmdID = 0x"<<std::hex<<(int)CmdID
    << " SubCmdID = 0x" <<(int)SubCmdID <<" BoardID = 0x"<<(int)BoardID<<" number of args = 0x"<<ArgNum
    <<": 0x"<<std::setw(8)<<std::setfill('0')<<word);
  }

  return word;
}

/** *******************************************************
 Word content: Bits 31-28: Cmd Req Arg ID; 27-16: frame index; 15-0: payload
 \brief Form the 32-bit Multiple argument Command Request Argument
 \param arg: vector of arguments
 ******************************************************** */ 
std::vector<uint32_t> GPIOBaseClass::MakeMultiargCommandRequestArgument(std::vector<uint16_t> arg)
{
  std::vector<uint32_t> word(arg.size());
 
 for (uint16_t i = 0; i < arg.size(); i++){
    word[i] = GPIOWordID::CmdReqArgID;
    word[i] <<= 12;
    word[i] |= (i & 0xFFF); // 12 bits frame number
    word[i] <<= 16;
    word[i] |= (arg[i] & 0xFFFF); //16 bits of argument
  }

  if (m_DEBUG) {
    TRACE("MultiargCommandRequest contains:"); 
    for (int i = 0; i < arg.size(); i++)
    TRACE("  frame number " <<i<<std::hex<< " argument arg =0x" <<arg[i]); 
  }

  return word; 
}

/** *******************************************************
 Word content: Bits 31-28: Cmd Req Arg ID; 27-21: board ID; 20-16: CmdID; 15-0: payload
 \brief Form the 32-bit Command Request Argument
 \param CmdID: command ID as documented in GPIO board firmware documentation
 \param arg: vector of arguments
 ******************************************************** */
uint32_t GPIOBaseClass::MakeCommandRequestArgument(uint8_t CmdID, uint16_t arg, uint8_t BoardID)
{
  uint32_t word = GPIOWordID::CmdReqArgID;
  word <<= 7;
  word |= (BoardID & 0x7F); // 7 bits board ID
  word <<= 5;
  word |= (CmdID & 0x1F); // 5 bits cmdID
  word <<= 16;
  word |= (arg & 0xFFFF); // 16 argument
  
  if (m_DEBUG) {
    TRACE("CommandRequestArgument with CmdID = 0x"<<std::hex<<CmdID
    <<" BoardID = 0x"<<BoardID<<" arg = 0x"<<arg<<": 0x"<<std::setw(8)<<std::setfill('0')<<word);
  }
  
  return word;
}

/** *******************************************************
 Word content: Bits 31-28: Cmd Req Arg ID; 27-21: board ID; 20-16: CmdID; 15-0: CRC16
 \brief Form the 32-bit Command Request Trailer
 \param CmdID: command ID as documented in GPIO board firmware documentation
 \param crc16: localy computed CRC checksum that is going to be attached to the trailer
 \param BoardID: board ID of the board to which we are sending the command
 ******************************************************** */
uint32_t GPIOBaseClass::MakeCommandRequestTrailer(uint8_t CmdID, uint16_t crc16, uint8_t BoardID)
{
  uint32_t word = GPIOWordID::CmdReqTrailID;
  word <<= 7;
  word |= (BoardID & 0x7F); // 7 bits board ID
  word <<= 5;
  word |= (CmdID & 0x1F); // 5 bits cmdID
  word <<= 16;
  word |= (crc16 & 0xFFFF); // 16 bit crc code
  
  if (m_DEBUG) {
    TRACE("CommandRequestTrailer with CmdID = 0x"<<std::hex<<CmdID
    <<" BoardID = 0x"<<BoardID<<" crc = 0x0000"<<": 0x0"<<std::setw(8)<<std::setfill('0')<<word);
  }
  
  return word;
}


/** *******************************************************
 \brief Querries all firmware versions from the tracker readout board and printed to log.
 Versions querried: FPGA top, FPGA decoder, Hardware ID, Product ID
 ******************************************************** */
bool GPIOBaseClass::GetFirmwareVersions()
{
  static const uint16_t argFPGAtop       = 0x0000;
  static const uint16_t argFPGAdecoder   = 0x0001;
  static const uint16_t argHardware      = 0x0002;
  static const uint16_t argProductID     = 0x0003;
  
  unsigned int FPGAmajor = 0, FPGAminor = 0, FPGAencoder = 0, hardware = 0, product = 0;
  uint16_t answArg = 0;
  
  if (!GetSingleFirmwareVersion(argFPGAtop, answArg)){
    ERROR("GetSingleFirmwareVersion(argFPGAtop) failed");
    //return false;
  }
  
  m_FPGA_version = answArg;
  
  FPGAminor = 0x000f & answArg;
  FPGAmajor = (0xfff0 & answArg) >> 4;
  
  if (!GetSingleFirmwareVersion(argFPGAdecoder, answArg)){
    ERROR("GetSingleFirmwareVersion(argFPGAdecoder) failed");
    //return false;
  }
  FPGAencoder = answArg;
  
  if (!GetSingleFirmwareVersion(argHardware, answArg)){
    ERROR("GetSingleFirmwareVersion(argHardware) failed");
    //return false;
  }
  hardware = answArg;
  
  if (!GetSingleFirmwareVersion(argProductID, answArg)){
    ERROR("GetSingleFirmwareVersion(argProductID) failed");
    //return false;
  }
  product = answArg;
  
  INFO("Firmware versions: \n"
  <<std::setw(13)<< " FPGA: "<<std::setfill(' ')<<FPGAmajor<<"."<<FPGAminor<<" = 0x"<<std::hex<<m_FPGA_version<<std::endl
  <<std::setw(13)<< " Encoder: "<<std::setfill(' ')<<std::setw(12)<<FPGAencoder<<std::setfill(' ')<<std::endl
  <<std::setw(13)<< " Hardware: "<<std::setfill(' ')<<std::setw(12)<<hardware<<std::setfill(' ')<<std::endl
  <<std::setw(13)<< " Product ID: "<<std::setfill(' ')<<std::setw(12)<<product<<std::setfill(' '));
  
  
  return true;
}

/** *******************************************************
 \brief Read the firmware version of a single unit.
 ******************************************************** */
bool GPIOBaseClass::GetSingleFirmwareVersion(uint16_t whichFirmware,  uint16_t &answArg){
  return SendAndRetrieve(GPIOCmdID::FIRMWARE_VERSION, whichFirmware, &answArg);
}

/** *******************************************************
 \brief Set the debug level, propagate to interface object
 ******************************************************** */
void GPIOBaseClass::SetDebug(int v){
  m_interface->SetDebug(v);
  if (v > 0){
    m_DEBUG = true;
  } else {
    m_DEBUG = false;
  }
}


/** *******************************************************
 \brief sleep for a well defined amount of micoseconds, without suspending the thread. 
 ******************************************************** */
void GPIOBaseClass::block(unsigned long microseconds)
{
  auto start = std::chrono::system_clock::now();
  auto end = std::chrono::system_clock::now();
  while (std::chrono::duration_cast<std::chrono::microseconds>(end-start).count() < microseconds) {
     end = std::chrono::system_clock::now();
  }
}

