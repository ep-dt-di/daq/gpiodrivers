/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#include "../GPIOBase/CRC.h"

//TODO Check if Doxygen documentation is sufficient
//TODO Reffer to Yanicks original C# code(?)

using namespace FASER;
         
UFE_CRC::~UFE_CRC(){}

/** *******************************************************
 \brief Reflect the data value
 \param x_val: input value
 \param x_nbBits: number of bits to reflect
 ******************************************************** */
uint32_t UFE_CRC::reflect(uint32_t x_val, uint8_t x_nbBits)
{
  uint32_t l_reflection = 0;

  // Reflect the data.
  for (uint8_t l_bit = 0; l_bit < x_nbBits; l_bit++)
  {
    // If the LSB bit is set, set the reflection of it.
    if ((x_val & 0x01)==0x01)
      l_reflection |= (uint32_t)(1 << ((x_nbBits - 1) - l_bit));

    x_val = (x_val >> 1);
  }

  return (l_reflection);
}


/** *******************************************************
 \brief Build the table for a fast CRC computation
 ******************************************************** */
void UFE_CRC::buildTable()
{
  uint32_t l_remainder;
  uint32_t l_topBit = (uint32_t)(1<<(m_crcStruct.m_size - 1));

  // Compute the remainder of each possible dividend.
  for (int l_dividend = 0; l_dividend < 256; ++l_dividend)
  {
    // Start with the dividend followed by zeros.
    l_remainder = (uint32_t)(l_dividend << (m_crcStruct.m_size - 8));

    // Perform modulo-2 division, a bit at a time.
    for (uint8_t l_bit = 8; l_bit > 0; --l_bit)
    {
      //Try to divide the current data bit.
      if ((l_remainder & l_topBit) > 0)
        l_remainder = (l_remainder << 1) ^ m_crcStruct.m_polynomial;
      else
        l_remainder = (l_remainder << 1);
    }

    // Store the result into the table.       
    m_table[l_dividend] = l_remainder & m_mask;
  }
}


/** *******************************************************
 \brief Compute CRC for a complete message
 \param x_message: message to be computed
 ******************************************************** */
uint32_t UFE_CRC::crc(std::vector<uint8_t> x_message)
{
  uint8_t l_data;
  uint32_t l_remainder = m_crcStruct.m_initRemainder;

  // Divide the message by the polynomial, a byte at a time.
  for (int l_i = 0; l_i < x_message.size(); l_i++)
  {
    // reflect DIN ?
    uint8_t l_din = (m_crcStruct.m_reflectDin) ? (uint8_t)(reflect(x_message[l_i], 8)): x_message[l_i];

    l_data = (uint8_t)(l_din ^ (l_remainder >> (m_crcStruct.m_size - 8)));

    l_remainder = m_table[l_data] ^ (l_remainder << 8);
  }

  uint32_t l_crc = (l_remainder & m_mask) ^ m_crcStruct.m_finalXor;

  if (m_crcStruct.m_reflectCRC)
    l_crc = reflect(l_crc, m_crcStruct.m_size);

  return l_crc;
}

/** *******************************************************
 *  \brief Converts vector<uint16_t> to vector<uint8_t>. 
 *  (i.e. splits 16-bit uint into two 8-bit uints)   
 *  \param x_message: vector to be converted
 *  ******************************************************** */
std::vector<uint8_t> UFE_CRC::convert16to8bit(std::vector<uint16_t> x_message16)
{
  static std::vector<uint8_t> l_message8(2*x_message16.size());
   
  for(int i = 0; i < x_message16.size(); i++)
  {
    l_message8[2*i] = x_message16[i];
    l_message8[2*i + 1] = (x_message16[i] >> 8);
  }

  return l_message8;
}

//Assigning value to FASER::CRC_16_1A2EB static variable
UFE_CRC::CRCstruct FASER::CRC_16_1A2EB::CRC = FASER::UFE_CRC::CRCstruct(0xA2EB, 16, 0xFFFF, 0x0000, true, false);

//Assigning values to FASER::CRC_16_18005 static variables
UFE_CRC::CRCstruct FASER::CRC_16_18005::CRC = FASER::UFE_CRC::CRCstruct(0x8005, 16, 0x0000, 0x0000, true, true);
std::vector<uint8_t> FASER::CRC_16_18005::testMessage = {49, 50, 51, 52, 53, 54, 55, 56, 57}; //123456789 in ASCII converted to array of uint8_t
uint16_t FASER::CRC_16_18005::checkedValue = 0xBB3D; //Checked value expected from testMessage

//Assigning values to FASER::CRC_CCITT_11021 static variables
UFE_CRC::CRCstruct FASER::CRC_CCITT_11021::CRC = FASER::UFE_CRC::CRCstruct(0x1021, 16, 0xFFFF, 0x0000, false, false);
std::vector<uint8_t> FASER::CRC_CCITT_11021::testMessage = {49, 50, 51, 52, 53, 54, 55, 56, 57}; //123456789 in ASCII converted to array of uint8_t
uint16_t FASER::CRC_CCITT_11021::checkedValue = 0x29B1; //Checked value expected from testMessage
